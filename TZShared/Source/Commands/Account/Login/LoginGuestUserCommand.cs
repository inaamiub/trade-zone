﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(LoginGuestUserCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class LoginGuestUserCommand : Command<LoginGuestUserCommandPayload, LoginUserCommandResult>
{
    
}

public class LoginGuestUserCommandPayload : ICommandPayload, IJwtTokenData
{
    public string ClientId { get; set; }
    public string BranchId { get; set; }
    public string ClientSecret { get; set; }
    public string DeviceId { get; set; }
    public string LangKey { get; set; }
    public string AppVersion { get; set; }
    public Platform Platform { get; set; }
}