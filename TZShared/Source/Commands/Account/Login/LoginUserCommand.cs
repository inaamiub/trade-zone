﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(LoginUserCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class LoginUserCommand : Command<LoginUserCommandPayload, LoginUserCommandResult>
{
    
}

public class LoginUserCommandPayload : LoginGuestUserCommandPayload
{
    public string UserName { get; set; }
    public string Password { get; set; }
}

public class LoginUserCommandResult : LoginWithRefreshTokenCommandResult
{
    public string RefreshToken { get; set; }
}