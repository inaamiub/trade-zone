﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(LoginWithRefreshTokenCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class LoginWithRefreshTokenCommand : Command<LoginWithRefreshTokenCommandPayload, LoginWithRefreshTokenCommandResult>
{
    
}

public class LoginWithRefreshTokenCommandPayload : ICommandPayload, IJwtTokenData
{
    public string RefreshToken { get; set; }
    public string ClientId { get; set; }
    public string ClientSecret { get; set; }
    public string DeviceId { get; set; }
    public string LangKey { get; set; }
    public string BranchId { get; set; }
    public string AppVersion { get; set; }
    public Platform Platform { get; set; }
}

public class LoginWithRefreshTokenCommandResult : ICommandResult
{
    public string AccessToken { get; set; }
    public UserInformationVO User { get; set; }
}

public interface IJwtTokenData
{
    public string ClientId { get; }
    public string DeviceId { get; }
    public string LangKey { get; }
    public string BranchId { get; }
    public string AppVersion { get; }
    public Platform Platform { get; }
}