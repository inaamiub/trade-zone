﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(SignupUserCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class SignupUserCommand : Command<SignupUserCommandPayload, LoginUserCommandResult>
{
    
}

public class SignupUserCommandPayload : ICommandPayload, IJwtTokenData
{
    public UserInformationVO User { get; set; } 
    public string ClientId { get; set; }
    public string ClientSecret { get; set; }
    public string DeviceId { get; set; }
    public string LangKey { get; set; }
    public string AppVersion { get; set; }
    public Platform Platform { get; set; }
    public string BranchId => User.BranchId;
}