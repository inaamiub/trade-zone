﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(RecoverPasswordCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class RecoverPasswordCommand : Command<RecoverPasswordCommandPayload>
{
    
}

public class RecoverPasswordCommandPayload : VerifyPasswordRecoveryTokenCommandPayload
{
    public string NewPassword { get; set; }
}