﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(SendAccountVerificationEmailCommand), CommandFlag.Default)]
public abstract class SendAccountVerificationEmailCommand : Command<VoidCommandPayload>
{
    
}