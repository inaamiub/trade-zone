﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(SendPasswordRecoveryEmailCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class SendPasswordRecoveryEmailCommand : Command<SendPasswordRecoveryEmailCommandPayload>
{
    
}

public class SendPasswordRecoveryEmailCommandPayload : ICommandPayload
{
    public string ClientId { get; set; }
    public string BranchId { get; set; }
    public string DeviceId { get; set; }
    public string Email { get; set; }
}