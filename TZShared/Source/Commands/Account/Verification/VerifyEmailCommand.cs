﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(VerifyEmailCommand), CommandFlag.Default)]
public abstract class VerifyEmailCommand : Command<VerifyEmailCommandPayload>
{
    
}

public class VerifyEmailCommandPayload : ICommandPayload
{
    public string Token { get; set; }
}