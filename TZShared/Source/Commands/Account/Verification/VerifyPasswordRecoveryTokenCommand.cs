﻿using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(VerifyPasswordRecoveryTokenCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class VerifyPasswordRecoveryTokenCommand : Command<VerifyPasswordRecoveryTokenCommandPayload>
{
    
}

public class VerifyPasswordRecoveryTokenCommandPayload : ICommandPayload
{
    public string Token { get; set; }
    public string Email { get; set; }
    public string BranchId { get; set; }
}