﻿using System.Collections.Generic;
using HatfCommand;

namespace TZShared;

[Command(nameof(GetAllUserAddressesCommand), CommandFlag.Default)]
public abstract class GetAllUserAddressesCommand : Command<VoidCommandPayload, GetAllUserAddressesCommandResult>
{
    
}

public class GetAllUserAddressesCommandResult : ICommandResult
{
    public List<UserAddressVO> Addresses { get; set; } = new List<UserAddressVO>();
    public string LastUsedAddressId { get; set; }
}