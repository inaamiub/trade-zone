﻿using System.Collections.Generic;
using HatfCommand;

namespace TZShared;

[Command(nameof(SetUserAddressCommand), CommandFlag.Default)]
public abstract class SetUserAddressCommand : Command<SetUserAddressCommandPayload, SetUserAddressCommandPayload>
{
    
}

public class SetUserAddressCommandPayload : ICommandPayload, ICommandResult
{
    public UserAddressVO Address { get; set; }
    public bool IsNew { get; set; }
}