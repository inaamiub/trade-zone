﻿using HatfCommand;

namespace TZShared;

[Command(nameof(CheckAppVersionCommand), CommandFlag.Default | CommandFlag.Anonymous)]
public abstract class CheckAppVersionCommand : Command<CheckAppVersionCommandPayload, CheckAppVersionCommandResult>
{
    
}

public class CheckAppVersionCommandPayload : ICommandPayload
{
    public string BranchId { get; set; }
    public int AppVersion { get; set; }
    public int LastAppVersion { get; set; }
}

public class CheckAppVersionCommandResult : ICommandResult
{
    public bool UpdateAvailable { get; set; }
    public bool Forced { get; set; }
    public bool ResetUserToken { get; set; }
}