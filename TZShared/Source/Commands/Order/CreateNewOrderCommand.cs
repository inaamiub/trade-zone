﻿using System.Collections.Generic;
using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(CreateNewOrderCommand), CommandFlag.Default)]
public abstract class CreateNewOrderCommand : Command<CreateNewOrderCommandPayload, GetUserOrderHistoryCommandResult>
{
    
}

public class CreateNewOrderCommandPayload : ICommandPayload
{
    public OrderDeliveryType OrderDeliveryType { get; set; }
    public string AddressId { get; set; }
    public string UUID { get; set; }
    public string Remarks { get; set; }
    public PaymentMethod PaymentMethod { get; set; }
    public Dictionary<string, uint> Products { get; set; }
}