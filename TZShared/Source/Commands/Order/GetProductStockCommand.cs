﻿using System.Collections.Generic;
using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(GetProductsStockCommand), CommandFlag.Default)]
public abstract class GetProductsStockCommand : Command<VoidCommandPayload, GetProductsStockCommandResult>
{
    
}
public class GetProductsStockCommandResult : ICommandResult
{
    public List<ProductStockModel> Products { get; set; } = new List<ProductStockModel>();
}