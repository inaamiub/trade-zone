﻿using System.Collections.Generic;
using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(GetProductsCommand), CommandFlag.Default)]
public abstract class GetProductsCommand : Command<VoidCommandPayload, GetProductsCommandResult>
{
    
}

public class GetProductsCommandResult : ICommandResult
{
    public List<ProductModel> Products { get; set; } = new List<ProductModel>();
}