﻿using System.Collections.Generic;
using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(GetStockReportCommand), CommandFlag.Default)]
public abstract class GetStockReportCommand : Command<VoidCommandPayload, GetStockReportCommandResult>
{
    
}

public class GetStockReportCommandResult : ICommandResult
{
    public List<StockReportModel> ReportData { get; set; } = new List<StockReportModel>();
}