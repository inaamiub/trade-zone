﻿using System.Collections.Generic;
using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(GetUserOrderHistoryCommand), CommandFlag.Default)]
public abstract class GetUserOrderHistoryCommand : Command<VoidCommandPayload, GetUserOrderHistoryCommandResult>
{
    
}

public class GetUserOrderHistoryCommandResult : ICommandResult
{
    public List<SalesOrderVO> Orders { get; set; }
}