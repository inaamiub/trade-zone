﻿using HatfCommand;

namespace TZShared;

[Command(nameof(GetCurrentUserProfileCommand), CommandFlag.Default)]
public abstract class GetCurrentUserProfileCommand : Command<VoidCommandPayload, GetCurrentUserProfileCommandResult>
{
    
}

public class GetCurrentUserProfileCommandResult : ICommandResult
{
    public UserInformationVO User { get; set; }
}