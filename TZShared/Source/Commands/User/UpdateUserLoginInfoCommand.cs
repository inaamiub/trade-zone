﻿using System.Collections.Generic;
using HatfCommand;
using HatfCore;
using HatfShared;

namespace TZShared;

[Command(nameof(UpdateUserLoginInfoCommand), CommandFlag.Default)]
public abstract class UpdateUserLoginInfoCommand : Command<UpdateUserLoginInfoCommandPayload>
{
    
}

public class UpdateUserLoginInfoCommandPayload : ICommandPayload
{
    public string NewUserName { get; set; }
    public string CurrentPassword { get; set; }
    public string NewPassword { get; set; }
}