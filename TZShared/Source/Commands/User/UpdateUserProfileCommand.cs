﻿using HatfCommand;

namespace TZShared;

[Command(nameof(UpdateUserProfileCommand), CommandFlag.Default)]
public abstract class UpdateUserProfileCommand : Command<UpdateUserProfileCommandPayload, GetCurrentUserProfileCommandResult>
{
    
}

public class UpdateUserProfileCommandPayload : ICommandPayload, ICommandResult
{
    public UserInformationVO User { get; set; }
}