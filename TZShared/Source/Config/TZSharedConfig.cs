﻿using System.Collections.Generic;
using HatfShared;

namespace TZShared
{
    public class TZSharedConfig : HBaseConfig
    {
        public List<string> States { get; set; }
        public bool EnsureItemStockInOrders { get; set; }
    }
}