﻿namespace TZShared
{
    public enum OrderDeliveryType
    {
        Pickup,
        Deliver
    }

    public enum PaymentMethod
    {
        CashOnDelivery,
        OnlineTransfer
    }
    
    public enum OrderType
    {
        A,
        W,
        D
    }
}