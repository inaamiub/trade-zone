﻿using System.ComponentModel;

namespace TZShared
{
    public enum UserType
    {
        [Description("None")]
        None = 0,
        [Description("Full Access")]
        FullAccess = 1,
        [Description("As Guest")]
        AsGuest = 2,
        [Description("Public Access")]
        PublicAccess = 3,
    }
}