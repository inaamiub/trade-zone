﻿using HatfShared;

namespace TZShared
{
    public class TZErrorCodes : HatfErrorCodes
    {
        public const string EmptyBranchId = "TZ-1";
        public const string BRANCH_NOT_FOUND = "TZ-2";
        public const string PARTY_DOES_NOT_EXISTS = "TZ-3";
        public const string PARTY_ALREADY_HAVE_ACCOUNT = "TZ-4";
        public const string CELL_NO_DIDNT_MATCH_WITH_SAVED_ONE = "TZ-5";
        public const string WRONG_USER_TYPE = "TZ-6";
        public const string EMAIL_ALREADY_EXISTS = "TZ-7";
        
        public const string VERSION_NOT_FOUND = "TZ-8";
        public const string EMAIL_NOT_EXISTS = "TZ-9";
        public const string GUEST_NOT_ALLOWED = "TZ-10";
        public const string GUEST_NOT_SUPPORTED = "TZ-11";
        // Order
        public const string GUEST_CAN_NOT_CREATE_ORDER = "ORD-1";
        public const string PRODUCTS_NOT_FOUND = "ORD-2";
        public const string ORDER_ALREADY_CREATED = "ORD-3";
        public const string ONE_OF_THE_PRODUCTS_NOT_FOUND = "ORD-4";
        public const string ONE_OF_THE_PRODUCTS_HAS_ZERO_QUANTITY = "ORD-5";
        public const string ADDRESS_NOT_FOUND = "ORD-6";
        public const string PLATFORM_NOT_HANDLED = "ORD-7";
        
        // User Address
        public const string WRONG_ADDRESS_PROVIDED = "ADR-1";
    }
}