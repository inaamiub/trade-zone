﻿using System.Collections.Generic;
using TZShared;

namespace HatfShared
{
    public class TZErrorMessages : HatfErrorMessages
    {
        private static readonly Dictionary<string, string> TZErrorCodeMessages = new () 
        {
            {TZErrorCodes.BRANCH_NOT_FOUND, "Branch not found"},
            {TZErrorCodes.PARTY_DOES_NOT_EXISTS, "Party doesn't exists"},
            {TZErrorCodes.PARTY_ALREADY_HAVE_ACCOUNT, "Party already associated with an account"},
            {TZErrorCodes.CELL_NO_DIDNT_MATCH_WITH_SAVED_ONE, "Cell No. didn't match with the saved one"},
            {TZErrorCodes.WRONG_USER_TYPE, SERVER_ERROR_OCCURED + TZErrorCodes.WRONG_USER_TYPE},
            {TZErrorCodes.EMAIL_ALREADY_EXISTS, "Email already exists"},
            {TZErrorCodes.VERSION_NOT_FOUND, "Version not found"},
            {TZErrorCodes.EMAIL_NOT_EXISTS, "Email not exists"},
            {TZErrorCodes.GUEST_CAN_NOT_CREATE_ORDER, "Guest can't create order. Please Login or Signup"},
            {TZErrorCodes.PRODUCTS_NOT_FOUND, "Product(s) not found"},
            {TZErrorCodes.ORDER_ALREADY_CREATED, "Order already created"},
            {TZErrorCodes.ONE_OF_THE_PRODUCTS_NOT_FOUND, "One of the products not found"},
            {TZErrorCodes.ONE_OF_THE_PRODUCTS_HAS_ZERO_QUANTITY, "One of the product has zero quantity"},
            {TZErrorCodes.ADDRESS_NOT_FOUND, "Address not found"},
            {TZErrorCodes.WRONG_ADDRESS_PROVIDED, "Wrong address provided"},
            {TZErrorCodes.GUEST_NOT_ALLOWED, "Unauthorized access. Please Login or Signup to continue"},
            {TZErrorCodes.GUEST_NOT_SUPPORTED, "Guest login is not supported"},
        };

        static TZErrorMessages()
        {
            foreach (KeyValuePair<string, string> tzErrorCodeMessage in TZErrorCodeMessages)
            {
                AddToCache(tzErrorCodeMessage.Key, tzErrorCodeMessage.Value);
            }
        }
        
    }
}