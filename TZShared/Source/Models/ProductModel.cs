﻿using System;

namespace TZShared
{
    [Serializable]
    public partial class ProductModel
    {
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public short? GroupID { get; set; }
        public string GroupName { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
    }
    
}