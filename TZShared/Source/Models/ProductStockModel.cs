﻿using System;

namespace TZShared
{
    [Serializable]
    public partial class ProductStockModel
    {
        public string ProductID { get; set; }
        public int Stock { get; set; }
    }
}