﻿#if HATF_MOBILE
using SQLite;
#endif
using System;
using HatfShared;

namespace TZShared;

[Serializable]
#if HATF_MOBILE
[Table("TblCart")]
#endif
public partial class ShoppingCartItemModel
#if HATF_MOBILE
: ISqliteTable
#endif
{
    public ShoppingCartItemModel()
    {

    }

#if HATF_MOBILE 
    [PrimaryKey][AutoIncrement][Column("ID")]
    public long Id { get; set; }
#endif
    public string ItemId { get; set; }
    public uint Quantity { get; set; }
#if HATF_MOBILE
    [Ignore]
    public StockReportModel ProductDetails { get; set; }
    public string UserId { get; set; }

    public ShoppingCartItemModel(ShoppingCartItemModel existingItem)
    {
        if (existingItem == null)
        {
            return;
        }
        this.Id = existingItem.Id;
        this.UserId = existingItem.UserId;
        this.ItemId = existingItem.ItemId;
        this.Quantity = existingItem.Quantity;
        this.ProductDetails = existingItem.ProductDetails;
    }
#endif
}