﻿using System;
using Newtonsoft.Json;
#if HATF_MOBILE
using HatfShared;
using SQLite;
#endif

namespace TZShared;

[Serializable]
#if HATF_MOBILE
[Table("TblReportData")]
#endif
public partial class StockReportModel
#if HATF_MOBILE
    : ISqliteTable
#endif
{
#if HATF_MOBILE 
    [PrimaryKey][AutoIncrement][Column("ID")]
    public long Id { get; set; }
#endif
    public string CompanyID { get; set; }
    public string CompanyName { get; set; }
    public short? GroupID { get; set; }
    public string GroupName { get; set; }
    public string ProductID { get; set; }
    public string ProductName { get; set; }
    public decimal RetailPrice { get; set; }
    public int Stock { get; set; }
    [JsonIgnore] public bool StockStatus => Stock > 0;
}