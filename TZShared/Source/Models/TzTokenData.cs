﻿using HatfShared;

namespace TZShared;

public class TzTokenData : TokenData
{
    public string BranchId { get; set; }
}