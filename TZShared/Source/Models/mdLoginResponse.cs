﻿using System;
using HatfShared;

namespace TZShared
{
    [Serializable]
    public class mdLoginResponse : mdHttpResponseBase
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public UserInformationVO User { get; set; }
    }
}