﻿using System;
using System.Collections.Generic;

namespace TZShared
{
    [Serializable]
    public partial class mdNewOrder
    {
        public OrderDeliveryType OrderDeliveryType { get; set; }
        public string AddressId { get; set; }
        public string UUID { get; set; }
        public string Remarks { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public Dictionary<string, uint> Products { get; set; }
    }
}
