﻿using System;
using System.Collections.Generic;
using HatfShared;

namespace TZShared
{
    [Serializable]
    public partial class mdOrderHistory : mdHttpResponseBase
    {
        public List<SalesOrderVO> Orders { get; set; }
    }
}
