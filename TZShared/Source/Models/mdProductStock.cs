﻿using System;
using System.Collections.Generic;
using HatfShared;

namespace TZShared
{
    [Serializable]
    public partial class mdProductStock : mdHttpResponseBase
    {
        public List<ProductStockModel> ProductStock { get; set; } = new List<ProductStockModel>();
    }

}
