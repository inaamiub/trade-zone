﻿using System;
using System.Collections.Generic;
using HatfShared;

namespace TZShared
{
    [Serializable]
    public partial class mdProducts : mdHttpResponseBase
    {
        public List<ProductModel> Products { get; set; } = new List<ProductModel>();
    }

}
