﻿using System;
using System.Collections.Generic;
using HatfShared;

namespace TZShared
{
    [Serializable]
    public partial class mdProductsWithStock : mdHttpResponseBase
    {
        public List<StockReportModel> ReportData { get; set; } = new List<StockReportModel>();
    }

}
