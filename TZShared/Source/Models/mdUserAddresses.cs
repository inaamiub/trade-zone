﻿using System;
using System.Collections.Generic;
using HatfShared;

namespace TZShared
{
    [Serializable]
    public partial class mdUserAddresses : mdHttpResponseBase
    {
        public List<UserAddressVO> Addresses { get; set; } = new List<UserAddressVO>();
        public string LastUsedAddressId { get; set; }
    }

}
