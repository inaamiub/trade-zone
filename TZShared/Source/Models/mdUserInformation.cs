﻿using System;

namespace TZShared
{
    [Serializable]
    public partial class mdUserInformation
    {
        public string NewUserName { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }

}
