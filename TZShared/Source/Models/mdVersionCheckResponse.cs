﻿using System;
using HatfShared;

namespace TZShared
{
    [Serializable]
    public class mdVersionCheckResponse : mdHttpResponseBase
    {
        public bool UpdateAvailable { get; set; }
        public bool Forced { get; set; }
        public bool ResetUserToken { get; set; }
    }
}