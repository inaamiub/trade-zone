﻿namespace TZShared
{
    public class TZAuthHelper
    {
        private const string BranchUserNameSplitter = "|||";

        public static string CombineBranchAndUserName(string branchId, string userName)
        {
            return string.Concat(branchId, BranchUserNameSplitter, userName);
        }

        public static bool ExtractBranchIdAndUserName(string fullKey, out string branchId, out string userName)
        {
            if (fullKey.Contains(BranchUserNameSplitter) == false)
            {
                branchId = null;
                userName = null;
                return false;
            }

            string[] fullKeyArray = fullKey.Split(BranchUserNameSplitter);
            branchId = fullKeyArray[0];
            userName = fullKeyArray[1];
            return true;
        }
    }
}