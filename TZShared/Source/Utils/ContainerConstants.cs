﻿namespace TZShared
{
    public class ContainerConstants
    {
        public const string USER_LAST_USED_ADDRESS_ID = "USER_LAST_USER_ADDRESS_ID";
        public const string ALL_USER_ADDRESSES = "ALL_USER_ADDRESSES";
    }
}