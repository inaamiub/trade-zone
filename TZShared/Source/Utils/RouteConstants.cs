﻿namespace TZShared
{
    public class TzRouteConstants
    {
        /********************************************************************************
        *                                    Account                                    * 
        ********************************************************************************/
        public const string ACCOUNT_CONTROLLER = "Account";
        public const string ACCOUNT_LOGIN = "Login";
        public const string ACCOUNT_REFRESH_TOKEN = "RefreshToken";
        public const string ACCOUNT_SIGNUP = "Signup";
        public const string ACCOUNT_SEND_ACCOUNT_VERIFICATION_TOKEN = "SendAccountVerificationEmail";
        public const string ACCOUNT_VERIFY_EMAIL = "VerifyEmail";
        public const string ACCOUNT_UPDATE_ACCOUNT_INFO = "UpdateAccountInfo";
        public const string ACCOUNT_UPDATE_BASIC_USER_INFO = "UpdateBasicUserInfo";
        public const string ACCOUNT_GET_USER_INFORMATION = "GetUserInformation";
        public const string ACCOUNT_VERIFY_TOKEN_BY_EMAIL = "VerifyTokenByEmail";
        public const string ACCOUNT_SEND_PASSWORD_RECOVERY_TOKEN = "SendPasswordRecoveryToken";
        public const string ACCOUNT_RECOVER_PASSWORD = "ResetPassword";
        /********************************************************************************
        *                                    AppVersion                                 * 
        ********************************************************************************/
        public const string APP_VERSION_CONTROLLER = "AppVersion";
        public const string APP_VERSION_ANDROID = "Android";
        public const string APP_VERSION_ANDROID_NEW = "Android2";
        /********************************************************************************
        *                                    Order                                      * 
        ********************************************************************************/
        public const string ORDER_CONTROLLER = "Order";
        public const string ORDER_GET_PRODUCTS_WITH_STOCK = "StockReport";
        public const string ORDER_GET_PRODUCTS = "GetProducts";
        public const string ORDER_GET_PRODUCT_STOCK = "GetProductStock";
        public const string ORDER_NEW_ANDROID_ORDER = "ANewOrder";
        public const string ORDER_NEW_WEB_ORDER = "WNewOrder";
        public const string ORDER_HISTORY = "History";
        /********************************************************************************
        *                               User Address                                    * 
        ********************************************************************************/
        public const string USER_ADDRESS_CONTROLLER = "UserAddress";
        public const string GET_ALL_USER_ADDRESSES = "GetAllUserAddresses";
        public const string ADD_NEW_ADDRESS = "AddAddress";
        public const string UPDATE_ADDRESS = "UpdateAddress";
        /********************************************************************************
        *                               Privacy Policy                                  * 
        ********************************************************************************/
        public const string PRIVACY_POLICY_CONTROLLER = "PrivacyPolicy";
        public const string MKT = "MKT";
    }
}