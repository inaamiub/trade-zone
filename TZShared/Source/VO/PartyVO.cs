﻿using System;
using HatfShared;

namespace TZShared
{
    [MSSqlCollection("dbo", "Parties")]
    public partial class PartyVO : HBaseVO
    {
        public string AccountNo { get; set; }
        public string BranchId { get; set; }
        public string PartyName { get; set; }
        public short? ActualTownId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ContactPerson { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string EMail { get; set; }
        public string WWW { get; set; }
        public string NTN { get; set; }
        public string STN { get; set; }
        public bool? IsCustomer { get; set; }
        public bool? IsVendor { get; set; }
        public string CustomerType { get; set; }
        public decimal CreditLimit { get; set; }
        public byte CreditPeriod { get; set; }
        public string PriceCategory { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool IsActive { get; set; }
        public string Status { get; set; }
        public string StatusRemarks { get; set; }
        public long? SyncSignature { get; set; }
        public string UUID { get; set; }
    }

    public class PartyVOKeys
    {
        public const string AccountNo = "AccountNo";
        public const string BranchId = "BranchId";
        public const string UUID = "UUID";
        public const string UserId = "UserId";
    }
}