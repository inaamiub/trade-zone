﻿using HatfShared;

namespace TZShared
{
    [MSSqlCollection("dbo", "Products")]
    public partial class ProductVO : HBaseVO
    {
        public string BranchId { get; set; }
        public string ProductId { get; set; }
        public string CompanyId { get; set; }
        public string GroupId { get; set; }
        public string ProductName { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal PurchaseDiscRatio { get; set; }
        public decimal PurchaseSTValue { get; set; }
        public decimal PurchaseSTRatio { get; set; }
        public decimal SalePrice1 { get; set; }
        public decimal SalePrice2 { get; set; }
        public decimal SalePrice3 { get; set; }
        public decimal SaleDiscRatio { get; set; }
        public decimal SaleDiscRatioRetail { get; set; }
        public decimal SaleSTValue { get; set; }
        public decimal SaleSTRatio { get; set; }
        public decimal RetailPrice { get; set; }
        public int MinStockLevel { get; set; }
        public int OrderStockLevel { get; set; }
        public bool IsSerialProduct { get; set; }
        public string BarCode { get; set; }
        public bool IsActive { get; set; }
        public int DepartmentID { get; set; }
        public long SyncSignature { get; set; }
        public string UUID { get; set; }
    }

    public class ProductVOKeys
    {
        public const string BranchId = "BranchId";
        public const string ProductId = "ProductId";
        public const string UUID = "UUID";
        public const string RetailPrice = "RetailPrice";
        public const string IsActive = "IsActive";
    }
}