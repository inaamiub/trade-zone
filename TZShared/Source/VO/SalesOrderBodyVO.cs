﻿using HatfShared;

namespace TZShared
{
    [MSSqlCollection("dbo", "SalesOrderBody")]
    public partial class SalesOrderBodyVO : HBaseVO
    {
        public int SerialNo { get; set; }
        public string BranchId { get; set; }
        public OrderType OrderType { get; set; }
        public long OrderID { get; set; }
        public string ProductID { get; set; }
        public decimal Quantity { get; set; }
        public decimal Bonus { get; set; }
        public decimal Price { get; set; }
        public decimal DiscRatio1 { get; set; }
        public decimal DiscRatio2 { get; set; }
        public decimal DiscValue { get; set; }
        public decimal STValue { get; set; }
        public decimal STRatio { get; set; }
        public decimal TTLValue { get; set; }
        public decimal TTLSalestax { get; set; }
        public string Scale { get; set; }
        public string UUID { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class SalesOrderBodyVOKeys
    {
        public const string BranchId = "BranchId";
        public const string UUID = "UUID";
    }
}