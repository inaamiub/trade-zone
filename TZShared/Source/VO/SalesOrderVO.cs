﻿using System;
using HatfShared;

namespace TZShared
{
    [MSSqlCollection("dbo", "SalesOrder")]
    public partial class SalesOrderVO : HBaseVO
    {
        public string BranchId { get; set; }
        public long OrderID { get; set; }
        public OrderType OrderType { get; set; }
        public string UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string CustomerID { get; set; }
        public string SalesmanID { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string DeliveryAddress { get; set; }
        public string CNIC { get; set; }
        public decimal Freight { get; set; }
        public decimal AdvanceCash { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsPrinted { get; set; }
        public string Remarks { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsCancel { get; set; }
        public string CancelReason { get; set; }
        public OrderDeliveryType DeliveryType { get; set; }
        public string Status { get; set; }
        public int UserNo { get; set; }
        public bool IsPosted { get; set; }
        public string UUID { get; set; }
        public bool IsRejected { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string AddressId { get; set; }
        
        [PropertyIgnore]
        public int NumberOfItems { get; set; }
        [PropertyIgnore]
        public int TotalPrice { get; set; }

        public string GetOrderId()
        {
            return OrderType + "-" + BranchId + "-" + OrderID;
        }
    }

    public class SalesOrderVOKeys
    {
        public const string BranchId = "BranchId";
        public const string UserId = "UserId";
        public const string OrderDate = "OrderDate";
        public const string OrderID = "OrderID";
        public const string UUID = "UUID";
    }
}