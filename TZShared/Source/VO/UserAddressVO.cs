﻿using HatfShared;

namespace TZShared
{
    [MSSqlCollection("dbo", "UserAddresses")]
    public partial class UserAddressVO : HBaseVO
    {
        public string BranchId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string CellNo { get; set; }
        public string UUID { get; set; }
    }

    public class UserAddressesVOKeys
    {
        public const string Id = "Id";
        public const string BranchId = "BranchId";
        public const string UUID = "UUID";
        public const string Name = "Name";
        public const string Address = "Address";
        public const string City = "City";
        public const string Country = "Country";
        public const string State = "State";
        public const string CellNo = "CellNo";
        public const string UserId = "UserId";
    }

    public static class UserAddressExtensions
    {
        public static string GetFullAddress(this UserAddressVO address)
        {
            if (address == null)
            {
                return string.Empty;
            }
            
            string fullAddress = address.Name;
            if (string.IsNullOrEmpty(address.Address) == false)
            {
                fullAddress += " - " + address.Address;
            }

            if (string.IsNullOrEmpty(address.City) == false)
            {
                fullAddress += ", " + address.City;
            }

            if (string.IsNullOrEmpty(address.State) == false)
            {
                fullAddress += ", " + address.State;
            }                
                
            if (string.IsNullOrEmpty(address.Country) == false)
            {
                fullAddress += ", " + address.Country;
            }

            return fullAddress;
        }
    }
}