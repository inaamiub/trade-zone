﻿using System;
using HatfShared;

namespace TZShared
{
    [MSSqlCollection("dbo", "UserInformation")]
    public class UserInformationVO : HBaseVO
    {
        public string BranchId { get; set; }
        public string Name { get; set; }
        public string CellNo { get; set; }
        public string Email { get; set; }
        public string AccountNo { get; set; }
        public string UUID { get; set; }
        public string SDKVersion { get; set; }
        public string Manufacturer { get; set; }
        public string Brand { get; set; }
        public string SDKReleaseVersion { get; set; }
        public string IMEI { get; set; }
        public DateTime? LocalDateTime { get; set; }
        public DateTime? ServerDateTime { get; set; }
        public UserType UserType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserRecordStatus RecordStatus { get; set; }
        
        [PropertyIgnore]
        public UserAddressVO UserAddressVO { get; set; }
    }

    public class UserInformationVOKeys
    {
        public const string Id = "Id";
        public const string BranchId = "BranchId";
        public const string Name = "Name";
        public const string CellNo = "CellNo";
        public const string City = "City";
        public const string Address = "Address";
        public const string Email = "Email";
        public const string AccountNo = "AccountNo";
        public const string UUID = "UUID";
        public const string UserName = "UserName";
        public const string Password = "Password";
        public const string RecordStatus = "RecordStatus";
        public const string UserType = "UserType";
    }
}