﻿using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;
using HatfCore;
using HatfShared;

namespace TZShared
{
    public class SalesOrderVOValidator : AbstractValidator<CreateNewOrderCommandPayload>
    {
        public SalesOrderVOValidator()
        {
            RuleFor(x => x.OrderDeliveryType).Custom((value, context) =>
            {
                if (Enum.IsDefined(typeof(OrderDeliveryType), value) == false)
                {
                    context.AddFailure("Invalid delivery type");
                }
            });
            RuleFor(x => x.AddressId).Custom((addressId, context) =>
            {
                CreateNewOrderCommandPayload instance = (CreateNewOrderCommandPayload) context.InstanceToValidate;
                if (instance.OrderDeliveryType == OrderDeliveryType.Deliver && string.IsNullOrEmpty(addressId))
                {
                    context.AddFailure("Address is required for delivery");
                }
            });
            RuleFor(x => x.PaymentMethod).Custom((value, context) =>
            {
                if (Enum.IsDefined(typeof(PaymentMethod), value) == false)
                {
                    context.AddFailure("Invalid payment method");
                }
            });
        }
        
    }

    public static class mdNewOrderExtensions
    {
        public static void ValidateNewOrder(this CreateNewOrderCommandPayload order, UserInformationVO userInformationVo)
        {
            List<ValidationFailure> validationErrors = new List<ValidationFailure>();
            
            if (userInformationVo.UserType != UserType.FullAccess &&
                userInformationVo.UserType != UserType.PublicAccess)
            {
                validationErrors.Add(new ValidationFailure("User", "Guest user(s) can not create orders. Please Login or Signup for orders"));
            }
            ValidationResult result = new SalesOrderVOValidator().Validate(order);
            if (result.IsValid == false || validationErrors.Count > 0)
            {
                validationErrors.AddRange(result.Errors);
                throw new HException(HatfErrorCodes.FormValidationError, validationErrors);
            }
        }   
    }
}