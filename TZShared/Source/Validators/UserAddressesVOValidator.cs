﻿using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;
using HatfCore;
using HatfShared;

namespace TZShared
{
    public class UserAddressesVOValidator : AbstractValidator<UserAddressVO>
    {
        public UserAddressesVOValidator()
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(UserInformationVOValidatorBase.NAME_MAXIMUM_LENGTH);
            RuleFor(x => x.CellNo).NotEmpty().MaximumLength(UserInformationVOValidatorBase.CELLNO_MAXIMUM_LENGTH);
            RuleFor(x => x.Address).NotEmpty().MaximumLength(UserInformationVOValidatorBase.ADDRESS_MAXIMUM_LENGTH);
            RuleFor(x => x.City).NotEmpty().MaximumLength(UserInformationVOValidatorBase.CITY_MAXIMUM_LENGTH);
            RuleFor(x => x.State).NotEmpty().MaximumLength(UserInformationVOValidatorBase.STATE_MAXIMUM_LENGTH);
        }
        
    }

    public static class UserAddressesVOValidatorExtensions
    {
        public static void ValidateUserAddress(this UserAddressVO userAddressVo, UserInformationVO userInformationVo)
        {
            List<ValidationFailure> validationErrors = new List<ValidationFailure>();
            
            if (userInformationVo.UserType != UserType.FullAccess &&
                userInformationVo.UserType != UserType.PublicAccess)
            {
                validationErrors.Add(new ValidationFailure("User", "Guest user(s) can not add address. Please Login or Signup to continue"));
            }
            
            ValidationResult result = new UserAddressesVOValidator().Validate(userAddressVo);
            if (result.IsValid == false || validationErrors.Count > 0)
            {
                validationErrors.AddRange(result.Errors);
                throw new HException(HatfErrorCodes.FormValidationError, validationErrors);
            }
        }   
    }
}