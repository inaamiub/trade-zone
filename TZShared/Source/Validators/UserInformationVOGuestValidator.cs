﻿using System;
using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Results;
using HatfCore;
using HatfShared;

namespace TZShared
{
    public class UserInformationVOValidatorBase : AbstractValidator<UserInformationVO>
    {
        public const int NAME_MAXIMUM_LENGTH = 30;
        public const int CELLNO_MAXIMUM_LENGTH = 12;
        public const int CITY_MAXIMUM_LENGTH = 20;
        public const int ADDRESS_MAXIMUM_LENGTH = 100;
        public const int STATE_MAXIMUM_LENGTH = 50;
        public const int UUID_MAXIMUM_LENGTH = 50;
        public const int EMAIL_MAXIMUM_LENGTH = 30;

        
        public const int USERNAME_MINIMUM_LENGTH = 6;
        public const int USERNAME_MAXIMUM_LENGTH = 32;
        public const string ALPHA_NUMERIC__REGEX = @"^[A-Za-z0-9_]+$";

        public const int PASSWORD_MINIMUM_LENGTH = 6;
        public const int PASSWORD_MAXIMUM_LENGTH = 32;
        public const string PASSWORD_SPECIAL_CHARS = @"\!\@\#\$\%\^\&\*\(\)\-\?";

        public static string PASSWORD_REGEX =
            "^((?=.*[A-Z]{1,})(?=.*[" + PASSWORD_SPECIAL_CHARS + "]{1,})(?=.*[0-9]{1,})(?=.*[a-z]{1,})).{" +
            PASSWORD_MINIMUM_LENGTH + "," + PASSWORD_MAXIMUM_LENGTH + "}$";

        public static string GetPasswordErrorMessage(UserInformationVO ui)
        {
            string[] errors = new []
            {
                "Password must fulfill the following requirements",
                $"\t1. Minimum {PASSWORD_MINIMUM_LENGTH} and maximum {PASSWORD_MAXIMUM_LENGTH} characters",
                "\t2. At-least one capital letter",
                "\t3. At-least one small letter",
                "\t4. At-least one number",
                $"\t5. At-least one special character '{PASSWORD_SPECIAL_CHARS.Replace(@"\", "")}'"
            };
            return string.Join(Environment.NewLine, errors);
        }

        protected void AddUserNameRule()
        {
            RuleFor(x => x.UserName).Length(USERNAME_MINIMUM_LENGTH, USERNAME_MAXIMUM_LENGTH).Custom(
                (userName, context) =>
                {
                    if (Regex.Matches(userName, ALPHA_NUMERIC__REGEX, RegexOptions.IgnoreCase).Count < 1)
                    {
                        context.AddFailure("'UserName' allows only Alphanumeric and underscore(_)");   
                    }
                });
        }
        
        protected void AddPasswordRules()
        {
            RuleFor(x => x.Password).Length(PASSWORD_MINIMUM_LENGTH, PASSWORD_MAXIMUM_LENGTH).Custom(
                (password, context) =>
                {
                    if (Regex.Matches(password, ALPHA_NUMERIC__REGEX, RegexOptions.IgnoreCase).Count < 1)
                    {
                        context.AddFailure($"'Password' must be alpha numeric with minimum {PASSWORD_MINIMUM_LENGTH} and maximum {PASSWORD_MAXIMUM_LENGTH} length");   
                    }
                });
        }
        
        protected void AddEmailRule()
        {
            RuleFor(x => x.Email).MaximumLength(EMAIL_MAXIMUM_LENGTH)
                .Custom((email, context) =>
                {
                    if (string.IsNullOrEmpty(email) || Utils.IsValidEmail(email) == false)
                    {
                        context.AddFailure("'Email' is invalid");
                    }
                });
        }
    }

    public class UserInformationVoEmailValidator : UserInformationVOValidatorBase {
        public UserInformationVoEmailValidator()
        {
            AddEmailRule();
        }

    }

    public class UserInformationVOBasicInfoValidator : UserInformationVoEmailValidator {
        public UserInformationVOBasicInfoValidator() {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(NAME_MAXIMUM_LENGTH);
            RuleFor(x => x.CellNo).NotEmpty().MaximumLength(CELLNO_MAXIMUM_LENGTH);
            RuleFor(x => x.UserAddressVO.Address).NotEmpty().MaximumLength(ADDRESS_MAXIMUM_LENGTH);
            RuleFor(x => x.UserAddressVO.City).NotEmpty().MaximumLength(CITY_MAXIMUM_LENGTH);
            RuleFor(x => x.UserAddressVO.State).MaximumLength(STATE_MAXIMUM_LENGTH)
                .Custom((value, context) =>
                {
                    if (string.IsNullOrEmpty(value) || HConfigManager.Get<TZSharedConfig>().States.Contains(value) == false)
                    {
                        context.AddFailure("'State/Province' is invalid");
                    }
                });
        }
    }

    public class UserInformationVOGuestValidator : UserInformationVOBasicInfoValidator {
        public UserInformationVOGuestValidator() {
            RuleFor(x => x.BranchId).NotEmpty();
            RuleFor(x => x.UUID).NotEmpty().MaximumLength(UUID_MAXIMUM_LENGTH);
            RuleFor(x => x.UserType).NotEmpty().IsInEnum().NotEqual(UserType.None);
        }

        
    }
    
    public class UserInformationVoPublicAccessValidator : UserInformationVOGuestValidator {
        public UserInformationVoPublicAccessValidator()
        {
            AddUserNameRule();
            AddPasswordRules();
        }

    }

    public class UserInformationVoUserNameValidator : UserInformationVOValidatorBase {
        public UserInformationVoUserNameValidator()
        {
            AddUserNameRule();
        }

    }

    public class UserInformationVoPasswordValidator : UserInformationVOValidatorBase {
        public UserInformationVoPasswordValidator()
        {
            AddPasswordRules();
        }

    }

    public class UserInformationVoFullAccessValidator : UserInformationVoPublicAccessValidator {
        public UserInformationVoFullAccessValidator() {
            RuleFor(x => x.AccountNo).NotEmpty().MaximumLength(ACCOUNTNO_MAXIMUM_LENGTH);
        }

        public const int ACCOUNTNO_MAXIMUM_LENGTH = 10;
    }

    public static partial class UserInformationVOExtensions
    {
        public static void Validate(this UserInformationVO userInformationVo)
        {
            AbstractValidator<UserInformationVO> validator;
            switch (userInformationVo.UserType)
            {
                case UserType.FullAccess:
                    validator = new UserInformationVoFullAccessValidator();
                    break;
                case UserType.PublicAccess:
                    validator = new UserInformationVoPublicAccessValidator();
                    break;
                case UserType.AsGuest:
                    validator = new UserInformationVOGuestValidator();
                    break;
                default:
                    throw new HException(TZErrorCodes.WRONG_USER_TYPE);
            }

            userInformationVo.ValidateInternal(validator);
        }

        private static void ValidateInternal(this UserInformationVO userInformationVo, AbstractValidator<UserInformationVO> validator)
        {
            ValidationResult result = validator.Validate(userInformationVo);
            if (result.IsValid == false)
            {
                throw new HException(HatfErrorCodes.FormValidationError, result.Errors);
            }
        }

        public static void ValidateBasicUserInfo(this UserInformationVO userInformationVo)
        {
            userInformationVo.ValidateInternal(new UserInformationVOBasicInfoValidator());
        }
        
    }
    
}
