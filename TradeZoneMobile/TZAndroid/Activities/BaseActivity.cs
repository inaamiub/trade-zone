﻿using Android.Gms.Ads;
using Android.Gms.Ads.Interstitial;
using Android.Runtime;
using AndroidX.AppCompat.App;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public abstract class BaseActivity : AppCompatActivity
    {
        private List<mdCustomMenuItem> customMenuItems = null;
        private int? menuLayoutResource = null;
        protected AndroidX.AppCompat.Widget.Toolbar toolbar;
        private CustomProgressBar progress = null;
        protected TzCommandExecutor _commandExecutor;
        protected DiContainer _container;
        protected TZMPrefs _prefs;
        protected IUtilityService _utilityService;

        public static Dictionary<ResourceType, Dictionary<string, int>> DynamicResourceCache =
            new Dictionary<ResourceType, Dictionary<string, int>>();
        
        protected abstract bool RequireLogin { get; }
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if (HM.Container != null)
            {
                _container = HM.Container;
                _container.RebindInstance(new HatfContext(this));
                _commandExecutor = _container.Resolve<TzCommandExecutor>();
                _prefs = _container.Resolve<TZMPrefs>();
                _utilityService = _container.Resolve<IUtilityService>();
            }
            TZM.ON_BRANCH_NOT_FOUND_ERROR = OnBranchNotFoundError;
            if (RequireLogin && TZM.IS_LOGGED_IN == false)
            {
                MoveToLogin();
            }
        }

        /// <summary>
        /// Initialize progress bar, check for update, ask for permission
        /// </summary>
        protected void AfterInitActivity(bool useProgressBar = true)
        {
            if (useProgressBar)
            {
                InitProgressBar();
            }

            Task.Run(InitMobileAds);
        }

        #region ProgressBar

        private void InitProgressBar()
        {
            try
            {
                ConstraintLayout mainLayout = FindViewById<ConstraintLayout>(Resource.Id.layoutMainBody);
                progress = new CustomProgressBar(this, mainLayout);
            }
            catch
            {
                // Ignore exception
            }
        }

        protected void HideProgressAndShowAlert(string message)
        {
            HideProgress();
            AlertBox(this, message);
        }

        protected void ShowProgress()
        {
            RunInvokeRequired((object param) =>
            {
                progress?.Show();
            });
        }

        protected void HideProgress()
        {
            RunInvokeRequired((object param) =>
            {
                progress?.Hide();
            });
        }

        #endregion

        public void ExitApplication()
        {
            Intent intent = new Intent(this, typeof(SplashActivity));
            intent.SetFlags(ActivityFlags.ClearTask | ActivityFlags.ClearTop);
            intent.PutExtra(Constants.Keys.EXIT, true);
            StartActivity(intent);
            Finish();
        }

        #region Options Menu

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            if (menuLayoutResource != null)
            {
                MenuInflater.Inflate(menuLayoutResource ?? Resource.Menu.toolbar_menu_one_item, menu);
                foreach (mdCustomMenuItem v in customMenuItems ?? new List<mdCustomMenuItem>())
                {
                    if (v.MenuItemId != null)
                    {
                        IMenuItem item = (IMenuItem) menu.FindItem(v.MenuItemId ?? 0);
                        item.SetIcon(v.DrawableResource ?? 0);
                        if (v.ActionView != null)
                        {
                            View? view = (View) v.ActionView;
                            view.Click += (sender, args) => { OnOptionsItemSelected(item); };
                            item.SetActionView(view);
                        }
                    }
                }
            }

            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            mdCustomMenuItem cmi = customMenuItems?.FirstOrDefault(m => m.MenuItemId == item.ItemId);
            if (cmi != null)
            {
                CallCustomMenuItemEvent(cmi);
            }

            return base.OnOptionsItemSelected(item);
        }

        #endregion

        #region Toolbar

        private void CallCustomMenuItemEvent(mdCustomMenuItem cmi)
        {
            if (cmi != null)
            {
                if (cmi.ClickedEvent != null)
                {
                    cmi.ClickedEvent(cmi.Params);
                }
            }
        }

        public void setIconicTitleBar(String actionBarTitle, int? navIcon = null,
            EventHandler<AndroidX.AppCompat.Widget.Toolbar.NavigationClickEventArgs> navClickEventHandler = null, int? logoDrawableResource = null,
            int? menuResource = null, List<mdCustomMenuItem> menuItems = null)
        {
            toolbar = FindViewById<AndroidX.AppCompat.Widget.Toolbar>(Resource.Id.custom_toolbar);
            if (toolbar != null)
            {
                if (string.IsNullOrWhiteSpace(actionBarTitle))
                {
                    actionBarTitle = HConfigManager.Get<TZMConfig>().ApplicationTitle;
                }

                toolbar.Title = actionBarTitle;

                SetSupportActionBar(toolbar);
                
                if (navClickEventHandler != null)
                {
                    toolbar.NavigationClick += navClickEventHandler;
                }

                if (navIcon == null)
                {
                    navIcon = DynamicResourceCache[ResourceType.Drawable][HConfigManager.Get<TZMConfig>().LogoSmall];
                }

                toolbar.SetNavigationIcon(navIcon ?? 0);

                if (logoDrawableResource != null)
                {
                    if (logoDrawableResource == 0)
                    {
                        logoDrawableResource = DynamicResourceCache[ResourceType.Drawable][HConfigManager.Get<TZMConfig>().LogoSmall];
                    }

                    toolbar.SetLogo(logoDrawableResource ?? 0);
                }

                customMenuItems = menuItems;
                menuLayoutResource = menuResource;
            }
        }

        private void BackButtonClick(object sender, EventArgs e)
        {
            Finish();
        }

        public void setBackBtnTitleBar(String actionBarTitle, int? menuResource = null,
            List<mdCustomMenuItem> menuItems = null)
        {
            setIconicTitleBar(actionBarTitle, Resource.Drawable.ic_arrow_back_primary_text_24dp, BackButtonClick, null,
                menuResource, menuItems);
        }

        public void setSimpleTitleBar(String actionBarTitle)
        {
            setBackBtnTitleBar(actionBarTitle);
        }

        #endregion

        public void RunInvokeRequired(Action<object> function, object parameters = null)
        {
            if (Looper.MyLooper() != Looper.MainLooper)
            {
                RunOnUiThread(() => { function(parameters); });
            }
            else
            {
                function(parameters);
            }
        }

        public void LoadInfoActivity(object param)
        {
            // For testing branch id not found
            // if (HM.Container.TryResolve(out TZMPrefs pref))
            // {
            //     pref.SetAccessToken("8FF712D4-279E-41B9-8EF0-BEC9872EB023");
            // }

            Intent intent = new Intent(this, typeof(InfoActivity));
            try
            {
                this.StartActivity(intent);
            }
            catch (Exception ex)
            {
                NotificationUtils.UserToast(this, ex.Message);
            }
        }

        public Android.App.AlertDialog AlertBox(string message, string title = null,
            string neutralButton = null, Action<object> neutralButtonClicked = null,
            string positiveButton = null, Action<object> positiveButtonClicked = null,
            string negativeButton = null, Action<object> negativeButtonClicked = null,
            object callbackdata = null)
        {
            return AlertBox(this, message, title, neutralButton, neutralButtonClicked, positiveButton,
                positiveButtonClicked, negativeButton, negativeButtonClicked, callbackdata);
        }
        public Android.App.AlertDialog AlertBox(Context context, string message, string title = null,
            string neutralButton = null, Action<object> neutralButtonClicked = null,
            string positiveButton = null, Action<object> positiveButtonClicked = null,
            string negativeButton = null, Action<object> negativeButtonClicked = null,
            object callbackdata = null)
        {
            Android.App.AlertDialog dialog = null;
            RunInvokeRequired((object param) =>
            {
                dialog = Utils.AlertBox(context, message, title, neutralButton, neutralButtonClicked,
                    positiveButton, positiveButtonClicked,
                    negativeButton, negativeButtonClicked, callbackdata);
            });
            return dialog;
        }

        #region Ad Related

        private AdView? _bannerAdView;
        private InterstitialAd? _interstitialAd;

        private void InitMobileAds()
        {
            if (HatfMobile.HatfMobile.MobileAdInitialized)
            {
                return;
            }

            try
            {
                MobileAds.Initialize(this);
                HatfMobile.HatfMobile.MobileAdInitialized = true;
            }
            catch (Exception ex)
            {
                HatfMobile.HatfMobile.MobileAdInitialized = false;
                HLogger.Error(ex, "Exception Occured while initializing ads");
                NotificationUtils.DeveloperToast(this, ex.Message);
            }
        }
        
        protected void InitBanner(int resourceId)
        {
            _bannerAdView = FindViewById<AdView>(resourceId);
            if (_bannerAdView != null)
            {
                RequestNewAd(_bannerAdView);
            }
        }

        protected void InitInterstitialAd()
        {
            // Destroy previous interstitial ad
            _interstitialAd?.Dispose();
            _interstitialAd = null;
            
            // Request a new interstitial ad
            AdRequest? adRequest = new AdRequest.Builder().Build();
            string interstitialAdUnitId = GetString(Resource.String.interstitial_ad_unit_id);
            InterstitialAd.Load(this, interstitialAdUnitId, adRequest, new InterstitialAdListener(this));
        }

        private void RequestNewInterstitial()
        {
            RequestNewAd(_interstitialAd);
        }

        protected void RequestNewAd(object? adUnit)
        {
            if (adUnit is AdView adView)
            {

                AdRequest? adRequest = new AdRequest.Builder().Build();
                adView.LoadAd(adRequest);
            }
            else if (adUnit is InterstitialAd)
            {
                InitInterstitialAd();
            }
        }

        protected virtual void OnAdClosed()
        {

        }

        protected bool IsInterstitialAdLoaded()
        {
            return _interstitialAd != null;
        }

        protected void ShowInterstitialAd()
        {
            if (_interstitialAd != null)
            {
                _interstitialAd.Show(this);
            }
            else
            {
                OnAdClosed();
            }
        }

        private class InterstitialAdListener : global::Android.Gms.Ads.Interstitial.InterstitialAdLoadCallback
        {
            private readonly BaseActivity _that;

            // Start hack code
            private static Delegate cb_onAdLoaded;

            private static Delegate GetOnAdLoadedHandler()
            {
                if (cb_onAdLoaded is null)
                {
                    cb_onAdLoaded = JNINativeWrapper.CreateDelegate((Action<IntPtr, IntPtr, IntPtr>)n_onAdLoaded);
                }
                return cb_onAdLoaded;
            }

            private static void n_onAdLoaded(IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
            {
                InterstitialAdLoadCallback thisobject = GetObject<InterstitialAdLoadCallback>(jnienv, native__this, JniHandleOwnership.DoNotTransfer);
                global::Android.Gms.Ads.Interstitial.InterstitialAd resultobject = GetObject<global::Android.Gms.Ads.Interstitial.InterstitialAd>(native_p0, JniHandleOwnership.DoNotTransfer);
                thisobject.OnAdLoaded(resultobject);
            }

            [Register("onAdLoaded", "(Lcom/google/android/gms/ads/interstitial/InterstitialAd;)V", "GetOnAdLoadedHandler")]
            public virtual void OnAdLoaded(global::Android.Gms.Ads.Interstitial.InterstitialAd interstitialAd)
            {
                _that._interstitialAd = interstitialAd;
                base.OnAdLoaded(interstitialAd);
            }
            
            // End hack code
            
            public InterstitialAdListener(BaseActivity t)
            {
                _that = t;
            }

            // Uncomment this code once the hack is fixed by xamarin
            // public override void OnAdLoaded(Object p0)
            // {
            //     _that._interstitialAd = (InterstitialAd)p0;
            //     base.OnAdLoaded(p0);
            // }

            public override void OnAdFailedToLoad(LoadAdError p0)
            {
                _that._interstitialAd?.Dispose();
                _that._interstitialAd = null;
                _that.RequestNewInterstitial();
                // _that.OnAdClosed();
            }
        }

        protected override void OnPause()
        {
            _bannerAdView?.Pause();
            base.OnPause();
        }

        protected override void OnResume()
        {
            base.OnResume();

            _bannerAdView?.Resume();
            RequestNewInterstitial();
        }

        protected override void OnDestroy()
        {
            _bannerAdView?.Destroy();
            _interstitialAd?.Dispose();
            _interstitialAd = null;
            base.OnDestroy();
        }

        #endregion

        protected void Logout(object param)
        {
            TZM.FORCE_LOGIN = false;
            if (HM.Container.TryResolve(out TZMPrefs pref))
            {
                pref.SetAccessToken("");
            }

            HM.ResetUser();
            Intent intent = new Intent(this, typeof(LoginActivity));
            intent.AddFlags(ActivityFlags.NoHistory);
            StartActivity(intent);
            Finish();
        }

        private void OnBranchNotFoundError()
        {
            RunInvokeRequired(_ => Logout(null));
        }

        protected void MoveToLogin()
        {
            _utilityService.MoveToLogin(this);
        }

        protected void MoveToMain()
        {
            _utilityService.MoveToMain(this);
        }
    }

}