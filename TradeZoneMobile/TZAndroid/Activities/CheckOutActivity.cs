﻿using Android.Net.Wifi;
using Android.Runtime;
using Google.Android.Material.Button;
using PopupMenu = AndroidX.AppCompat.Widget.PopupMenu;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class CheckOutActivity : BaseActivity
    {
        private RadioGroup _rgDeliveryType, _rgUserAddresses, _rgPaymentType;
        private MaterialButton _mbAddNewAddress, _mbPostOrder;
        private ConstraintLayout _clDeliveryAddress;
        private TextView _tvTotalItems, _tvTotalPrice;
        private EditText _etRemarks;
        
        private ShoppingCartSystem _shoppingCartSystem;
        private IUtilityService _utilityService;
        
        private Dictionary<int, UserAddressVO> _userAddressesCache;
        private Dictionary<int, PaymentMethod> _paymentMethodsCache;
        private Dictionary<int, OrderDeliveryType> _deliveryTypeCache;
        private string _uuid = HatfShared.Utils.NewShortGuid();
        private mdProductsWithStock _allProducts;
        protected override bool RequireLogin => true;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.check_out);

            setBackBtnTitleBar("Check Out", Resource.Menu.toolbar_menu_one_item);
            
            Init();
        }

        private void Init()
        {
            UserInformationVO? userInformationVo = _container.Resolve<UserInformationVO>();
            if (userInformationVo.UserType == UserType.AsGuest)
            {
                AlertBox(this, TZErrorMessages.GetErrorMessage(TZErrorCodes.GUEST_CAN_NOT_CREATE_ORDER), "Alert", "OK", _ =>
                {
                    RunInvokeRequired(_ => Finish());
                });
            }

            // Init shopping cart system
            _shoppingCartSystem = HM.Container.Resolve<ShoppingCartSystem>();
            _shoppingCartSystem.AddOnUpdatedEventListener(OnCartUpdated);

            if (_container.TryResolve<mdProductsWithStock>(out mdProductsWithStock? products) == false)
            {
                NotificationUtils.UserToast(this, "Sorry! Products not found");
                Finish();
                return;
            }

            _allProducts = products;

            if (_shoppingCartSystem?.TotalQuantity < 1)
            {
                AlertBox(this, "No item exists in the cart", "Alert", "OK", _ =>
                {
                    RunInvokeRequired(_ => Finish());
                });
                return;
            }

            _utilityService = HM.Container.Resolve<IUtilityService>();
            TZMConfig? tzmConfig = HConfigManager.Get<TZMConfig>();

            _clDeliveryAddress = FindViewById<ConstraintLayout>(Resource.Id.clDeliveryAddress);
            _etRemarks = FindViewById<EditText>(Resource.Id.etRemarks);
            _tvTotalItems = FindViewById<TextView>(Resource.Id.tvTotalItems);
            _tvTotalPrice = FindViewById<TextView>(Resource.Id.tvTotalPrice);
            UpdateProductsSummary();
            
            // Delivery type
            _deliveryTypeCache = new Dictionary<int, OrderDeliveryType>();
            foreach (OrderDeliveryType orderDeliveryType in Enum.GetValues(typeof(OrderDeliveryType)))
            {
                _deliveryTypeCache.Add(Utils.GetRandomNumber(), orderDeliveryType);
            }
            
            _rgDeliveryType = FindViewById<RadioGroup>(Resource.Id.rgDeliveryType);
            _rgDeliveryType.CheckedChange += DeliveryTypeChanged;
            int selectedDeliveryType = 0;
            foreach ((int id, OrderDeliveryType orderDeliveryType) in _deliveryTypeCache)
            {
                RadioButton radioButton = new RadioButton(this) {Text = orderDeliveryType.ToString(), Id = id};
                if (orderDeliveryType == tzmConfig.DefaultOrderDeliveryType)
                {
                    selectedDeliveryType = id;
                }
                _rgDeliveryType.AddView(radioButton);
            }

            // Set selected value
            if (selectedDeliveryType != 0)
            {
                _rgDeliveryType.Check(selectedDeliveryType);
            }
            
            // User Addresses
            _rgUserAddresses = FindViewById<RadioGroup>(Resource.Id.rgUserAddresses);
            LoadAddresses();
            
            // Payment Method
            _paymentMethodsCache = new Dictionary<int, PaymentMethod>();
            foreach (PaymentMethod paymentMethod in Enum.GetValues(typeof(PaymentMethod)))
            {
                _paymentMethodsCache.Add(Utils.GetRandomNumber(), paymentMethod);
            }

            _rgPaymentType = FindViewById<RadioGroup>(Resource.Id.rgPaymentType);
            int selectedPaymentType = 0;
            foreach ((int id, PaymentMethod paymentMethod) in _paymentMethodsCache)
            {
                RadioButton radioButton = new RadioButton(this) {Text = paymentMethod.ToString(), Id = id};
                if (paymentMethod == tzmConfig.DefaultPaymentMethod)
                {
                    selectedPaymentType = id;
                }
                _rgPaymentType.AddView(radioButton);
            }

            // Set selected value
            if (selectedPaymentType != 0)
            {
                _rgPaymentType.Check(selectedPaymentType);
            }

            _mbAddNewAddress = FindViewById<MaterialButton>(Resource.Id.mbAddNewAddress);
            _mbAddNewAddress.Click += AddNewAddressOnClick;
            _mbPostOrder = FindViewById<MaterialButton>(Resource.Id.mbPostOrder);
            _mbPostOrder.Click += BtnPostOrderClicked;
            
            AfterInitActivity();;
        }

        private void DeliveryTypeChanged(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            if (_deliveryTypeCache.ContainsKey(e.CheckedId) == false)
            {
                AlertBox(this, "Invalid delivery type", "Error");
                return;
            }

            if (_deliveryTypeCache[e.CheckedId] == OrderDeliveryType.Deliver)
            {
                _clDeliveryAddress.Visibility = ViewStates.Visible;
            }
            else
            {
                _clDeliveryAddress.Visibility = ViewStates.Gone;
            }
        }

        private void BtnPostOrderClicked(object sender, EventArgs e)
        {
            try
            {
                if (_deliveryTypeCache.ContainsKey(_rgDeliveryType.CheckedRadioButtonId) == false)
                {
                    AlertBox(this, "Invalid delivery type", "Error");
                    return;
                }

                if (_paymentMethodsCache.ContainsKey(_rgPaymentType.CheckedRadioButtonId) == false)
                {
                    AlertBox(this, "Invalid payment method", "Error");
                    return;
                }

                // Validate order
                CreateNewOrderCommandPayload order = new CreateNewOrderCommandPayload()
                {
                    UUID = _uuid,
                    PaymentMethod = _paymentMethodsCache[_rgPaymentType.CheckedRadioButtonId],
                    OrderDeliveryType = _deliveryTypeCache[_rgDeliveryType.CheckedRadioButtonId],
                    Remarks = _etRemarks.Text
                };

                if (order.OrderDeliveryType == OrderDeliveryType.Deliver)
                {
                    if (_userAddressesCache.ContainsKey(_rgUserAddresses.CheckedRadioButtonId) == false)
                    {
                        AlertBox(this, "Invalid delivery address", "Error");
                        return;
                    }

                    order.AddressId = _userAddressesCache[_rgUserAddresses.CheckedRadioButtonId].Id;
                }

                order.Products = _shoppingCartSystem.GetAllItems().Where(m => m.Quantity > 0).ToDictionary(m => m.ItemId, m => m.Quantity);

                order.ValidateNewOrder(HM.Container.Resolve<UserInformationVO>());

                AlertBox(this, "Are you sure to proceed with this order?", "Question", positiveButton: "Yes",
                    positiveButtonClicked: PostOrderFinally, neutralButton: "No", callbackdata: order);

            }
            catch (HException ex)
            {
                HideProgressAndShowAlert(ex.Message);
            }
            catch
            {
                HideProgressAndShowAlert(HatfErrorMessages.GetErrorMessage(HatfErrorCodes.UnknownError));
            }
        }

        private void PostOrderFinally(object obj)
        {
            // Post order
            CreateNewOrderCommandPayload order = (CreateNewOrderCommandPayload) obj;

            Task.Run(async () =>
            {
                ShowProgress();
                bool success = false;
                try
                {
                    GetUserOrderHistoryCommandResult? result =
                        await _commandExecutor.ExecuteCommand<GetUserOrderHistoryCommandResult>(order);
                    mdOrderHistory orderHistory = new mdOrderHistory()
                    {
                        Orders = result.Orders ?? []
                    };
                    _container.RebindInstance(orderHistory);
                    success = true;
                }
                catch (HException e)
                {
                    if (e.ErrorCode == TZErrorCodes.ORDER_ALREADY_CREATED)
                    {
                        success = true;
                        if (_container.HasBinding<mdOrderHistory>())
                        {
                            _container.Unbind<mdOrderHistory>();
                        }
                    }
                    else
                    {
                        AlertBox(e.Message, "Error", neutralButton: "OK");   
                    }
                }
                catch (Exception e)
                {
                    AlertBox(e.Message, "Error", neutralButton: "OK");
                }
                HideProgress();
                
                if (success)
                {
                    RunInvokeRequired(_ =>
                    {
                        // Clear cart from local db
                        _shoppingCartSystem.ClearCart();
                    });
                    AlertBox(this, "Order posted successfully", "Success", "Ok", _ =>
                    {
                        Intent intent = new Intent(this, typeof(OrderHistoryActivity));
                        StartActivity(intent);
                        SetResult(Result.Ok);
                        Finish();   
                    });
                }
            });
        }

        private void RemoveAllChildren(RadioGroup radioGroup)
        {
            int length = radioGroup.ChildCount;
            if (length < 1)
            {
                return;
            }

            for (int i = length - 1; i >= 0; i--)
            {
                View? view = radioGroup.GetChildAt(i);
                if (view is RadioButton)
                {
                    radioGroup.RemoveViewAt(i);
                }
            }

            radioGroup.Selected = false;
        }
        
        private void OnCartUpdated(string arg1, uint arg2, long arg3)
        {
            // Update the price if shown
            UpdateProductsSummary();
        }

        private void UpdateProductsSummary()
        {
            List<ShoppingCartItemModel>? items = _shoppingCartSystem.GetAllItems();
            _tvTotalItems.Text = items.Count.ToString();

            decimal totalPrice = items.Select(m => (m.ProductDetails?.RetailPrice ?? 0) * m.Quantity).Sum(); 
            _tvTotalPrice.Text = "RS " + Math.Round(totalPrice, 2);
        }
        
        private void LoadAddresses(string selectedAddressId = "")
        {
            Task.Run(async () =>
            {
                // Get all user addresses
                try
                {
                    // Local addresses cache is empty
                    if (_userAddressesCache == null)
                    {
                        // Check in container
                        if (HM.Container.TryResolve(out List<UserAddressVO> userAddresses,
                            ContainerConstants.ALL_USER_ADDRESSES) == false)
                        {
                            ShowProgress();
                            try
                            {
                                GetAllUserAddressesCommandResult? commandResult =
                                    await _commandExecutor.ExecuteVoidCommand<GetAllUserAddressesCommandResult>(
                                        nameof(GetAllUserAddressesCommand));

                                userAddresses = [..commandResult.Addresses];
                                _container.BindInstanceWithId(userAddresses, ContainerConstants.ALL_USER_ADDRESSES);
                                _container.RebindInstanceWithId(commandResult.LastUsedAddressId, ContainerConstants.USER_LAST_USED_ADDRESS_ID);
                            }
                            catch (Exception e)
                            {
                                HLogger.Error(e, "Exception occured while executing http request");
                                NotificationUtils.DeveloperToast(this, e);
                            }
                        }

                        // Check again from container cache
                        if (HM.Container.TryResolve(out userAddresses, ContainerConstants.ALL_USER_ADDRESSES))
                        {

                            // Add to user addresses dict
                            _userAddressesCache = new Dictionary<int, UserAddressVO>();
                            foreach (UserAddressVO userAddressVO in userAddresses)
                            {
                                _userAddressesCache.Add(Utils.GetRandomNumber(), userAddressVO);

                            }
                        }
                        else
                        {
                            NotificationUtils.UserToast(this, "Unable to user addresses");
                        }
                    }
                }
                catch (Exception e)
                {
                    NotificationUtils.UserToast(this, "Unable to user addresses");
                }
                finally
                {
                    RunInvokeRequired(_ =>
                    {
                        // Remove all children
                        RemoveAllChildren(_rgUserAddresses);

                        if (_userAddressesCache == null)
                        {
                            return;
                        }

                        foreach ((int buttonId, UserAddressVO userAddressesVo) in _userAddressesCache)
                        {
                            RadioButton radioButton = new RadioButton(this)
                            {
                                Text = userAddressesVo.GetFullAddress(), Id = buttonId, LongClickable = true
                            };
                            radioButton.LongClick += OnLongClickAddress;
                            _rgUserAddresses.AddView(radioButton);
                        }

                        HM.Container.TryResolve(out string userLastUsedAddressId,
                            ContainerConstants.USER_LAST_USED_ADDRESS_ID);

                        if (string.IsNullOrEmpty(selectedAddressId))
                        {
                            selectedAddressId = userLastUsedAddressId;
                        }
                        // Set selected address
                        if (_userAddressesCache.Any(m => m.Value.Id == selectedAddressId))
                        {
                            _rgUserAddresses.Check(_userAddressesCache.First(m => m.Value.Id == selectedAddressId).Key);
                        }

                        HideProgress();
                    });
                }
            });
        }

        private void OnLongClickAddress(object sender, View.LongClickEventArgs e)
        {
            RadioButton radioButton = (RadioButton) sender;
            ShowEditAddressMenu(radioButton);
        }

        private void ShowEditAddressMenu(View view)
        {
            string radioButtonIdKey = "RADIO_BUTTON_ID";
            PopupMenu popupMenu = new AndroidX.AppCompat.Widget.PopupMenu(this, view);

            Intent intent = new Intent();
            intent.PutExtra(radioButtonIdKey, view.Id);
            IMenuItem menuItem = popupMenu.Menu.Add("Update Address").SetIcon(Resource.Drawable.ic_baseline_edit_note_24dp).SetIntent(intent);
            
            popupMenu.MenuItemClick += (object sender, AndroidX.AppCompat.Widget.PopupMenu.MenuItemClickEventArgs e) =>
            {
                int radioButtonId = e?.Item?.Intent?.Extras?.GetInt(radioButtonIdKey, 0) ?? 0;
                if (_userAddressesCache.ContainsKey(radioButtonId) == false)
                {
                    AlertBox(this, "Something went wrong, address not found in local cache", "Error");
                    return;
                }
                AddOrUpdateAddress(_userAddressesCache[radioButtonId].Id);
            };
            // Show the popup menu.
            popupMenu.Show();
        }

        private void AddOrUpdateAddress(string addressId)
        {
            Intent intent = new Intent(this, typeof(UserAddressActivity));
            intent.PutExtra(UserAddressActivity.USER_ADDRESS_ID, addressId);
            StartActivityForResult(intent, 0);
        }
        
        private void AddNewAddressOnClick(object sender, EventArgs e)
        {
            AddOrUpdateAddress("");
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            if (resultCode == Result.Ok)
            {
                string updatedAddressId = data?.Extras?.GetString(UserAddressActivity.USER_ADDRESS_ID, string.Empty);
                
                // Force user addresses to redraw
                _userAddressesCache = null;
                
                // Reload user addresses
                LoadAddresses(updatedAddressId);
            }
        }

        protected override void OnResume()
        {
            // Update the price if shown
            UpdateProductsSummary();
            base.OnResume();
        }

        public override void Finish()
        {
            _shoppingCartSystem?.RemoveOnUpdatedEventListener(OnCartUpdated);
            base.Finish();
        }

    }

}