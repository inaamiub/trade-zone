﻿namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class CompanyDetailsActivity : BaseActivity
    {
        private CompanyModel com;
        private TextView tvcdCompanyName, tvcdOwnerName, tvcdPhone, tvcdCell, tvcdEmail, tvcdAddress;//, tvcdBankName, tvcdAccTitle, tvcdAccNum;
        private ImageView imgcdCompanyLogo;
        private RecyclerView rvcdBankAccounts;

        protected override bool RequireLogin => false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            try
            {
                SetContentView(Resource.Layout.company_details);
                setBackBtnTitleBar("", Resource.Menu.toolbar_menu_one_item, new List<mdCustomMenuItem>()
                {
                    new mdCustomMenuItem()
                    {
                        ClickedEvent = LoadInfoActivity,
                        DrawableResource = Resource.Drawable.ic_help_outline_white_24dp,
                    }
                });
                intIt();
            }
            catch (Exception ex)
            {
                NotificationUtils.UserToast(this, ex.ToString());
            }
        }

        private void intIt()
        {
            try
            {
                tvcdCompanyName = (TextView)FindViewById(Resource.Id.tvcdCompanyName);
                tvcdOwnerName = (TextView)FindViewById(Resource.Id.tvcdOwnerName);
                tvcdPhone = (TextView)FindViewById(Resource.Id.tvcdPhone);
                tvcdCell = (TextView)FindViewById(Resource.Id.tvcdCell);
                tvcdEmail = (TextView)FindViewById(Resource.Id.tvcdEmail);
                tvcdAddress = (TextView)FindViewById(Resource.Id.tvcdAddress);
                imgcdCompanyLogo = (ImageView)FindViewById(Resource.Id.imgcdCompanyLogo);


                rvcdBankAccounts = (RecyclerView)FindViewById(Resource.Id.rvcdBankAccounts);
                rvcdBankAccounts.SetLayoutManager(new LinearLayoutManager(this));

                if (Intent.Extras.ContainsKey("comobj"))
                {
                    string comobj = Intent.Extras.GetString("comobj");
                    com = JsonUtils.Deserialize<CompanyModel>(comobj);
                    if (com != default(CompanyModel))
                    {
                        tvcdCompanyName.Text = com.CompanyName;
                        tvcdOwnerName.Text = com.OwnerName;
                        tvcdPhone.Text = string.Join("\n", com.CompanyPhone);
                        tvcdCell.Text = string.Join("\n", com.CompanyCell);
                        tvcdEmail.Text = com.CompanyEmail;
                        tvcdAddress.Text = com.CompanyAddress;
                        imgcdCompanyLogo.SetImageResource(
                            DynamicResourceCache[ResourceType.Drawable][HConfigManager.Get<TZMConfig>().BannerImage]);
                        
                        rvcdBankAccounts.SetAdapter(new BankAccountsAdapter(this, com.BankAccounts));
                    }
                }

            }
            catch (Exception ex)
            {
                NotificationUtils.DeveloperToast(this, "Exp Init\n" + ex.ToString());
            }
        }

    }
}