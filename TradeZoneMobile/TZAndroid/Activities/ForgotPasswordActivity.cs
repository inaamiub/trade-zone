﻿using FluentValidation.Results;

namespace TZAndroid;

[Activity]

public class ForgotPasswordActivity : BaseActivity
{
    private EditText etEmail;
    private AppCompatButton btnSend;
    private ImageButton ibBackForgotPassword;
    
    protected override bool RequireLogin => false;
    protected override void OnCreate(Bundle savedInstanceState)
    {
        base.OnCreate(savedInstanceState);
        
        Init();
    }

    private void Init()
    {
        SetContentView(Resource.Layout.forgot_password_activity);

        etEmail = FindViewById<EditText>(Resource.Id.etEmail);
        btnSend = FindViewById<AppCompatButton>(Resource.Id.btnSend);
        ibBackForgotPassword = FindViewById<ImageButton>(Resource.Id.ibBackForgotPassword);

        AfterInitActivity();
        // Initialize listeners
        btnSend.Click += BtnSendOnClick;
        ibBackForgotPassword.Click += IbBackForgotPasswordOnClick;
        
        // Set fouc on first edit text
        etEmail.RequestFocus();
    }

    private void BtnSendOnClick(object sender, EventArgs e)
    {
        string email = etEmail.Text;
        ValidationResult? validationResult = new UserInformationVoEmailValidator().Validate(new UserInformationVO
        {
            Email = email
        });

        if (validationResult.IsValid == false)
        {
            HM.Container.Resolve<IUtilityService>()
                .AlertBox(string.Join("\n", validationResult.Errors.Select(m => m.ErrorMessage)));
            return;
        }
        
        // Send account verification email
        ShowProgress();
        Task.Run(async () =>
        {
            try
            {
                await _commandExecutor.ExecuteVoidCommand(new SendPasswordRecoveryEmailCommandPayload()
                {
                    Email = email,
                    ClientId = HConfigManager.Get<AuthConfig>().ClientId,
                    DeviceId = HM.Container.Resolve<IUtilityService>().GetDeviceId(),
                    BranchId = HConfigManager.Get<TZMConfig>().BranchId
                });
                RunInvokeRequired(_ =>
                {
                    Intent intent = new Intent(this, typeof(VerificationActivity));
                    intent.PutExtra(VerificationActivity.DISABLE_RESEND_KEY, true);
                    intent.PutExtra(VerificationActivity.VERIFICATION_TYPE_KEY, TokenType.PasswordRecoveryToken.ToString());
                    intent.PutExtra(VerificationActivity.VERIFICATION_EMAIL_KEY, email);
                    intent.PutExtra(VerificationActivity.SHOW_TRY_LATER_KEY, false);
                    StartActivity(intent);
                    HideProgress();
                });
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
                AlertBox(e.Message, "Error", neutralButton: "OK");
            }
        });
    }

    private void IbBackForgotPasswordOnClick(object sender, EventArgs e)
    {
        Finish();
    }
}