﻿
namespace TZAndroid
{
    [Activity]
    public class IconicActivity : Activity
    {
        private Activity context;
        private String BaseUrl;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            context = this;
            try
            {
                RequestWindowFeature(WindowFeatures.CustomTitle);
                SetContentView(Resource.Layout.activity_main);
                Window.SetFeatureInt(WindowFeatures.CustomTitle, Resource.Layout.iconic_titlebar);
            }
            catch (Exception ex)
            {
                NotificationUtils.UserToast(this, ex.ToString());
            }
        }
    }
}