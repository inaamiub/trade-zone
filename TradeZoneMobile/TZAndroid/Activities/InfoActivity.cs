﻿namespace TZAndroid
{
    [Activity(Label = "Aarib Soft", Theme = "@style/NoActionBar")]
    public class InfoActivity : BaseActivity
    {
        //RecyclerView rvInfo;
        private ImageView imgInfoCompanyLogo;
        private List<NameValueModel> list;
        private TextView tvInfoDevelopedBy, tvInfoSoftwareEngineer, tvInfoCell, tvInfoEmail, tvInfoCity, tvInfoVersion;
        protected override bool RequireLogin => false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            try
            {
                SetContentView(Resource.Layout.info_activity);
                setBackBtnTitleBar("Aarib Soft", null);
                intIt();
            }
            catch (Exception ex)
            {
                NotificationUtils.DeveloperToast(this, "Exp Init\n" + ex.ToString());
            }
        }

        private void intIt()
        {
            try
            {
                //rvInfo = (RecyclerView)FindViewById(Resource.Id.rvInfo);
                //rvInfo.SetLayoutManager(new LinearLayoutManager(this));
                imgInfoCompanyLogo = (ImageView)FindViewById(Resource.Id.imgInfoCompanyLogo);
                tvInfoDevelopedBy = (TextView)FindViewById(Resource.Id.tvInfoDevelopedBy);
                tvInfoSoftwareEngineer = (TextView)FindViewById(Resource.Id.tvInfoSoftwareEngineer);
                tvInfoCell = (TextView)FindViewById(Resource.Id.tvInfoCell);
                tvInfoEmail = (TextView)FindViewById(Resource.Id.tvInfoEmail);
                tvInfoCity = (TextView)FindViewById(Resource.Id.tvInfoCity);
                tvInfoVersion = (TextView)FindViewById(Resource.Id.tvInfoVersion);

                imgInfoCompanyLogo.SetImageResource(Resource.Drawable.trade_zone_logo);
                tvInfoDevelopedBy.Text = "Aarib Soft";
                tvInfoSoftwareEngineer.Text = "Irfan Siddiqui";
                tvInfoCell.Text = "03336129006";
                tvInfoEmail.Text = @"irfanfraz786@yahoo.com
irfanfraz@gmail.com
irfanfraz@hotmail.com";
                tvInfoCity.Text = "Multan";
                tvInfoVersion.Text = HM.Container.Resolve<IUtilityService>().GetAppVersionName();
            }
            catch (Exception ex)
            {
                NotificationUtils.DeveloperToast(this, "Exp Init\n" + ex.ToString());
            }
        }

        private void loadInfoList()
        {
            try
            {
                list = getloadInfoList();
                //rvInfo.SetAdapter(new infoAdapter(this, list));
            }
            catch (Exception e)
            {
                NotificationUtils.DeveloperToast(this, "Exp Load Products\n" + e.ToString());
            }
        }

        private List<NameValueModel> getloadInfoList()
        {
            List<NameValueModel> lst = new List<NameValueModel>();
            NameValueModel m = new NameValueModel {Name = "Version", Value = HM.Container.Resolve<IUtilityService>().GetAppVersionName()};
            lst.Add(m);
            m = new NameValueModel {Name = "Developed By", Value = "Aarib Soft"};
            lst.Add(m);
            m = new NameValueModel {Name = "Software Engineer", Value = "Irfan Siddiqui"};
            lst.Add(m);
            m = new NameValueModel {Name = "Cell", Value = "03336129006"};
            lst.Add(m);
            m = new NameValueModel
            {
                Name = "Email",
                Value = "irfanfraz786@yahoo.com\n" +
                        "irfanfraz@gmail.com\n" +
                        "irfanfraz@hotmail.com"
            };
            lst.Add(m);
            m = new NameValueModel {Name = "City", Value = "Multan"};
            lst.Add(m);

            return lst;
        }

    }
}