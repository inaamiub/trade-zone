﻿using Android.Runtime;
using Android.Util;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class LoginActivity : BaseActivity
    {
        private Button btnFullAccess, btnAsGuest, btnLogin;
        private EditText etLoginPassword, etLoginUserName;
        private ImageView ivLoginBanner;
        private TextView tvRegister, tvForgotPassword, tvPrivacyPolicy;
        private CheckBox cbPrivacyPolicy;
        protected override bool RequireLogin => false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.login_activity);
            Init();
        }

        private void LoadLoginInfoActivity(object sender)
        {
            Intent intent = new Intent(this, typeof(LoginInfoActivity));
            try
            {
                StartActivity(intent);
            }
            catch (Exception ex)
            {
                NotificationUtils.UserToast(this, ex.Message);
            }
        }

        private void Init()
        {
            btnAsGuest = (Button)FindViewById(Resource.Id.btnAsGuest);
            btnAsGuest.Click += BtnAsGuest_Click;
            tvRegister = FindViewById<TextView>(Resource.Id.tvRegister);
            tvRegister.Click += BtnRegister_Click;
            tvForgotPassword = FindViewById<TextView>(Resource.Id.tvForgotPassword);
            tvForgotPassword.Click += TvForgotPasswordOnClick;
            // btnFullAccess = (Button)FindViewById(Resource.Id.btnFullAccess);
            // btnFullAccess.Click += BtnFullAccess_Click;
            btnLogin = (Button)FindViewById(Resource.Id.btnLogin);
            btnLogin.Click += BtnLogin_Click;
            etLoginPassword = (EditText)FindViewById(Resource.Id.etLoginPassword);
            etLoginUserName = (EditText)FindViewById(Resource.Id.etLoginUserName);
            tvPrivacyPolicy = FindViewById<TextView>(Resource.Id.tvPrivacyPolicy);
            cbPrivacyPolicy = FindViewById<CheckBox>(Resource.Id.cbPrivacyPolicy);
#if DEBUG
            etLoginUserName.Text = TZM.UserName;
            etLoginPassword.Text = TZM.Password;
#endif
            cbPrivacyPolicy.Checked = HM.Container.Resolve<TZMPrefs>().GetPrivacyPolicy();
            tvPrivacyPolicy.Click += TvPrivacyPolicyOnClick;
            ivLoginBanner = (ImageView)FindViewById(Resource.Id.ivLoginBanner);

            ivLoginBanner.SetImageResource(
                DynamicResourceCache[ResourceType.Drawable][HConfigManager.Get<TZMConfig>().BannerImage]);
            
            AfterInitActivity();
            HM.Container.RebindInstance(new mdProductsWithStock() {ReportData = new List<StockReportModel>()});
            TZM.FORCE_LOGIN = false;
        }

        private void TvPrivacyPolicyOnClick(object sender, EventArgs e)
        {
            StartActivity(new Intent(this, typeof(PrivacyPolicyActivity)));
            HM.Container.Resolve<TZMPrefs>().SetPrivacyPolicy(true);
        }

        private void TvForgotPasswordOnClick(object sender, EventArgs e)
        {
            StartActivity(new Intent(this, typeof(ForgotPasswordActivity)));
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (cbPrivacyPolicy.Checked == false)
            {
                HideProgressAndShowAlert(GetString(Resource.String.privacy_policy_checkbox_message));
                return;
            }

            UserInformationVO ui = new UserInformationVO();
            ui.UserName = etLoginUserName.Text;
            ui.Password = etLoginPassword.Text;
            if (string.IsNullOrWhiteSpace(ui.UserName))
            {
                HideProgressAndShowAlert(Constants.Messages.USERNAME_REQUIRED);
                return;
            }

            if (string.IsNullOrWhiteSpace(ui.Password))
            {
                HideProgressAndShowAlert(Constants.Messages.PASSWORD_REQUIRED);
                return;
            }

            ShowProgress();
            Task.Run(async () =>
            {
                // Login http request
                LoginUserCommandPayload payload = new LoginUserCommandPayload()
                {
                    UserName = ui.UserName,
                    Password = ui.Password
                };
                await Login(payload);
            });
        }

        private void BtnRegister_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(UserInformationActivity));
            intent.PutExtra(Constants.Keys.USERACCESSTYPE_KEY, UserType.PublicAccess.ToString());
            StartActivityForResult(intent, (int)Result.FirstUser);
        }

        private void BtnAsGuest_Click(object sender, EventArgs e)
        {
            if (cbPrivacyPolicy.Checked == false)
            {
                HideProgressAndShowAlert(GetString(Resource.String.privacy_policy_checkbox_message));
                return;
            }

            ShowProgress();
            Task.Run(async () =>
            {
                // Login http request
                await Login(new LoginGuestUserCommandPayload());
            });
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            if (resultCode == Result.FirstUser)
            {
                Finish();
            }
        }

        private async Task Login(LoginGuestUserCommandPayload payload)
        {
            try
            {
                InitLoginPayload(payload);
                LoginUserCommandResult result = await _commandExecutor.ExecuteCommand<LoginUserCommandResult>(payload);
                // Bind user information with container
                HM.Container.Resolve<TZMPrefs>().SetAccessToken(result.AccessToken);
                HM.Container.Resolve<TZMPrefs>().SetRefreshToken(result.RefreshToken);
                HM.Container.BindInstance(result.User);
            }
            catch (Exception exception)
            {
                HideProgressAndShowAlert(exception.Message);
                return;
            }

            // Login successful
            HM.Container.Resolve<TZMPrefs>().SetPrivacyPolicy(true); // Tick privacy policy
            StartActivity(new Intent(this, typeof(MainActivity)));
            Finish();
        }

        private void InitLoginPayload(LoginGuestUserCommandPayload payload)
        {
            AuthConfig authConfig = _container.Resolve<AuthConfig>(); 
            TZMConfig tzmConfig = _container.Resolve<TZMConfig>(); 
            payload.ClientId = authConfig.ClientId;
            payload.ClientSecret = authConfig.ClientSecret;
            payload.DeviceId = HM.Container.Resolve<IUtilityService>().GetDeviceId();
            payload.AppVersion = TZM.APP_VERSION.ToString();
            payload.BranchId = tzmConfig.BranchId;
            payload.Platform = Platform.Android;
        }
    }
}