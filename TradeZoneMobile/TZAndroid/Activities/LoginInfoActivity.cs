﻿namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class LoginInfoActivity : BaseActivity
    {
        protected override bool RequireLogin => true;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.login_info_activity);
            setBackBtnTitleBar("");

        }
    }
}