﻿using AndroidX.AppCompat.App;
using AndroidX.DrawerLayout.Widget;
using Google.Android.Material.Button;
using HatfCommand;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class MainActivity : BaseActivity
    {
        private RecyclerView _rvMain = null!;
        private List<CompanyModel> _list = null!;
        private MaterialButton _btnStockReport = null!;
        private MaterialButton _btnOrderHistory = null!;
        private MaterialButton _btnNewOrder = null!;
        
        #region Navigation Drawer Variables

        private DrawerLayout drawerLayoutMain;
        private ImageView imgMainDrawerHeader, ivAccountStatus;
        private ActionBarDrawerToggle drawerToggle;
        private RecyclerView rvMainNavigation;
        private RelativeLayout drawerPane;
        private TextView tvMainNavTitle, tvAccountStatus;
        #endregion
        
        private ShoppingCartSystem _shoppingCartSystem;
        private BadgedMenuItem _badgedCartMenuItem;

        public const string STOCK_LAST_UPDATED_TIME = "STOCK_LAST_UPDATED_TIME";
        protected override bool RequireLogin => true;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            
            View? menuItemView = View.Inflate(this, Resource.Layout.badge_action_item_layout, null);
            _badgedCartMenuItem = new BadgedMenuItem
            {
                ClickedEvent = GoToCart, // Goto cart
                DrawableResource = Resource.Drawable.ic_baseline_shopping_cart_24dp,
                MenuItemId = Resource.Id.toolbar_menu_one_item1,
                ActionView = menuItemView,
                TextView = menuItemView.FindViewById<TextView>(Resource.Id.tvCartBadge)
            };
            setIconicTitleBar("", Resource.Drawable.ic_menu_white_24dp, null, null, Resource.Menu.toolbar_menu_one_item, new List<mdCustomMenuItem>()
            {
                _badgedCartMenuItem
            });
            Init();
            LoadCompanies();
            StartListeners();
        }
        
        private void Init()
        {
            BindStockReport(new mdProductsWithStock());
            SyncStockData();
            try
            {
                _rvMain = (RecyclerView)FindViewById(Resource.Id.rvMain);
                _rvMain.SetLayoutManager(new LinearLayoutManager(this));
                
                _btnOrderHistory = FindViewById<MaterialButton>(Resource.Id.btnOrderHistory);
                _btnNewOrder = FindViewById<MaterialButton>(Resource.Id.btnNewOrder);
                _btnStockReport = FindViewById<MaterialButton>(Resource.Id.btnStockReport);
                
                InitNavigationLayout();
                AfterInitActivity();

                // Resolve dependencies
                _shoppingCartSystem = HM.Container.Resolve<ShoppingCartSystem>();
                _shoppingCartSystem.AddOnUpdatedEventListener(OnItemQuantityUpdated);
                // Set default value for cart
                OnItemQuantityUpdated("", 0, _shoppingCartSystem.TotalQuantity);

                InitAds();
            }
            catch (Exception ex)
            {
                NotificationUtils.DeveloperToast(this, "Exp Init\n" + ex.ToString());
            }
        }

        private void OnItemQuantityUpdated(string itemId, uint quantity, long count)
        {
            _badgedCartMenuItem.SetBadgeCount(count);
        }

        public override void Finish()
        {
            _shoppingCartSystem?.RemoveOnUpdatedEventListener(OnItemQuantityUpdated);
            base.Finish();
        }

        private void GoToCart(object obj)
        {
            Intent intent = new Intent(this, typeof(ShoppingCartActivity));
            intent.AddFlags(ActivityFlags.ReorderToFront);
            StartActivity(intent);
        }

        #region ad related work
        private void InitAds()
        {
            InitBanner(Resource.Id.avBannerActivityMain);
            InitInterstitialAd();
        }

        protected override void OnAdClosed()
        {
            LoadStockReport();
        }

        #endregion

        private void InitNavigationLayout()
        {
            drawerPane = (RelativeLayout)FindViewById(Resource.Id.drawerPane);
            drawerPane.Click += DrawerPane_Click;
            drawerLayoutMain = (DrawerLayout)FindViewById(Resource.Id.drawerLayoutMain);
            drawerToggle = new ActionBarDrawerToggle(this, drawerLayoutMain, toolbar, Resource.String.drawer_open, Resource.String.drawer_close);
            drawerToggle.DrawerIndicatorEnabled = true;

            drawerLayoutMain.AddDrawerListener(drawerToggle);
            drawerLayoutMain.Click += DrawerLayoutMain_Click;

            tvMainNavTitle = (TextView)drawerLayoutMain.FindViewById(Resource.Id.tvMainNavTitle);
            UserInformationVO? userInformationVO = HM.Container.Resolve<UserInformationVO>();
            tvMainNavTitle.Text = userInformationVO.Name;

            imgMainDrawerHeader = (ImageView)drawerLayoutMain.FindViewById(Resource.Id.imgMainDrawerHeader);
            imgMainDrawerHeader.Click += ImgMainDrawerHeader_Click;
            rvMainNavigation = (RecyclerView)FindViewById(Resource.Id.rvMainNavigation);
            rvMainNavigation.SetLayoutManager(new LinearLayoutManager(this));

            tvAccountStatus = FindViewById<TextView>(Resource.Id.tvAccountStatus);
            ivAccountStatus = FindViewById<ImageView>(Resource.Id.ivAccountStatus);
            // Set user account status
            if (userInformationVO.UserType == UserType.FullAccess ||
                userInformationVO.UserType == UserType.PublicAccess)
            {
                if (userInformationVO.RecordStatus == UserRecordStatus.Active)
                {
                    tvAccountStatus.Text = Resources.GetString(Resource.String.verified);
                    ivAccountStatus.SetImageDrawable(
                        Resources.GetDrawable(Resource.Drawable.ic_baseline_check_circle_24dp));
                }
                else if (userInformationVO.RecordStatus == UserRecordStatus.PendingVerification)
                {
                    tvAccountStatus.Text = Resources.GetString(Resource.String.not_verified);
                    ivAccountStatus.SetImageDrawable(
                        Resources.GetDrawable(Resource.Drawable.ic_baseline_cancel_24dp));
                    tvAccountStatus.Click += VerifyAccount_Click;
                    ivAccountStatus.Click += VerifyAccount_Click;
                }
                else
                {
                    tvAccountStatus.Visibility = ViewStates.Gone;
                    ivAccountStatus.Visibility = ViewStates.Gone;
                }
            }
            else
            {
                tvAccountStatus.Visibility = ViewStates.Gone;
                ivAccountStatus.Visibility = ViewStates.Gone;
            }

            List<mdCustomMenuItem> cmiList = new List<mdCustomMenuItem>();
            if (HM.Container.Resolve<UserInformationVO>().UserName == TZM.UserName || HM.Container.Resolve<UserInformationVO>().Email == TZM.Email)
            {
                cmiList.Add(new mdCustomMenuItem()
                {
                    DrawableResource = Resource.Drawable.ic_phone_iphone_grey_24dp,
                    Text = "Private Info",
                    ClickedEvent = PrivateInfoClicked
                });
            }
            cmiList.Add(new mdCustomMenuItem()
            {
                DrawableResource = Resource.Drawable.ic_phone_iphone_grey_24dp,
                Text = userInformationVO.CellNo
            });
            cmiList.Add(new mdCustomMenuItem()
            {
                DrawableResource = Resource.Drawable.ic_home_grey_24dp,
                Text = userInformationVO.UserAddressVO?.City
            });
            cmiList.Add(new mdCustomMenuItem()
            {
                DrawableResource = Resource.Drawable.ic_email_grey_24dp,
                Text = userInformationVO.Email
            });
            cmiList.Add(new mdCustomMenuItem()
            {
                DrawableResource = Resource.Drawable.ic_playlist_add_check_grey_24dp,
                Text = EnumUtils.GetEnumDescription(userInformationVO.UserType)
            });
            if(userInformationVO.UserType == UserType.FullAccess || userInformationVO.UserType == UserType.PublicAccess)
            {
                cmiList.Add(new mdCustomMenuItem()
                {
                    DrawableResource = Resource.Drawable.ic_update_gery_24dp,
                    Text = GetString(Resource.String.update_account_info),
                    ClickedEvent = UpdateAccountInfo_Click,
                });
                cmiList.Add(new mdCustomMenuItem()
                {
                    DrawableResource = Resource.Drawable.ic_person_grey_24dp,
                    Text = GetString(Resource.String.update_profile),
                    ClickedEvent = UpdateProfile_Click,
                });
            }
            cmiList.Add(new mdCustomMenuItem()
            {
                DrawableResource = Resource.Drawable.ic_baseline_exit_to_app_24dp,
                Text = GetString(Resource.String.log_out),
                ClickedEvent = ChangeAccessItem_Click,
            });
            cmiList.Add(new mdCustomMenuItem()
            {
                DrawableResource = Resource.Drawable.ic_baseline_privacy_tip_grey_24dp,
                Text = GetString(Resource.String.privacy_policy),
                ClickedEvent = PrivacyPolicy_Click,
            });
            cmiList.Add(new mdCustomMenuItem()
            {
                DrawableResource = Resource.Drawable.ic_info_outline_grey_24dp,
                Text = GetString(Resource.String.about),
                ClickedEvent = LoadInfoActivity,
            });
            rvMainNavigation.SetAdapter(new MainNavigationAdapter(this, cmiList));

        }

        private void VerifyAccount_Click(object sender, EventArgs args)
        {
            // Send account verification email
            ShowProgress();
            Task.Run(async () =>
            {
                try
                {
                    await _commandExecutor.ExecuteVoidCommand(nameof(SendAccountVerificationEmailCommand));
                    RunInvokeRequired(_ =>
                    {
                        Intent intent = new Intent(this, typeof(VerificationActivity));
                        intent.PutExtra(VerificationActivity.DISABLE_RESEND_KEY, true);
                        intent.PutExtra(VerificationActivity.VERIFICATION_TYPE_KEY, TokenType.AccountVerificationToken.ToString());
                        StartActivity(intent);
                        HideProgress();
                    });
                }
                catch (Exception e)
                {
                    HLogger.Error(e, "Exception occured while executing http request");
                    _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
                }
            });
        }

        private void PrivateInfoClicked(object obj)
        {
            StartActivity(new Intent(this, typeof(PrivateInfoActivity)));
        }

        private void UpdateAccountInfo_Click(object param)
        {
            StartActivity(new Intent(this, typeof(UpdateAccountInfoActivity)));
        }

        private void UpdateProfile_Click(object param)
        {
            Intent intent = new Intent(this, typeof(UserInformationActivity));
            intent.PutExtra(Constants.Keys.ISEDIT, true);
            StartActivity(intent);
        }

        private void PrivacyPolicy_Click(object param)
        {
            Intent intent = new Intent(this, typeof(PrivacyPolicyActivity));
            StartActivity(intent);
        }

        private void ChangeAccessItem_Click(object param)
        {
            AlertBox(this, "Are sure to log out?", "Question", "No", null, "Yes", Logout);
        }

        private void DrawerPane_Click(object sender, EventArgs e)
        {
            
        }

        private void ImgMainDrawerHeader_Click(object sender, EventArgs e)
        {
            drawerLayoutMain.CloseDrawer((int)GravityFlags.Left, true);
        }

        private void DrawerLayoutMain_Click(object sender, EventArgs e)
        {
            NotificationUtils.DeveloperToast(this, "Drawer Clicked");
        }

        private void LoadCompanies()
        {
            try
            {
                _list = GetCompaniesList();
                _rvMain.SetAdapter(new CompaniesAdapter(this, _list));
            }
            catch (Exception e)
            {
                NotificationUtils.DeveloperToast(this, "Exp Load Products\n" + e.ToString());
            }
        }

        private void StartListeners()
        {
            _btnStockReport.Click += BtnStockReportClick;
            _btnNewOrder.Click += BtnStockReportClick;
            _btnOrderHistory.Click += BtnOrderHistoryClicked;
        }

        private void BtnOrderHistoryClicked(object sender, EventArgs e)
        {
            Intent? intent = new Intent(this, typeof(OrderHistoryActivity));
            StartActivity(intent);
        }

        private void BtnStockReportClick(object sender, EventArgs e)
        {
            if (this.IsInterstitialAdLoaded())
            {
                this.ShowInterstitialAd();
            }
            else
            {
                LoadStockReport();
            }
        }

        private void LoadStockReport()
        {
            String partyName = "";
            if (_list.Count > 0)
            {
                partyName = _list[0].CompanyName;
            }
            Intent intent = new Intent(this, typeof(StockReportActivity));
            intent.PutExtra("PartyName", partyName);
            StartActivity(intent);
        }

        private List<CompanyModel> GetCompaniesList()
        {
            return new List<CompanyModel> {HConfigManager.Get<TZMConfig>().CompanyDetails};
        }

        private void SyncStockData()
        {
            Task.Run(async () =>
            {
                ShowProgress();
                try
                {
                    GetStockReportCommandResult? stockReportCommandResult =
                        await _commandExecutor.ExecuteVoidCommand<GetStockReportCommandResult>(
                            nameof(GetStockReportCommand));
                    mdProductsWithStock? stockReport = new mdProductsWithStock()
                    {
                        ReportData = stockReportCommandResult.ReportData
                    };
                    BindStockReport(stockReport, DateTime.Now);
                }
                catch (Exception)
                {
                    NotificationUtils.UserToast(this, "Sorry! Could not connect to the server");
                }
                finally
                {
                    HideProgress();
                }
            });
        }

        private void BindStockReport(mdProductsWithStock stockReport, DateTime time = default)
        {
            _container.RebindInstance(stockReport);
            _container.RebindInstanceWithId(time, STOCK_LAST_UPDATED_TIME);
        }

    }
}

