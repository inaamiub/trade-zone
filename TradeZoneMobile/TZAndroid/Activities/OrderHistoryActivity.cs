﻿using HatfCommand;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class OrderHistoryActivity : BaseActivity
    {
        private RecyclerView _rvOrderHistory;
        private UserInformationVO _userInformationVo;
        protected override bool RequireLogin => true;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.order_history);

            setBackBtnTitleBar("Orders", Resource.Menu.toolbar_menu_one_item);
            
            Init();
        }

        private void Init()
        {
            _rvOrderHistory = FindViewById<RecyclerView>(Resource.Id.rvOrderHistory);
            _rvOrderHistory.SetLayoutManager(new LinearLayoutManager(this));
            _userInformationVo = HM.Container.Resolve<UserInformationVO>();
            if (_userInformationVo.UserType != UserType.FullAccess && _userInformationVo.UserType != UserType.PublicAccess)
            {
                AlertBox(this, TZErrorMessages.GetErrorMessage(TZErrorCodes.GUEST_NOT_ALLOWED), neutralButtonClicked:
                    _ =>
                    {
                        Finish();
                    });
                return;
            }

            if (HM.Container.TryResolve<mdOrderHistory>(out _) == false)
            {
                ShowProgress();
                // Fetch order history from server
                Task.Run(async () =>
                {
                    try
                    {
                        GetUserOrderHistoryCommandResult result = await _commandExecutor.ExecuteVoidCommand<GetUserOrderHistoryCommandResult>(nameof(GetUserOrderHistoryCommand));
                        if (result != null)
                        {
                            mdOrderHistory? orderHistory = new mdOrderHistory()
                            {
                                Orders = result.Orders
                            };
                            _container.RebindInstance(orderHistory);
                            RunInvokeRequired(_ =>
                            {
                                LoadRecyclerView(); 
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        AlertBox("Error happened " + e.Message, "Error", neutralButton:"OK");
                    }
                });
            }
            else
            {
                LoadRecyclerView();
            }
        }

        private void LoadRecyclerView()
        {
            HM.Container.TryResolve<mdOrderHistory>(out mdOrderHistory? orderHistory);
            _rvOrderHistory.SetAdapter(new OrderHistoryAdapter(this, orderHistory?.Orders ?? new List<SalesOrderVO>()));
            HideProgress();
        }
        
        protected override void OnResume()
        {
            // Update order history view
            LoadRecyclerView();
            base.OnResume();
        }

    }

}