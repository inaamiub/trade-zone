﻿using Android.Webkit;

namespace TZAndroid
{
    [Activity(Label = "Privacy Policy", Theme = "@style/NoActionBar")]
    public class PrivacyPolicyActivity : BaseActivity
    {
        //RecyclerView rvInfo;
        private TextView tvPPContent;
        private WebView wvPrivacyPolicy;
        protected override bool RequireLogin => false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            try
            {
                SetContentView(Resource.Layout.privacy_policy_activity);
                setBackBtnTitleBar(GetString(Resource.String.privacy_policy), null);
                Init();
            }   
            catch (Exception ex)
            {
                NotificationUtils.DeveloperToast(this, "Exp Init\n" + ex.ToString());
            }
        }

        private void Init()
        {
            wvPrivacyPolicy = FindViewById<WebView>(Resource.Id.wvPrivacyPolicy);
            // string htmlContent = GetStringFromRawResource(Resource.Raw.privacy_policy);
            // wvPrivacyPolicy.LoadData(htmlContent, null, null);
            
            string url = HConfigManager.Get<TZMConfig>().PrivacyPolicyUrl;
            wvPrivacyPolicy.LoadUrl(url);
        }

        private string GetStringFromRawResource(int resourceId)
        {
            try
            {
                using Stream? stream = Resources?.OpenRawResource(resourceId);
                using StreamReader? streamReader = new StreamReader(stream);
                string value = streamReader.ReadToEnd();
                return value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}