﻿namespace TZAndroid
{
    [Activity(Label = "Private Info", Theme = "@style/NoActionBar")]
    public class PrivateInfoActivity : BaseActivity
    {
        //RecyclerView rvInfo;
        private TextView tvAppSettingsFileName;
        private EditText etBaseUrl;
        private Button btnProceed;
        protected override bool RequireLogin => true;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            try
            {
                SetContentView(Resource.Layout.private_info_activity);
                setBackBtnTitleBar("Private Info", null);
                Init();
            }   
            catch (Exception ex)
            {
                NotificationUtils.DeveloperToast(this, "Exp Init\n" + ex.ToString());
            }
        }

        private void Init()
        {
            tvAppSettingsFileName = FindViewById<TextView>(Resource.Id.tvAppSettingsFileName);
            etBaseUrl = FindViewById<EditText>(Resource.Id.etBaseUrl);
            btnProceed = FindViewById<Button>(Resource.Id.btnProceed);

            tvAppSettingsFileName.Text = Program.GetAppSettingsFileName();
            etBaseUrl.Text = HM.BaseUrl;
            btnProceed.Click += BtnProceedOnClick;
        }

        private void BtnProceedOnClick(object sender, EventArgs e)
        {
            bool relogin = false;
            if (HM.BaseUrl.Equals(etBaseUrl.Text) == false)
            {
                HM.BaseUrl = etBaseUrl.Text;
                relogin = true;
            }

            HideProgressAndShowAlert("Saved successfully");
            Logout(null);
        }
    }
}