﻿using Android.Text;
using AndroidX.ViewPager.Widget;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class ProductDetailsActivity : BaseActivity
    {
        private ViewPager _vpImageSlider;
        private Button _btnDecrementQuantity, _btnIncrementQuantity, _btnRemoveFromCart;
        private EditText _etQuantity;
        private TextView _tvGroup, _tvStockStatus, _tvPrice, _tvName, _tvCompany;
        private ImageView _imgStockStatus;

        public const string PRODUCT_ID_KEY = "ProductId";

        private uint _quantity = 0;
        private HatfEvent<int> OnPageSelected = new HatfEvent<int>();
        private ShoppingCartSystem _shoppingCartSystem;
        private BadgedMenuItem _badgedCartMenuItem;
        private StockReportModel _productDetails; 
        protected override bool RequireLogin => true;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            if (HM.Container.TryResolve<mdProductsWithStock>(out mdProductsWithStock? products) == false)
            {
                NotificationUtils.UserToast(this, "Sorry! Products not found");
                Finish();
                return;
            }

            _productDetails = products?.ReportData?.FirstOrDefault(m => m.ProductID == Intent.GetStringExtra(PRODUCT_ID_KEY));
            if (_productDetails == null)
            {
                NotificationUtils.UserToast(this, "Sorry! Product not found");
                Finish();
                return;   
            }
            
            SetContentView(Resource.Layout.product_details);

            View? menuItemView = View.Inflate(this, Resource.Layout.badge_action_item_layout, null);
            _badgedCartMenuItem = new BadgedMenuItem
            {
                ClickedEvent = GoToCart, // Goto cart
                DrawableResource = Resource.Drawable.ic_baseline_shopping_cart_24dp,
                MenuItemId = Resource.Id.toolbar_menu_one_item1,
                ActionView = menuItemView,
                TextView = menuItemView.FindViewById<TextView>(Resource.Id.tvCartBadge)
            };
            setIconicTitleBar("Product Details", Resource.Drawable.ic_baseline_close_24dp, CloseButtonClicked, null,
                Resource.Menu.toolbar_menu_one_item, new List<mdCustomMenuItem>() {_badgedCartMenuItem});

            inIt();
        }

        private void GoToCart(object obj)
        {
            Intent intent = new Intent(this, typeof(ShoppingCartActivity));
            intent.AddFlags(ActivityFlags.ReorderToFront);
            StartActivity(intent);
        }

        private void CloseButtonClicked(object sender, AndroidX.AppCompat.Widget.Toolbar.NavigationClickEventArgs e)
        {
            Finish();
        }

        private void inIt()
        {
            // Resolve dependencies
            _shoppingCartSystem = HM.Container.Resolve<ShoppingCartSystem>();

            _vpImageSlider = FindViewById<ViewPager>(Resource.Id.vpImageSlider);
            _vpImageSlider.PageSelected += VpImageSliderOnPageSelected;
            ProductImageSliderAdapter? adapter = new ProductImageSliderAdapter(this);
            OnPageSelected.AddListener(adapter.OnPageSelected);
            _vpImageSlider.Adapter = adapter;

            _btnDecrementQuantity = FindViewById<Button>(Resource.Id.btnDecrementQuantity);
            _btnIncrementQuantity = FindViewById<Button>(Resource.Id.btnIncrementQuantity);
            _etQuantity = FindViewById<EditText>(Resource.Id.etQuantity);
            _btnRemoveFromCart = FindViewById<Button>(Resource.Id.btnRemoveFromCart);
            
            // If stock not available, then no order
            if (HConfigManager.Get<TZSharedConfig>().EnsureItemStockInOrders == false && _productDetails.StockStatus == false)
            {
                _btnIncrementQuantity.Clickable = false;
            }
            
            _btnDecrementQuantity.Click += BtnDecrementQuantityOnClick;
            _btnIncrementQuantity.Click += BtnIncrementQuantityOnClick;
            _etQuantity.TextChanged += EtQuantityOnTextChanged;
            // _etQuantity.KeyPress += EtQuantityOnKeyPress;
            _etQuantity.FocusChange += (sender, args) =>
            {
                if (args.HasFocus)
                {
                    _etQuantity.SelectAll();
                }
            };
            _btnRemoveFromCart.Click += BtnRemoveFromCartOnClick;
            _shoppingCartSystem.AddOnUpdatedEventListener(OnItemQuantityUpdated);
            
            // Set default quantity
            _quantity = _shoppingCartSystem.GetItemQuantity(_productDetails.ProductID);
            OnItemQuantityUpdated(_productDetails.ProductID, _quantity, _shoppingCartSystem.TotalQuantity);
            
            // Load product details
            _tvGroup = FindViewById<TextView>(Resource.Id.tvGroup);
            _tvStockStatus = FindViewById<TextView>(Resource.Id.tvStockStatus);
            _tvPrice = FindViewById<TextView>(Resource.Id.tvPrice);
            _tvName = FindViewById<TextView>(Resource.Id.tvName);
            _tvCompany = FindViewById<TextView>(Resource.Id.tvCompany);
            _imgStockStatus = FindViewById<ImageView>(Resource.Id.imgStockStatus);

            _tvGroup.Text = _productDetails.GroupName;
            _tvName.Text = _productDetails.ProductName;
            _tvCompany.Text = _productDetails.CompanyName;
            _tvPrice.Text = "Rs " +  _productDetails.RetailPrice;
            
            if (_productDetails.StockStatus)
            {
                _tvStockStatus.Text = "In Stock";
                _imgStockStatus.SetImageResource(Resource.Drawable.ic_album_green_24dp);
            }
            else
            {
                _tvStockStatus.Text = "Out of Stock";
                _imgStockStatus.SetImageResource(Resource.Drawable.ic_album_red_24dp);
            }
        }

        private void EtQuantityOnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (uint.TryParse(_etQuantity.Text, out uint quantity) == false)
            {
                quantity = 0;
            }
            
            if (quantity != _quantity)
            {
                _quantity = quantity;
                _etQuantity.Text = _quantity.ToString();
                OnQuantityChange();
            }
        }

        private void EtQuantityOnKeyPress(object sender, View.KeyEventArgs e)
        {
            if (e?.Event?.Action == KeyEventActions.Up)
            {
                if(e.KeyCode == Keycode.Enter || e.KeyCode == Keycode.NumpadEnter || e.KeyCode == Keycode.Search)
                {
                    Utils.HideKeyboardFrom(this);
                }
                else if((e.KeyCode >= Keycode.Num0 && e.KeyCode <= Keycode.Numpad9))
                {
                    if (uint.TryParse(_etQuantity.Text, out uint quantity))
                    {
                        _quantity = quantity;
                    }
                    else
                    {
                        _quantity = 0;
                        _etQuantity.Text = _quantity.ToString();
                    }
                    _etQuantity.SetSelection(_quantity.ToString().Length);

                    if (quantity != _quantity)
                    {
                        OnQuantityChange();
                    }
                }
            }
        }

        private void OnItemQuantityUpdated(string itemId, uint quantity, long count)
        {
            _etQuantity.Text = _quantity.ToString();
            _btnRemoveFromCart.Enabled = _quantity > 0;
            LoadBadgeCount();
        }

        private void LoadBadgeCount()
        {
            _badgedCartMenuItem.SetBadgeCount(_shoppingCartSystem.TotalQuantity);
        }

        private void BtnRemoveFromCartOnClick(object sender, EventArgs e)
        {
            _quantity = 0;
            OnQuantityChange();
        }

        private void BtnIncrementQuantityOnClick(object sender, EventArgs e)
        {
            if (HConfigManager.Get<TZSharedConfig>().EnsureItemStockInOrders && _quantity >= _productDetails.Stock)
            {
                AlertBox(this, $"Item {_productDetails.ProductName} don't have enough stock", "Error");
                return;
            }
            _quantity++;
            OnQuantityChange();
        }

        private void BtnDecrementQuantityOnClick(object sender, EventArgs e)
        {
            if (_quantity > 0)
            {
                _quantity--;
                OnQuantityChange();
            }
        }

        private void OnQuantityChange()
        {
            if (_quantity < 0)
            {
                _quantity = 0;
                _etQuantity.Text = _quantity.ToString();
            }
            
            // Update quantity in cart
            _shoppingCartSystem.UpdateItem(_productDetails.ProductID, _quantity);
        }
        
        private void VpImageSliderOnPageSelected(object sender, ViewPager.PageSelectedEventArgs e)
        {
            OnPageSelected.Invoke(e.Position);
        }

        protected override void OnResume()
        {
            // Update the price if shown
            LoadBadgeCount();
            base.OnResume();
        }

        public override void Finish()
        {
            _shoppingCartSystem?.RemoveOnUpdatedEventListener(OnItemQuantityUpdated);
            base.Finish();
        }
    }

}