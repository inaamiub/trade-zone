﻿using FluentValidation.Results;

namespace TZAndroid
{
    [Activity]
    
    public class ResetPasswordActivity : BaseActivity
    {
        private EditText etNewPassword, etConfirmPassword;
        private AppCompatButton btnSave;
        private ImageButton ibBackResetPassword;

        public const string RESET_PASSWORD_EMAIL_KEY = "RESET_PASSWORD_EMAIL_KEY";
        public const string RESET_PASSWORD_TOKEN_KEY = "RESET_PASSWORD_TOKEN_KEY";

        protected override bool RequireLogin => false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            Init();
        }

        private void Init()
        {
            SetContentView(Resource.Layout.reset_password_activity);

            etNewPassword = FindViewById<EditText>(Resource.Id.etNewPassword);
            etConfirmPassword = FindViewById<EditText>(Resource.Id.etConfirmPassword);
            btnSave = FindViewById<AppCompatButton>(Resource.Id.btnSave);
            ibBackResetPassword = FindViewById<ImageButton>(Resource.Id.ibBackResetPassword);

            // Initialize listeners
            ibBackResetPassword.Click += IbBackResetPasswordOnClick;
            btnSave.Click += BtnSave_Click;
            
            // Set fouc on first edit text
            etNewPassword.RequestFocus();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string newPassword = etNewPassword.Text.Trim();
            string confirmPassword = etConfirmPassword.Text.Trim();
            
            // Validate
            ValidationResult? validationResult = new UserInformationVoPasswordValidator().Validate(new UserInformationVO
            {
                Password = newPassword
            });
            
            if (validationResult.IsValid == false)
            {
                HM.Container.Resolve<IUtilityService>()
                    .AlertBox(string.Join("\n", validationResult.Errors.Select(m => m.ErrorMessage)));
                return;
            }

            if (newPassword != confirmPassword)
            {
                HM.Container.Resolve<IUtilityService>()
                    .AlertBox("New Password and Confirm Password did not match");
                return;
            }

            // Validated successfully
            ShowProgress();

            Task.Run(async () =>
            {
                try
                {
                    await _commandExecutor.ExecuteVoidCommand(new RecoverPasswordCommandPayload()
                    {
                        NewPassword = newPassword,
                        BranchId = HConfigManager.Get<TZMConfig>().BranchId,
                        Token = PasswordRecoveryToken,
                        Email = RecoveryEmail
                    });
                    // Request executed successfully
                    AlertBox(
                        "Your password has been successfully updated. Please log in to continue", "Success",
                        neutralButton: "OK",
                        neutralButtonClicked: _ => { MoveToLogin(); });
                }
                catch (HException e)
                {
                    HLogger.Error(e, "Exception occured while executing http request");
                    AlertBox(e.Message, "Error", neutralButton: "OK");
                }
                HideProgress();
            });
        }

        private void IbBackResetPasswordOnClick(object sender, EventArgs e)
        {
            Finish();
        }
        
        private string RecoveryEmail
        {
            get
            {
                string email = Intent.GetStringExtra(RESET_PASSWORD_EMAIL_KEY);
                if (string.IsNullOrEmpty(email))
                {
                    throw new Exception("Email not provided by the previous activity");
                }

                return email;
            }
        }

        private string PasswordRecoveryToken
        {
            get
            {
                string token = Intent.GetStringExtra(RESET_PASSWORD_TOKEN_KEY);
                if (string.IsNullOrEmpty(token))
                {
                    throw new Exception("Password Recovery Token not provided by the previous activity");
                }

                return token;
            }
        }

    }
}