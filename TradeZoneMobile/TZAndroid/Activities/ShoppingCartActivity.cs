﻿using Android.Runtime;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class ShoppingCartActivity : BaseActivity
    {
        private RecyclerView _rvShoppingCart;
        private Button _btnPostOrder;
        private TextView _tvTotalItems, _tvTotalPrice;
        
        private ShoppingCartSystem _shoppingCartSystem;
        private UserInformationVO _userInformationVo;
        private mdProductsWithStock _allProducts;
        
        protected override bool RequireLogin => true;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.shopping_cart);

            setBackBtnTitleBar("Cart", Resource.Menu.toolbar_menu_one_item);
            
            inIt();
        }

        private void inIt()
        {
            if (HM.Container.TryResolve<mdProductsWithStock>(out mdProductsWithStock? products) == false)
            {
                NotificationUtils.UserToast(this, "Sorry! Products not found");
                Finish();
                return;
            }

            _allProducts = products;

            _tvTotalItems = FindViewById<TextView>(Resource.Id.tvTotalItems);
            _tvTotalPrice = FindViewById<TextView>(Resource.Id.tvTotalPrice);
            
            _btnPostOrder = FindViewById<Button>(Resource.Id.btnPostOrder);
            _btnPostOrder.Click += BtnPostOrderOnClick;
            
            _rvShoppingCart = FindViewById<RecyclerView>(Resource.Id.rvShoppingCart);
            _rvShoppingCart.SetLayoutManager(new LinearLayoutManager(this));
            
            // Resolve dependencies
            _shoppingCartSystem = HM.Container.Resolve<ShoppingCartSystem>();
            _shoppingCartSystem.AddOnUpdatedEventListener(OnCartUpdated);

            OnCartUpdated(null, 0, 0);
        }

        private void BtnPostOrderOnClick(object sender, EventArgs e)
        {
            if (_shoppingCartSystem?.TotalQuantity < 1)
            {
                AlertBox(this, "No item exists in the cart");
                return;
            }
            
            _userInformationVo = HM.Container.Resolve<UserInformationVO>();
            if (_userInformationVo.UserType != UserType.FullAccess && _userInformationVo.UserType != UserType.PublicAccess)
            {
                AlertBox(this, TZErrorMessages.GetErrorMessage(TZErrorCodes.GUEST_CAN_NOT_CREATE_ORDER));
                return;
            }

            // Check if any item which is out of stock is also added in the list
            if (HConfigManager.Get<TZSharedConfig>().EnsureItemStockInOrders)
            {
                List<ShoppingCartItemModel>? outOfStockItems = _shoppingCartSystem.GetAllItems().Where(m =>
                    m.Quantity > 0 && _allProducts.ReportData.Any(a => a.ProductID == m.ItemId && a.StockStatus) ==
                    false).ToList();

                if (outOfStockItems.Count > 0)
                {
                    StockReportModel? productDetails = _allProducts?.ReportData?.FirstOrDefault(m => m.ProductID == outOfStockItems.First().ItemId);
                    AlertBox(this, $"Item {productDetails.ProductName} is out of stock", "Error");
                    return;
                }
            }

            // Go to check out activity
            Intent intent = new Intent(this, typeof(CheckOutActivity));
            StartActivityForResult(intent, 0);
        }

        private void OnCartUpdated(string arg1, uint arg2, long arg3)
        {
            LoadCartDetails();
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            if (resultCode == Result.Ok)
            {
                SetResult(Result.Ok);
                Finish();
            }
        }

        protected override void OnResume()
        {
            // Update the price if shown
            LoadCartDetails();
            base.OnResume();
        }

        public override void Finish()
        {
            _shoppingCartSystem?.RemoveOnUpdatedEventListener(OnCartUpdated);
            base.Finish();
        }

        private void LoadCartDetails()
        {
            List<ShoppingCartItemModel>? items = _shoppingCartSystem.GetAllItems();
            _rvShoppingCart.SetAdapter(new ShoppingCartAdapter(this, items));
            _tvTotalItems.Text = items.Count.ToString();

            decimal totalPrice = items.Select(m => (m.ProductDetails?.RetailPrice ?? 0) * m.Quantity).Sum(); 
            _tvTotalPrice.Text = "RS " + Math.Round(totalPrice, 2);
            
            _btnPostOrder.Enabled = _shoppingCartSystem.TotalQuantity > 0;
        }
    }

}