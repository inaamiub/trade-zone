﻿using Android;
using Android.Content.PM;
using Android.Runtime;
using AndroidX.AppCompat.App;
using HatfCommand;

namespace TZAndroid;

[Activity(MainLauncher = true, Theme = "@style/NoActionBar")]
public class SplashActivity : AppCompatActivity
{
    private ImageView ivSplashBanner;
    private TextView tvCopyRight;

    protected override void OnCreate(Bundle savedInstanceState)
    {
        base.OnCreate(savedInstanceState);

        if (Intent.GetBooleanExtra(Constants.Keys.EXIT, false))
        {
            Finish();
            return;
        }

        // Init Logger
        HLogger.EnabledLogLevel = LogLevel.Debug;
        HLogger.Current.AddAppender(new AndroidLogAppender(PackageName));

        try
        {
            NotificationUtils.DeveloperToast(this, "Passed exit check");
            // Register exception handlers
            AppDomain.CurrentDomain.UnhandledException += HM.OnUnhandledException;
            TaskScheduler.UnobservedTaskException += HM.OnUnobservedTaskException;

            NotificationUtils.DeveloperToast(this, "Registered exception handlers");
            // Initialize this activity resources
            Init();

            NotificationUtils.DeveloperToast(this, "Called init");
            // Initialize application
            Task.Run(async () => { await Program.InitializeApplication(); }).Wait();

            NotificationUtils.DeveloperToast(this, "Initiaized application");
            // Load image/string resources and add to cache
            ValidateAndCreateCache();

            NotificationUtils.DeveloperToast(this, "Done cache");
            // Set image resource
            ivSplashBanner.SetImageResource(
                BaseActivity.DynamicResourceCache[ResourceType.Drawable][
                    HConfigManager.Get<TZMConfig>().BannerImage]);

            NotificationUtils.DeveloperToast(this, "Setup the banner");
            HM.Container.RebindInstance(new HatfContext(this));

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private void ValidateAndCreateCache()
    {
        TZMConfig? tzmConf = HConfigManager.Get<TZMConfig>();
        Dictionary<ResourceType, List<string>> resourceToCache = new Dictionary<ResourceType, List<string>>();

        // Add drawable resources
        resourceToCache.Add(ResourceType.Drawable, new List<string>
        {
            tzmConf.BannerImage,
            tzmConf.LogoSmall
        });

        foreach (KeyValuePair<ResourceType, List<string>> entry in resourceToCache)
        {
            if (BaseActivity.DynamicResourceCache.ContainsKey(entry.Key) == false)
            {
                BaseActivity.DynamicResourceCache.Add(entry.Key, new Dictionary<string, int>());
            }

            int GetResource(string name)
            {
                if (entry.Key == ResourceType.Drawable)
                {
                    int id = Resources.GetIdentifier(name, "drawable", PackageName);
                    return id;
                }
                if (entry.Key == ResourceType.String)
                {
                    int id = Resources.GetIdentifier(name, "string", PackageName);
                    return id;
                }

                throw new Exception($"Resource type {entry.Key} not handled to be cached");
            }

            foreach (string resourceName in entry.Value)
            {
                BaseActivity.DynamicResourceCache[entry.Key][resourceName] = GetResource(resourceName);
            }
        }
    }

    private void Init()
    {
        SetContentView(Resource.Layout.splash_activity);

        ivSplashBanner = FindViewById<ImageView>(Resource.Id.ivSplashBanner);

        tvCopyRight = FindViewById<TextView>(Resource.Id.tvCopyRight);
        tvCopyRight.Text = "© Aarib Soft";
    }

    protected override void OnResume()
    {
        base.OnResume();

        InitPermissionCheck();
    }

    private void CheckForLogin(object param)
    {
        Task.Run(async () =>
        {
            // If it is the first use, then user must go to login
            if (TZM.IS_LOGGED_IN == false)
            {
                HM.Container.Resolve<IUtilityService>().MoveToLogin(this);
                return;
            }

            try
            {
                GetCurrentUserProfileCommandResult? result = await HM.Container.Resolve<TzCommandExecutor>()
                    .ExecuteVoidCommand<GetCurrentUserProfileCommandResult>(nameof(GetCurrentUserProfileCommand));
                // Bind user information with container
                HM.Container.BindInstance(result.User);
                HM.Container.Resolve<IUtilityService>().MoveToMain();
            }
            catch (Exception e)
            {
                HM.Container.Resolve<IUtilityService>().MoveToLogin();
            }
        });
    }

    private void CheckForLatestVersion(Action<object> afterVersionCheck)
    {
        try
        {
            RunInvokeRequired(_ => { NotificationUtils.DeveloperToast(this, "Checking for version"); });
            TZMPrefs? tzPrefs = HM.Container.Resolve<TZMPrefs>();
            int currentVersion = HM.Container.Resolve<IUtilityService>().GetAppVersionCode();
            if (currentVersion < 1)
            {
                return;
            }

            Task.Run(async () =>
            {
                CheckAppVersionCommandResult res;
                IUtilityService? utilityService = HM.Container.Resolve<IUtilityService>();
                try
                {
                    CheckAppVersionCommandPayload payload = new CheckAppVersionCommandPayload()
                    {
                        BranchId = HConfigManager.Get<TZMConfig>().BranchId,
                        LastAppVersion = tzPrefs.GetLastAppVersion(),
                        AppVersion = TZM.APP_VERSION
                    };
                    CommandRequest request = CommandRequest.Create(payload);
                    res = await HM.Container.Resolve<TzCommandExecutor>()
                        .ExecuteCommand<CheckAppVersionCommandResult>(request);
                }
                catch (HException ex)
                {
                    utilityService.AlertBox(ex.Message, "Error", neutralButton: "OK");
                    return;
                }
                catch (Exception ex)
                {
                    utilityService.AlertBox("Unable to connect to server", "Error", neutralButton: "OK");
                    return;
                }

                if (res.ResetUserToken)
                {
                    tzPrefs.SetAccessToken(string.Empty);
                }

                if (res?.UpdateAvailable == false)
                {
                    tzPrefs.SetLastAppVersion(utilityService.GetAppVersionCode());
                    afterVersionCheck(null);
                    return;
                }

                // Update available
                utilityService.AlertBox(res.Forced
                        ? Constants.Messages.APP_UPDATE_AVAILABE_FORECE
                        : Constants.Messages.APP_UPDATE_AVAILABE, GetString(Resource.String.update_available),
                    null, null,
                    GetString(Resource.String.update_now),
                    positiveButtonClicked: UpdateButtonClicked,
                    res.Forced ? null : GetString(Resource.String.later),
                    res.Forced ? null : afterVersionCheck);
            });
        }
        catch (Exception ex)
        {
            HM.Container.Resolve<IUtilityService>().AlertBox("Unable to connect to server", "Error",
                neutralButton: "OK",
                neutralButtonClicked: _ => { HM.Container.Resolve<IUtilityService>().ExitApplication(); });
        }
    }

    private void UpdateButtonClicked(object obj)
    {
        string packageName = HM.Container.Resolve<IUtilityService>().GetAppPackageName();
        if (string.IsNullOrWhiteSpace(packageName))
        {
            return;
        }

        try
        {
            StartActivity(new Intent(Intent.ActionView,
                Android.Net.Uri.Parse(
                    string.Format(Constants.Endpoints.PLAYSTORE_MARKET_APP_DETAILS,
                        packageName))));
        }
        catch (Exception)
        {
            try
            {
                StartActivity(new Intent(Intent.ActionView,
                    Android.Net.Uri.Parse(
                        string.Format(Constants.Endpoints.PLAYSTORE_WEB_APP_DETAILS,
                            packageName))));
            }
            catch
            {
                // Ignore
            }
        }
        finally
        {

        }
    }

    #region Permission Checking

    /********************************************************************************
    *                                    Permission                                 
    ********************************************************************************/
    private static int _permissionRetryCount = 0;

    private readonly String[] _requiredPermissions = new String[]
    {
        Manifest.Permission.AccessWifiState,
        Manifest.Permission.AccessNetworkState,
        Manifest.Permission.Internet,
        //Manifest.Permission.ReadPhoneState
    };

    private List<bool> _permissionsRequestResponse = new List<bool>();

    private void InitPermissionCheck()
    {
        //permission
        _permissionRetryCount = HConfigManager.Get<HatfMobile.HatfMobileConfig>().PermissionRetries;
        NotificationUtils.DeveloperToast(this, "Checking for permission");
        Task.Run(CheckForPermissionsContinously);
    }

    private void CheckForPermissions()
    {
        try
        {
            _permissionsRequestResponse = new List<bool>();
            List<string> permissionToRequest = new List<string>();
            foreach (string v in _requiredPermissions)
            {
                if (ContextCompat.CheckSelfPermission(this, v) != Permission.Granted)
                {
                    permissionToRequest.Add(v);
                }
                else
                {
                    _permissionsRequestResponse.Add(true);
                }
            }

            if (permissionToRequest.Count > 0)
            {
                RequestPermissions(permissionToRequest.ToArray(), (int)Permission.Granted);
            }
        }
        catch (Exception ex)
        {
            NotificationUtils.UserToast(this, "Error occured while requesting permissions: " + ex.Message);
        }
    }

    private void CheckForPermissionsContinously()
    {
        _permissionRetryCount--;
        CheckForPermissions();
        while (_permissionsRequestResponse.Count < _requiredPermissions.Length)
        {
            Task.Delay(200).Wait();
        }

        if (_permissionsRequestResponse.Any(m => m == false))
        {
            if (_permissionRetryCount > 0)
            {
                CheckForPermissionsContinously();
            }
            else
            {
                HM.Container.Resolve<IUtilityService>().RunInvokeRequired((object param) =>
                {
                    HM.Container.Resolve<IUtilityService>().AlertBox(Constants.Messages.ALLOW_PERMISSIONS,
                        "Alert", "OK",
                        (object para) => { HM.Container.Resolve<IUtilityService>().ExitApplication(); });
                });
            }
        }
        else
        {
            RunInvokeRequired(_ => { NotificationUtils.DeveloperToast(this, "Finished permission check"); });
            // Permission check finished
            CheckForLatestVersion(CheckForLogin);
        }
    }

    public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
        [GeneratedEnum] Permission[] grantResults)
    {
        _permissionsRequestResponse.Add(grantResults[0] == Permission.Granted);

        base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    #endregion

    public void RunInvokeRequired(Action<object> function, object parameters = null)
    {
        if (Looper.MyLooper() != Looper.MainLooper)
        {
            RunOnUiThread(() => { function(parameters); });
        }
        else
        {
            function(parameters);
        }
    }

}
