﻿using Android.Runtime;
using Android.Util;
using AndroidX.SwipeRefreshLayout.Widget;
using Google.Android.Material.FloatingActionButton;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar")]
    public class StockReportActivity : BaseActivity
    {
        private RecyclerView? rvStockReport;
        private List<StockReportModel> currentList;
        private TextView tvLastUpdatedOn;
        private Spinner spCompany;
        private Spinner spGroup;
        private EditText etSearchText;
        private ImageButton btnClearSearchText;
        private List<string> companies, groups;
        private ArrayAdapter<String> arrayAdapter;

        private SwipeRefreshLayout srlStockReport;
        //AdView avBannerStockReport;
        
        private FloatingActionButtonWithBadge floatingActionButtonWithBadge;
        private FloatingActionButton fabCart;
        private ShoppingCartSystem _shoppingCartSystem;
        private DateTime _lastUpdated;

        protected override bool RequireLogin => true;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.stock_report);
            string partyName = Intent.GetStringExtra("PartyName");
            setBackBtnTitleBar(partyName, Resource.Menu.toolbar_menu_one_item, new List<mdCustomMenuItem>()
            {
                new mdCustomMenuItem()
                {
                    ClickedEvent = ReloadClickListener,
                    DrawableResource = Resource.Drawable.ic_autorenew_white_24dp,
                    MenuItemId = Resource.Id.toolbar_menu_one_item1,
                }
            });

            Init();
        }

        private void Init()
        {
            rvStockReport = (RecyclerView)FindViewById(Resource.Id.rvStockReport);
            rvStockReport.SetLayoutManager(new LinearLayoutManager(this));
            tvLastUpdatedOn = (TextView)FindViewById(Resource.Id.tvLastUpdatedOn);
            spCompany = (Spinner)FindViewById(Resource.Id.spCompany);
            spGroup = (Spinner)FindViewById(Resource.Id.spGroup);
            etSearchText = (EditText)FindViewById(Resource.Id.etSearchText);
            btnClearSearchText = (ImageButton)FindViewById(Resource.Id.btnClearSearchText);
            srlStockReport = (SwipeRefreshLayout)FindViewById(Resource.Id.srlStockReport);
            btnClearSearchText.SetImageResource(Resource.Drawable.icclearorange);
            AfterInitActivity();
            StartSearchFilterListeners();

            _lastUpdated = HM.Container.Resolve<DateTime>(MainActivity.STOCK_LAST_UPDATED_TIME);
            ShowProgress();
            Task.Run(LoadReport);
            //local ad
            InitAd();
            
            fabCart = FindViewById<FloatingActionButton>(Resource.Id.fabCart);
            if (HConfigManager.Get<TZMConfig>().ShowCartButton == false)
            {
                fabCart.Visibility = ViewStates.Gone;
            }
            else
            {
                fabCart.Click += FabOnClick;
            
                _shoppingCartSystem = HM.Container.Resolve<ShoppingCartSystem>();
                floatingActionButtonWithBadge = new FloatingActionButtonWithBadge(
                    FindViewById<FrameLayout>(Resource.Id.flFABBadge)?.FindViewById<TextView>(Resource.Id.tvCartBadge));
                _shoppingCartSystem.AddOnUpdatedEventListener(OnCartUpdated);
                
                // Set current length
                UpdateBadgeCount();
            }
        }

        private void OnCartUpdated(string itemId, uint quantity, long totalQuantity)
        {
            UpdateBadgeCount();
        }

        private void UpdateBadgeCount()
        {
            floatingActionButtonWithBadge.SetBadgeCount(_shoppingCartSystem.TotalQuantity);
        }
        
        private void FabOnClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ShoppingCartActivity));
            intent.AddFlags(ActivityFlags.ReorderToFront);
            StartActivityForResult(intent, 0);
        }

        private void InitAd()
        {
            InitBanner(Resource.Id.avBannerStockReport);
        }

        private void LoadReport()
        {
            try
            {
                RunInvokeRequired((object param) =>
                {
                    UpdateReportAdapter(HM.Container.Resolve<mdProductsWithStock>().ReportData);

                    String lastUpdatedOn = "Last Updated On: " + DateTime.Now.ToString(Constants.DATE_TIME_FORMAT_ddd_dd_MMM_yyyy_hh_mm_tt);
                    tvLastUpdatedOn.Text = lastUpdatedOn;

                    companies = GetDistinctCompaniesFromList(HM.Container.Resolve<mdProductsWithStock>().ReportData);
                    arrayAdapter = new ArrayAdapter<String>(this,
                            Android.Resource.Layout.SimpleSpinnerDropDownItem, companies);
                    spCompany.Adapter = arrayAdapter;

                    groups = GetDistinctGroupsFromList(HM.Container.Resolve<mdProductsWithStock>().ReportData);
                    arrayAdapter = new ArrayAdapter<String>(this,
                            Android.Resource.Layout.SimpleSpinnerDropDownItem, groups);
                    spGroup.Adapter = arrayAdapter;

                    srlStockReport.Refreshing = false;
                    HideProgress();
                });
            }
            catch (Exception ex)
            {
                Log.Debug("STATUS", "Error in loading report\n" + ex.ToString());
                srlStockReport.Refreshing = false;
                HideProgress();
                NotificationUtils.UserToast(this, "Sorry! Something went wrong in loading report");
            }
        }

        private List<string> GetDistinctCompaniesFromList(List<StockReportModel> lst)
        {
            List<string> distinctItems = new List<string>();
            distinctItems.Add("All");
            foreach (StockReportModel srm in lst)
            {
                if (srm.CompanyName != null)
                {
                    if (!string.IsNullOrEmpty(srm.CompanyName))
                    {
                        if (distinctItems.IndexOf(srm.CompanyName) == -1)
                        {
                            distinctItems.Add(srm.CompanyName);
                        }
                    }
                }
            }
            String[] response = new String[distinctItems.Count];
            return distinctItems;
        }

        private List<string> GetDistinctGroupsFromList(List<StockReportModel> lst)
        {
            List<String> distinctItems = new List<string>();
            distinctItems.Add("All");
            foreach (StockReportModel srm in lst)
            {
                if (srm.GroupName != null)
                {
                    if (!string.IsNullOrEmpty(srm.GroupName))
                    {
                        if (distinctItems.IndexOf(srm.GroupName) == -1)
                        {
                            distinctItems.Add(srm.GroupName);
                        }
                    }
                }
            }
            String[] response = new String[distinctItems.Count];
            return distinctItems;
        }

        private void SyncStockData()
        {
            Task.Run(async () =>
            {
                ShowProgress();
                try
                {
                    GetProductsStockCommandResult? result =
                        await _commandExecutor.ExecuteVoidCommand<GetProductsStockCommandResult>(
                            nameof(GetProductsStockCommand));
                    
                    List<ProductStockModel>? stocks = result?.Products ?? [];
                    mdProductsWithStock? oldStock = _container.Resolve<mdProductsWithStock>();
                    oldStock.ReportData ??= new List<StockReportModel>();
                    for (int i = 0; i < oldStock.ReportData.Count; i++)
                    {
                        ProductStockModel? newStock = stocks.FirstOrDefault(m => m.ProductID == oldStock.ReportData[i].ProductID);
                        if (newStock == null)
                        {
                            oldStock.ReportData[i].Stock = 0;
                        }
                        else
                        {
                            oldStock.ReportData[i].Stock = newStock.Stock;
                        }
                    }
                    
                    _lastUpdated = DateTime.Now;
                    HM.Container.RebindInstanceWithId(_lastUpdated, MainActivity.STOCK_LAST_UPDATED_TIME);
                }
                catch (Exception ex)
                {
                    NotifyError(ex.ToString(), "Sorry! Could not connect to the server");
                    return;
                }
                // Clear the db
                // SQLiteBase.Instance.DeleteAll<StockReportModel>();
                if (HM.Container.Resolve<mdProductsWithStock>().ReportData.Count < 1)
                {
                    NotifyError(string.Empty, "Sorry! no data found");
                    return;
                }
                // try
                // {
                //     bool inserted = SQLiteBase.Instance.InsertAll(HM.Container.Resolve<mdStockReport>().ReportData);
                //     Log.Debug("STATUS", "Stock Record Insert Successfully");
                // }
                // catch (Exception ex)
                // {
                //     NotifyError(ex.ToString(), "Server was found but an exception was occured.\nPlease report system admin.");
                //     return;
                // }
                // HM.Container.Resolve<TZMPrefs>().SetLastSyncTime(DateTime.Now.ToString(Constants.DATE_TIME_FORMAT));
                LoadReport();
            });
        }

        private void NotifyError(string exception, string userMessage)
        {
            Log.Debug("STATUS", userMessage + "\n" + exception);
            RunInvokeRequired((object param) =>
            {
                try
                {
                    srlStockReport.Refreshing = false;
                    HideProgress();
                    NotificationUtils.UserToast(this, userMessage);
                }
                catch(Exception ex)
                {
                    throw;
                }
            });
        }

        private void ReloadStockFromServer()
        {
            Task.Run(() =>
            {
                RunInvokeRequired((object param) => {
                    Utils.HideKeyboardFrom(this);
                });
                UpdateReportAdapter();
                SyncStockData();
            });
        }

        private void ReloadClickListener(object sender)
        {
            ReloadStockFromServer();
        }

        private void SrlStockReport_Refresh(object sender, EventArgs e)
        {
            ReloadStockFromServer();
        }

        private void UpdateReportAdapter(List<StockReportModel> reportData = null)
        {
            RunInvokeRequired(_ =>
            {
                currentList = reportData ?? new List<StockReportModel>();
                rvStockReport.SetAdapter(new StockReportAdapter(this, currentList));
                rvStockReport.SetLayoutManager(new LinearLayoutManager(this));
            });
        }
        
        #region Search Related

        private void StartSearchFilterListeners()
        {
            spCompany.ItemSelected += SpCompany_ItemSelected;
            spCompany.NothingSelected += SpCompany_NothingSelected;

            spGroup.ItemSelected += SpCompany_ItemSelected;
            spGroup.NothingSelected += SpCompany_NothingSelected;

            etSearchText.TextChanged += EtSearchText_TextChanged;

            btnClearSearchText.Click += BtnClearSearchText_Click;
            srlStockReport.Refresh += SrlStockReport_Refresh;

        }

        private void ApplySearches()
        {
            try
            {
                List<StockReportModel>? list = HM.Container.Resolve<mdProductsWithStock>().ReportData;
                int companyPosition = spCompany.SelectedItemPosition;
                int groupPosition = spGroup.SelectedItemPosition;
                String searchText = etSearchText.Text;
                List<StockReportModel>? searchAppliedList = new List<StockReportModel>();
                if (companyPosition == 0 && groupPosition == 0 && string.IsNullOrEmpty(searchText))
                {
                    if (list != null)
                    {
                        searchAppliedList = new List<StockReportModel>(list);
                    }
                }
                else
                {
                    searchAppliedList = new List<StockReportModel>();

                    //Applying Company Search
                    if (companyPosition == 0) //All
                    {
                        searchAppliedList = new List<StockReportModel>(list);
                    }
                    else
                    {
                        if (list != null)
                        {
                            if (list.Count > 0)
                            {
                                if (companies?.Count > 0)
                                {
                                    string selectedCompany = companies[companyPosition];
                                    if (!string.IsNullOrWhiteSpace(selectedCompany))
                                    {
                                        searchAppliedList = list.Where(m => m?.CompanyName != null &&
                                            m.CompanyName.Equals(selectedCompany,
                                                StringComparison.CurrentCultureIgnoreCase)).ToList();
                                    }
                                }
                            }
                        }
                    }

                    //Applying Group Search
                    if (searchAppliedList.Count > 0)
                    {
                        if (groupPosition > 0)
                        {
                            if (groups?.Count > 0)
                            {
                                string selectedGroup = groups[groupPosition].ToUpper();
                                if (!string.IsNullOrWhiteSpace(selectedGroup))
                                {
                                    searchAppliedList = searchAppliedList.Where(m => m?.GroupName != null &&
                                            m.GroupName.Contains(selectedGroup,
                                                StringComparison.CurrentCultureIgnoreCase))
                                        .ToList();
                                }
                            }
                        }
                    }

                    //Applying Group Search
                    if (searchAppliedList?.Count > 0)
                    {
                        //Applying Search Text
                        if (!string.IsNullOrWhiteSpace(searchText))
                        {
                            searchAppliedList = searchAppliedList.Where(m => m?.ProductName != null &&
                                m.ProductName.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)).ToList();
                        }
                    }
                }

                UpdateReportAdapter(searchAppliedList);
            }
            catch (Exception ex)
            {
                NotificationUtils.UserToast(this, "An Exception Occoured. Please inform Developer \n" + ex.ToString());
            }
        }

        private void SpCompany_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            ApplySearches();
        }

        private void SpCompany_NothingSelected(object sender, AdapterView.NothingSelectedEventArgs e)
        {
            Spinner sp = (Spinner)sender;
            sp.SetSelection(0);
        }

        private void EtSearchText_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            ApplySearches();
        }

        private void BtnClearSearchText_Click(object sender, EventArgs e)
        {
            etSearchText.Text = "";
        }

        #endregion

        public override void Finish()
        {
            _shoppingCartSystem?.RemoveOnUpdatedEventListener(OnCartUpdated);
            base.Finish();
        }
        
        protected override void OnResume()
        {
            // Update the price if shown
            UpdateBadgeCount();
            base.OnResume();
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            if (resultCode == Result.Ok)
            {
                Finish();
            }
        }

    }

}