﻿using Google.Android.Material.TextField;

namespace TZAndroid
{
    [Activity(Label = "@string/update_account_info", Theme = "@style/NoActionBar", WindowSoftInputMode = SoftInput.AdjustResize)]
    public class UpdateAccountInfoActivity : BaseActivity
    {
        private Switch switchUCIUpdateUserName, switchUCIUpdatePassword;
        private EditText etUCIUserName, etUCINewUserName, etUCICurrentPassword, etUCINewPassword, etUCIConfirmPassword;
        private AppCompatButton btnUIProceed;

        private TextInputLayout uci_user_name_text_input_layout,
            uci_new_user_name_text_input_layout,
            uci_new_password_text_input_layout,
            uci_confirm_password_text_input_layout;
        protected override bool RequireLogin => true;
        protected override void OnCreate(Android.OS.Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.update_account_info_acitivity);
            setBackBtnTitleBar(GetString(Resource.String.update_account_info));
            Init();

        }

        private void Init()
        {
            switchUCIUpdateUserName = (Switch)FindViewById(Resource.Id.switchUCIUpdateUserName);
            switchUCIUpdatePassword = (Switch)FindViewById(Resource.Id.switchUCIUpdatePassword);
            
            uci_user_name_text_input_layout = (TextInputLayout)FindViewById(Resource.Id.uci_user_name_text_input_layout);
            uci_new_user_name_text_input_layout = (TextInputLayout)FindViewById(Resource.Id.uci_new_user_name_text_input_layout);
            uci_new_password_text_input_layout = (TextInputLayout)FindViewById(Resource.Id.uci_new_password_text_input_layout);
            uci_confirm_password_text_input_layout = (TextInputLayout)FindViewById(Resource.Id.uci_confirm_password_text_input_layout);
            
            etUCIUserName = (EditText)FindViewById(Resource.Id.etUCIUserName);
            etUCINewUserName = (EditText)FindViewById(Resource.Id.etUCINewUserName);
            etUCICurrentPassword = (EditText)FindViewById(Resource.Id.etUCICurrentPassword);
            etUCINewPassword = (EditText)FindViewById(Resource.Id.etUCINewPassword);
            etUCIConfirmPassword = (EditText)FindViewById(Resource.Id.etUCIConfirmPassword);
            btnUIProceed = (AppCompatButton)FindViewById(Resource.Id.btnUIProceed);

#if DEBUG
            etUCIConfirmPassword.Text = TZM.Password;
            etUCINewPassword.Text = TZM.Password;
            etUCICurrentPassword.Text = TZM.Password;
            etUCINewUserName.Text = TZM.UserName;
            etUCIUserName.Text = TZM.UserName;
#endif
            
            //binding events
            switchUCIUpdateUserName.CheckedChange += SwitchUCIUpdateUserName_CheckedChange;
            switchUCIUpdatePassword.CheckedChange += SwitchUCIUpdatePassword_CheckedChange;
            btnUIProceed.Click += BtnUIProceed_Click;

            if (switchUCIUpdatePassword.Checked)
            {
                uci_new_password_text_input_layout.Visibility = ViewStates.Visible;
                uci_confirm_password_text_input_layout.Visibility = ViewStates.Visible;
            }
            else
            {
                uci_new_password_text_input_layout.Visibility = ViewStates.Gone;
                uci_confirm_password_text_input_layout.Visibility = ViewStates.Gone;
            }

            if (switchUCIUpdateUserName.Checked)
            {
                uci_new_user_name_text_input_layout.Visibility = ViewStates.Visible;
            }
            else
            {
                uci_new_user_name_text_input_layout.Visibility = ViewStates.Gone;
            }

            etUCIUserName.Enabled = false;
            etUCIUserName.Text = HM.Container.Resolve<UserInformationVO>().UserName;
            AfterInitActivity();
            
            HandleProceedButtonDisplay();
        }

        private void BtnUIProceed_Click(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                ShowProgress();
                try
                {
                    UserInformationVO? userInformationVO = HM.Container.Resolve<UserInformationVO>();
                    //filling model
                    mdUserInformation ui = new mdUserInformation
                    {
                        NewUserName = etUCINewUserName.Text,
                        CurrentPassword = etUCICurrentPassword.Text,
                        NewPassword = etUCINewPassword.Text
                    };

                    mdCallResponse res = new mdCallResponse();
                    //validation

                    if (switchUCIUpdateUserName.Checked)
                    {
                        //verify new user name
                        if (string.IsNullOrWhiteSpace(ui.NewUserName))
                        {
                            res.AppedMessageNL(Constants.Messages.NEWUSERNAME_REQUIRED);
                        }
                        else if(ui.NewUserName == userInformationVO.UserName)
                        {
                            res.AppedMessageNL(Constants.Messages.NEWANDOLD_USERNAME_MUST_BE_DIFFERENT);
                        }
                    }

                    if (switchUCIUpdatePassword.Checked)
                    {
                        if (string.IsNullOrWhiteSpace(ui.CurrentPassword))
                        {
                            res.AppedMessageNL(Constants.Messages.PASSWORD_REQUIRED);
                        }

                        //verify new password
                        if (string.IsNullOrWhiteSpace(ui.NewPassword))
                        {
                            res.AppedMessageNL(Constants.Messages.NEWPASSWORD_REQUIRED);
                        }

                        if (string.IsNullOrWhiteSpace(etUCIConfirmPassword.Text))
                        {
                            res.AppedMessageNL(Constants.Messages.CONFIRMPASSWORD_REQUIRED);
                        }

                        if (ui.NewPassword.Equals(ui.CurrentPassword, StringComparison.Ordinal))
                        {
                            res.AppedMessageNL(Constants.Messages.NEWANDOLD_PASSWORD_MUST_BE_DIFFERENT);
                        }

                        if (!ui.NewPassword.Equals(etUCIConfirmPassword.Text, StringComparison.Ordinal))
                        {
                            res.AppedMessageNL(Constants.Messages.PASSWORD_CONFIRMPASSWORD_MISSMATCH);
                        }

                    }
                    //end validation

                    if (!string.IsNullOrWhiteSpace(res.Message))
                    {
                        HideProgressAndShowAlert(res.Message);
                        return;
                    }
                    
                    AlertBox(this, Constants.Messages.UPDATE_USERINFO_CONFIRMATION, GetString(Resource.String.confirmation), null, null, GetString(Resource.String.YES), (object param) =>
                    {
                        Task.Run(async () =>
                        {
                            try
                            {
                                await _commandExecutor.ExecuteVoidCommand(new UpdateUserLoginInfoCommandPayload()
                                {
                                    CurrentPassword = ui.CurrentPassword,
                                    NewPassword = ui.NewPassword,
                                    NewUserName = ui.NewUserName
                                });
                                _container.Resolve<IUtilityService>().AlertBox("Updated Successfully", "Success", "OK",
                                    (object para) => { _container.Resolve<IUtilityService>().MoveToLogin(); });
                            }
                            catch (Exception e)
                            {
                                HLogger.Error(e, "Exception occured while executing http request");
                                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
                            }

                            HideProgress();
                        });

                    }, GetString(Resource.String.NO), (object param) =>
                    {
                        HideProgress();
                    });
                }
                catch (Exception ex)
                {
                    HideProgressAndShowAlert(Constants.Messages.SOMETHING_WENT_WRONG + System.Environment.NewLine + ex.Message);
                }
            });
        }

        private void SwitchUCIUpdatePassword_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            if (e.IsChecked)
            {
                uci_new_password_text_input_layout.Visibility = ViewStates.Visible;
                uci_confirm_password_text_input_layout.Visibility = ViewStates.Visible;
            }
            else
            {
                uci_new_password_text_input_layout.Visibility = ViewStates.Gone;
                uci_confirm_password_text_input_layout.Visibility = ViewStates.Gone;
            }
            
            HandleProceedButtonDisplay();
        }

        private void SwitchUCIUpdateUserName_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            if (e.IsChecked)
            {
                uci_new_user_name_text_input_layout.Visibility = ViewStates.Visible;
            }
            else
            {
                uci_new_user_name_text_input_layout.Visibility = ViewStates.Gone;
            }

            HandleProceedButtonDisplay();
        }

        private void HandleProceedButtonDisplay()
        {
            if (switchUCIUpdatePassword.Checked == false && switchUCIUpdateUserName.Checked == false)
            {
                btnUIProceed.Visibility = ViewStates.Gone;
            }
            else
            {
                btnUIProceed.Visibility = ViewStates.Visible;
            }
        }
        
    }
}