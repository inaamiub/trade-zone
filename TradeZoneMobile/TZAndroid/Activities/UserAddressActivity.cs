﻿using Android.Text;
using Google.Android.Material.Button;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar", WindowSoftInputMode = SoftInput.AdjustResize)]
    public class UserAddressActivity : BaseActivity
    {
        private EditText _etUIName, _etUICell, _etUICity, _etUIAddress, _etUICountry;
        private AutoCompleteTextView actUIState;
        private MaterialButton _btnSaveAddress;

        public const string USER_ADDRESS_ID = "USER_ADDRESS_ID";

        private List<string> _states = HConfigManager.Get<TZSharedConfig>().States;
        private UserAddressVO _userAddressVo = null;
        private UserInformationVO _userInformationVo = null;
        
        protected override bool RequireLogin => true;
        protected override void OnCreate(Android.OS.Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.user_address);
            setBackBtnTitleBar(GetString(Resource.String.user_address), Resource.Menu.toolbar_menu_one_item);
            
            string addressId = Intent.Extras.GetString(USER_ADDRESS_ID, string.Empty);
            if (string.IsNullOrEmpty(addressId) == false)
            {
                if (HM.Container.TryResolve(out List<UserAddressVO> userAddressesVos, ContainerConstants.ALL_USER_ADDRESSES))
                {
                    _userAddressVo = userAddressesVos.FirstOrDefault(m => m.Id == addressId);
                }
                else
                {
                    // Address not found, consider new address
                }
            }

            _userInformationVo = HM.Container.Resolve<UserInformationVO>();
            Init();
        }

        private void Init()
        {
            if (_userInformationVo.UserType != UserType.FullAccess && _userInformationVo.UserType != UserType.PublicAccess)
            {
                AlertBox(this, TZErrorMessages.GetErrorMessage(TZErrorCodes.GUEST_NOT_ALLOWED));
                return;
            }
            
            _etUIName = (EditText)FindViewById(Resource.Id.etUIName);
            _etUICell = (EditText)FindViewById(Resource.Id.etUICell);
            _etUICity = (EditText)FindViewById(Resource.Id.etUICity);
            _etUIAddress = (EditText)FindViewById(Resource.Id.etUIAddress);
            actUIState = (AutoCompleteTextView)FindViewById(Resource.Id.actUIState);
            _etUICountry = (EditText)FindViewById(Resource.Id.etUICountry);

            ArrayAdapter<string>? adapter = new ArrayAdapter<string>(this, Resource.Layout.list_item, _states);
            actUIState.Adapter = adapter;
            actUIState.SetRawInputType(InputTypes.Null);
            actUIState.FocusChange += ActUIStateOnFocusChange;

            if (_userAddressVo != null)
            {
                _etUIName.Text = _userAddressVo.Name;
                _etUICell.Text = _userAddressVo.CellNo;
                _etUICity.Text = _userAddressVo.City;
                _etUIAddress.Text = _userAddressVo.Address;
                actUIState.SetText(_userAddressVo.State, false);
                _etUICountry.Text = _userAddressVo.Country;
            }
            
            _btnSaveAddress = FindViewById<MaterialButton>(Resource.Id.btnSaveAddress);
            _btnSaveAddress.Click += BtnSaveAddressClick;
            AfterInitActivity();
        }

        private void ActUIStateOnFocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus)
            {
                Utils.HideKeyboardFrom(this);
            }
        }

        private void BtnSaveAddressClick(object sender, EventArgs e)
        {
            ShowProgress();
            Task.Run(async () =>
            {
                bool isNew = string.IsNullOrEmpty(_userAddressVo?.Id);
                if (isNew)
                {
                    _userAddressVo = new UserAddressVO();
                }

                _userAddressVo.Name = _etUIName.Text;
                _userAddressVo.CellNo = _etUICell.Text;
                _userAddressVo.Address = _etUIAddress.Text;
                _userAddressVo.City = _etUICity.Text;
                _userAddressVo.State = actUIState.Text;
                _userAddressVo.Country = _etUICountry.Text;
                _userAddressVo.UUID = HatfShared.Utils.NewShortGuid();

                _userAddressVo.Trim();
                try
                {
                    _userAddressVo.ValidateUserAddress(_userInformationVo);
                    try
                    {
                        SetUserAddressCommandPayload result =
                            await _commandExecutor.ExecuteCommand<SetUserAddressCommandPayload>(
                                new SetUserAddressCommandPayload()
                                {
                                    Address = _userAddressVo,
                                    IsNew = isNew
                                });
                        _userAddressVo = result.Address;
                    }
                    catch (Exception e)
                    {
                        NotificationUtils.DeveloperToast(this, e);
                        AlertBox(e.Message, "Error", neutralButton: "OK");
                        return;
                    }
                    
                    if (_userAddressVo != null)
                    {
                        if (_container.TryResolve(out List<UserAddressVO> userAddressesVos,
                                ContainerConstants.ALL_USER_ADDRESSES) == false)
                        {
                            userAddressesVos = new List<UserAddressVO>();
                            _container.BindInstanceWithId(userAddressesVos, ContainerConstants.ALL_USER_ADDRESSES);
                        }

                        UserAddressVO? existingAddress = userAddressesVos.FirstOrDefault(m => m.Id == _userAddressVo.Id);
                        if (existingAddress == null)
                        {
                            // Add address to list
                            userAddressesVos.Add(_userAddressVo);
                        }
                        else
                        {
                            // Address already exists in the list, update on it's index
                            int index = userAddressesVos.IndexOf(existingAddress);
                            userAddressesVos[index] = _userAddressVo;
                        }

                        AlertBox(this, "Address saved successfully", "Success", positiveButton: "Yes",
                            positiveButtonClicked: _ => {
                                RunInvokeRequired(_ =>
                                {
                                    Intent? intent = new Intent();
                                    intent.PutExtra(USER_ADDRESS_ID, _userAddressVo.Id);
                                    SetResult(Result.Ok, intent);
                                    Finish();
                                });
                            });
                    }
                }
                catch (Exception ex)
                {
                    HideProgressAndShowAlert(ex.Message);
                }
                finally
                {
                    HideProgress();   
                }
            });
        }

    }
}