﻿using Android.Text;
using Google.Android.Material.TextField;

namespace TZAndroid
{
    [Activity(Theme = "@style/NoActionBar", WindowSoftInputMode = SoftInput.AdjustResize)]
    public class UserInformationActivity : BaseActivity
    {
        private EditText etUIName, etUICell, etUICity, etUIAddress, etUICountry, etUIEmail, etUIUserName, 
            etUINewPassword, etUIConfirmPassword;

        private AutoCompleteTextView actUIState;
        private Button btnUIProceed;
        private TextInputLayout til_user_name, til_new_password, til_confirm_password;
        private UserType userType;
        private TextView tvPrivacyPolicy;
        private CheckBox cbPrivacyPolicy;

        private List<string> _states = HConfigManager.Get<TZSharedConfig>().States;
        protected override bool RequireLogin => false;

        protected override void OnCreate(Android.OS.Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.user_information_activity);
            setBackBtnTitleBar(GetString(Resource.String.user_information), Resource.Menu.toolbar_menu_one_item, new List<mdCustomMenuItem>()
            {
                new mdCustomMenuItem()
                {
                    ClickedEvent = btnReloadClickListener,
                    DrawableResource = Resource.Drawable.ic_autorenew_white_24dp,
                    MenuItemId = Resource.Id.toolbar_menu_one_item1,
                }
            });
            if (TZM.IS_LOGGED_IN == false)
            {
                string uType = Intent.Extras.GetString(Constants.Keys.USERACCESSTYPE_KEY, string.Empty);
                if (Enum.IsDefined(typeof(UserType), uType) == false)
                {
                    StartActivity(new Intent(this, typeof(LoginActivity)));
                    Finish();
                    return;
                }
                userType = EnumUtils.ParseEnum<UserType>(uType);
            }
            else
            {
                userType = HM.Container.Resolve<UserInformationVO>().UserType;
            }

            if (!Enum.IsDefined(typeof(UserType), userType))
            {
                StartActivity(new Intent(this, typeof(LoginActivity)));
                Finish();
                return;
            }
            Init();
        }

        private void btnReloadClickListener(object sender)
        {
            etUIName.Text = "";
            etUICell.Text = "";
            etUICity.Text = "";
            etUIAddress.Text = "";
            etUIEmail.Text = "";
            etUIUserName.Text = "";
            etUINewPassword.Text = "";
            etUIConfirmPassword.Text = "";
        }

        private void Init()
        {
            etUIName = (EditText)FindViewById(Resource.Id.etUIName);
            etUICell = (EditText)FindViewById(Resource.Id.etUICell);
            etUICity = (EditText)FindViewById(Resource.Id.etUICity);
            etUIAddress = (EditText)FindViewById(Resource.Id.etUIAddress);
            actUIState = (AutoCompleteTextView)FindViewById(Resource.Id.actUIState);
            etUICountry = (EditText)FindViewById(Resource.Id.etUICountry);
            etUIEmail = (EditText)FindViewById(Resource.Id.etUIEmail);
            etUIUserName = (EditText)FindViewById(Resource.Id.etUIUserName);
            etUINewPassword = (EditText)FindViewById(Resource.Id.etUINewPassword);
            etUIConfirmPassword = (EditText)FindViewById(Resource.Id.etUIConfirmPassword);
            tvPrivacyPolicy = FindViewById<TextView>(Resource.Id.tvPrivacyPolicy);
            cbPrivacyPolicy = FindViewById<CheckBox>(Resource.Id.cbPrivacyPolicy);
            tvPrivacyPolicy.Click += TvPrivacyPolicyOnClick;

            ArrayAdapter<string>? adapter = new ArrayAdapter<string>(this, Resource.Layout.list_item, _states);
            actUIState.Adapter = adapter;
            actUIState.SetRawInputType(InputTypes.Null);
            actUIState.FocusChange += ActUIStateOnFocusChange;
            
            til_user_name = (TextInputLayout)FindViewById(Resource.Id.ui_user_name_text_input_layout);
            til_new_password = (TextInputLayout)FindViewById(Resource.Id.ui_new_password_text_input_layout);
            til_confirm_password = (TextInputLayout)FindViewById(Resource.Id.ui_confirm_password_text_input_layout);
#if DEBUG
            etUIName.Text = TZM.Name;
            etUICell.Text = TZM.CellNo;
            etUICity.Text = TZM.City;
            etUIAddress.Text = TZM.Address;
            etUIEmail.Text = TZM.Email;
            etUIUserName.Text = TZM.UserName;
            etUINewPassword.Text = TZM.Password;
            etUIConfirmPassword.Text = TZM.Password;
            actUIState.SetText(TZM.State, false);
            cbPrivacyPolicy.Checked = true;
#endif
            btnUIProceed = (Button)FindViewById(Resource.Id.btnUIProceed);
            btnUIProceed.Click += BtnUIProceed_Click;

            switch (userType)
            {
                case UserType.AsGuest:
                    til_user_name.Visibility = ViewStates.Gone;
                    til_new_password.Visibility = ViewStates.Gone;
                    til_confirm_password.Visibility = ViewStates.Gone;
                    break;

                case UserType.PublicAccess:
                    ViewStates vs = ViewStates.Visible;
                    if (TZM.IS_LOGGED_IN)
                    {
                        vs = ViewStates.Gone;
                    }
                    til_user_name.Visibility = vs;
                    til_new_password.Visibility = vs;
                    til_confirm_password.Visibility = vs;
                    break;

                default:
                    til_user_name.Visibility = ViewStates.Gone;
                    til_new_password.Visibility = ViewStates.Gone;
                    til_confirm_password.Visibility = ViewStates.Gone;
                    break;
            }

            if(TZM.IS_LOGGED_IN)
            {
                UserInformationVO? userInformationVO = HM.Container.Resolve<UserInformationVO>();
                etUIName.Text = userInformationVO.Name;
                etUICell.Text = userInformationVO.CellNo;
                etUIEmail.Text = userInformationVO.Email;
                
                if (userInformationVO.UserAddressVO != null)
                {
                    UserAddressVO? userAddress = userInformationVO.UserAddressVO;
                    etUIAddress.Text = userAddress.Address;
                    etUICity.Text = userAddress.City;
                    etUIAddress.Text = userAddress.Address;
                    if (string.IsNullOrEmpty(userAddress.State) == false && _states.Contains(userAddress.State))
                    {
                        actUIState.SetText(userAddress.State, false);
                    }

                    etUICountry.Text = userAddress.Country;
                }

                cbPrivacyPolicy.Checked = true;
            }
            AfterInitActivity();
        }

        private void TvPrivacyPolicyOnClick(object sender, EventArgs e)
        {
            StartActivity(new Intent(this, typeof(PrivacyPolicyActivity)));
        }

        private void ActUIStateOnFocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus)
            {
                Utils.HideKeyboardFrom(this);
            }
        }

        private void BtnUIProceed_Click(object sender, EventArgs e)
        {
            if (cbPrivacyPolicy.Checked == false)
            {
                HideProgressAndShowAlert(GetString(Resource.String.privacy_policy_checkbox_message));
                return;
            }

            ShowProgress();
            Task.Run(async () =>
            {
                UserInformationVO ui = new UserInformationVO
                {
                    Name = etUIName.Text,
                    CellNo = etUICell.Text,
                    Email = etUIEmail.Text,
                    UserName = etUIUserName.Text,
                    Password = etUINewPassword.Text,
                    UserAddressVO = new UserAddressVO()
                    {
                        Address = etUIAddress.Text,
                        City = etUICity.Text,
                        State = actUIState.Text,
                        Country = etUICountry.Text 
                    }
                };

                ui.Trim();
                try
                {
                    if (TZM.IS_LOGGED_IN && userType != UserType.AsGuest)
                    {
                        // Update basic profile info
                        ui.ValidateBasicUserInfo();
                        try
                        {
                            GetCurrentUserProfileCommandResult result = await _commandExecutor.ExecuteCommand<GetCurrentUserProfileCommandResult>(
                                new UpdateUserProfileCommandPayload()
                                {
                                    User = ui
                                });
                            _container.BindInstance(result.User);

                            AlertBox(this, "Information updated successfully", "Success",
                                neutralButton: "OK", o => MoveToMain());
                        }
                        catch (Exception e)
                        {
                            HLogger.Error(e, "Exception occured while executing http request");
                            AlertBox(e.Message, "Error", neutralButton: "OK");
                        }
                    }
                    else
                    {
                        // Signup
                        Utils.CompleteRemnantsForUserInformation(this, userType, ref ui);
                        // Validate
                        ui.Validate();

                        if (ui.Password != etUIConfirmPassword.Text.Trim())
                        {
                            throw new Exception("Password and Confirm Password didn't match");
                        }

                        // Make http request
                        SignupUserCommandPayload payload = new SignupUserCommandPayload()
                        {
                            User = ui,
                            ClientId = HConfigManager.Get<AuthConfig>().ClientId,
                            ClientSecret = HConfigManager.Get<AuthConfig>().ClientSecret,
                            DeviceId = HM.Container.Resolve<IUtilityService>().GetDeviceId(),
                            AppVersion = TZM.APP_VERSION.ToString(),
                            Platform = Platform.Android
                        };
                        LoginUserCommandResult signupResponse =
                            await _commandExecutor.ExecuteCommand<LoginUserCommandResult>(payload);
                        // Bind user information with container
                        _prefs.SetAccessToken(signupResponse.AccessToken);
                        _prefs.SetRefreshToken(signupResponse.RefreshToken);
                        _container.BindInstance(signupResponse.User);

                        if (signupResponse.User.RecordStatus == UserRecordStatus.PendingVerification)
                        {
                            // Go to account verification
                            SetResult(Result.FirstUser);
                            Intent? intent = new Intent(this, typeof(VerificationActivity));
                            intent.PutExtra(VerificationActivity.DISABLE_RESEND_KEY, true);
                            intent.PutExtra(VerificationActivity.VERIFICATION_TYPE_KEY,
                                TokenType.AccountVerificationToken.ToString());
                            StartActivity(intent);
                            Finish();
                        }
                        else if (signupResponse.User.RecordStatus == UserRecordStatus.Active)
                        {
                            HM.Container.Resolve<IUtilityService>().MoveToMain(this);
                        }
                    }
                }
                catch (Exception ex)
                {
                    HideProgressAndShowAlert(ex.Message);
                }

                HideProgress();
            });
        }

    }
}