﻿using Android.Text;
using HatfCommand;

namespace TZAndroid
{
    [Activity]
    public class VerificationActivity : BaseActivity
    {
        private EditText etCode1, etCode2, etCode3, etCode4, etCode5, etCode6;
        private EditText[] etCodes;
        private TextView tvResendCode, tvTryLater;
        private AppCompatButton btnVerify;
        private TokenType _tokenType;

        private int _resendButtonDisabledTime = 120; // Seconds
        private int ResendButtonDisabledTime;
        public static string DISABLE_RESEND_KEY = "DISABLE_RESEND_KEY";
        public static string VERIFICATION_TYPE_KEY = "VERIFICATION_TYPE_KEY";
        public static string VERIFICATION_EMAIL_KEY = "VERIFICATION_EMAIL_KEY";
        public static string SHOW_TRY_LATER_KEY = "SHOW_TRY_LATER_KEY";
        
        protected override bool RequireLogin => false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            Init();
        }

        private void Init()
        {
            SetContentView(Resource.Layout.verification_activity);

            etCode1 = FindViewById<EditText>(Resource.Id.etCode1);
            etCode2 = FindViewById<EditText>(Resource.Id.etCode2);
            etCode3 = FindViewById<EditText>(Resource.Id.etCode3);
            etCode4 = FindViewById<EditText>(Resource.Id.etCode4);
            etCode5 = FindViewById<EditText>(Resource.Id.etCode5);
            etCode6 = FindViewById<EditText>(Resource.Id.etCode6);
            etCodes = new[] { etCode1, etCode2, etCode3, etCode4, etCode5, etCode6 };
            tvResendCode = FindViewById<TextView>(Resource.Id.tvResendCode);
            tvTryLater = FindViewById<TextView>(Resource.Id.tvTryLater);
            btnVerify = FindViewById<AppCompatButton>(Resource.Id.btnVerify);
            
            // Focus next
            etCode1.TextChanged += EtCode1OnTextChanged;
            etCode2.TextChanged += EtCode2OnTextChanged;
            etCode3.TextChanged += EtCode3OnTextChanged;
            etCode4.TextChanged += EtCode4OnTextChanged;
            etCode5.TextChanged += EtCode5OnTextChanged;
            etCode6.TextChanged += EtCode6OnTextChanged;
   
            // Clear previous value on focus gain
            etCode1.FocusChange += ETCodeFocusChange;
            etCode2.FocusChange += ETCodeFocusChange;
            etCode3.FocusChange += ETCodeFocusChange;
            etCode4.FocusChange += ETCodeFocusChange;
            etCode5.FocusChange += ETCodeFocusChange;
            etCode6.FocusChange += ETCodeFocusChange;

            ResetCodeTextBoxes();
            // Disable verify button by default
            btnVerify.Enabled = false;
            
            // Set button listeners
            string verificationType = Intent.GetStringExtra(VERIFICATION_TYPE_KEY);
            if (string.IsNullOrEmpty(verificationType))
            {
                throw new Exception("Token type is required for verification activity");
            }

            _tokenType = EnumUtils.ParseEnum<TokenType>(verificationType);
            if (_tokenType != TokenType.AccountVerificationToken && _tokenType != TokenType.PasswordRecoveryToken)
            {
                throw new Exception($"Token type {_tokenType} not allowed for verification");
            }

            tvResendCode.Click += ResendCode_Click;
            btnVerify.Click += Verify_Click;
            tvTryLater.Click += TryLater_Click;

            if (ShowTryLater == false)
            {
                tvTryLater.Visibility = ViewStates.Gone;
            }
            
            AfterInitActivity();

            if (Intent.GetBooleanExtra(DISABLE_RESEND_KEY, false))
            {
                ResendButtonDisabledTime = _resendButtonDisabledTime;
                DisableResendButton();
            }
        }

        private void TryLater_Click(object sender, EventArgs e)
        {
            if (_tokenType == TokenType.PasswordRecoveryToken)
            {
                HM.Container.Resolve<IUtilityService>().MoveToLogin(this);
            }
            else
            {
                HM.Container.Resolve<IUtilityService>().MoveToMain(this);
            }
        }

        private void Verify_Click(object sender, EventArgs e)
        {
            if (GetCode(out string code))
            {
                ShowProgress();
                
                Task.Run(async () =>
                {
                    bool success = false;
                    try
                    {
                        if (_tokenType == TokenType.AccountVerificationToken)
                        {
                            try
                            {
                                await _commandExecutor.ExecuteVoidCommand(new VerifyEmailCommandPayload()
                                {
                                    Token = code
                                });
                                // Request executed successfully
                                AlertBox("Your account is successfully verified", "Success",
                                    neutralButton: "OK",
                                    neutralButtonClicked: _ =>
                                    {
                                        UserInformationVO? userInformationVo = _container.Resolve<UserInformationVO>();
                                        userInformationVo.RecordStatus = UserRecordStatus.Active;
                                        MoveToMain();
                                    });
                                success = true;
                            }
                            catch (HException e)
                            {
                                AlertBox(e.Message, "Error", neutralButton: "OK");
                            }
                        }
                        else if (_tokenType == TokenType.PasswordRecoveryToken)
                        {
                            try
                            {
                                await _commandExecutor.ExecuteVoidCommand(new VerifyPasswordRecoveryTokenCommandPayload()
                                {
                                    BranchId = HConfigManager.Get<TZMConfig>().BranchId,
                                    Email = RecoveryEmail,
                                    Token = code
                                });
                                success = true;
                                // Go to reset password activity
                                RunInvokeRequired(_ =>
                                {
                                    Intent intent = new Intent(this, typeof(ResetPasswordActivity));
                                    intent.PutExtra(ResetPasswordActivity.RESET_PASSWORD_EMAIL_KEY, RecoveryEmail);
                                    intent.PutExtra(ResetPasswordActivity.RESET_PASSWORD_TOKEN_KEY, code);
                                    StartActivity(intent);
                                });
                            }
                            catch (Exception e)
                            {
                                HLogger.Error(e, "Exception occured while executing http request");
                                AlertBox(e.Message, "Error", neutralButton: "OK");
                            }
                        }
                        else
                        {
                            throw new Exception($"Token type {_tokenType} not allowed for verification");
                        }

                    }
                    catch (Exception e)
                    {
                        HLogger.Error(e, "Exception occured while executing http request");
                        AlertBox(e.Message, "Error", neutralButton: "OK");
                    }

                    RunInvokeRequired(_ =>
                    {
                        if (success == false)
                        {
                            ResetCodeTextBoxes();
                        }
                        HideProgress();
                    });
                });
            }
        }

        private bool GetCode(out string code)
        {
            code = string.Join("", etCodes.Select(m => m.Text));
            if (code.Length == etCodes.Length)
            {
                return true;
            }
            HideProgressAndShowAlert($"Please enter all {etCodes.Length} digits");
            return false;
        }

        private void ResendCode_Click(object sender, EventArgs e)
        {
            RunInvokeRequired(_ =>
            {
                ShowProgress();

                CommandRequest commandRequest = _tokenType switch
                {
                    TokenType.AccountVerificationToken =>
                        CommandRequest.Create(nameof(SendAccountVerificationEmailCommand), VoidCommandPayload.Instance),
                    TokenType.PasswordRecoveryToken =>
                        CommandRequest.Create(new SendPasswordRecoveryEmailCommandPayload()
                        {
                            ClientId = HConfigManager.Get<AuthConfig>().ClientId,
                            DeviceId = HM.Container.Resolve<IUtilityService>().GetDeviceId(),
                            BranchId = HConfigManager.Get<TZMConfig>().BranchId,
                            Email = RecoveryEmail
                        }),
                    _ => throw new Exception($"Token type {_tokenType} not allowed for verification")
                };
                // Resend button clicked
                Task.Run(async () =>
                {
                    try
                    {
                        await _commandExecutor.ExecuteVoidCommand(commandRequest);
                        // Request executed successfully
                        HM.Container.Resolve<IUtilityService>().AlertBox("Verification code sent successfully",
                            "Success",
                            neutralButton: "OK", neutralButtonClicked: _ =>
                            {
                                ResetCodeTextBoxes();
                                HideProgress();
                            });

                        ResendButtonDisabledTime = _resendButtonDisabledTime;
                        DisableResendButton();

                    }
                    catch (Exception e)
                    {
                        HLogger.Error(e, "Exception occured while executing http request");
                        _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
                    }
                });
            });
        }

        private void ResetCodeTextBoxes()
        {
            // Clear the text boxes
            etCode1.Text = "";
            etCode2.Text = "";
            etCode3.Text = "";
            etCode4.Text = "";
            etCode5.Text = "";
            etCode6.Text = "";
            // Set fouc on first edit text
            etCode1.RequestFocus();
        }
        
        private void ETCodeFocusChange(object sender, View.FocusChangeEventArgs e)
        {
            EditText et = (EditText) sender;
            if (et.IsFocused)
            {
                et.Text = string.Empty;
            }
        }

        private void EtCode1OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (etCode1.Text.Length > 0)
            {
                etCode2.RequestFocus();   
            }
        }
        
        private void EtCode2OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (etCode2.Text.Length > 0)
            {
                etCode3.RequestFocus();
            }
        }
        
        private void EtCode3OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (etCode3.Text.Length > 0)
            {
                etCode4.RequestFocus();
            }
        }
        
        private void EtCode4OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (etCode4.Text.Length > 0)
            {
                etCode5.RequestFocus();
            }
        }

        private void EtCode5OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (etCode5.Text.Length > 0)
            {
                etCode6.RequestFocus();
            }
        }

        private void EtCode6OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (etCode6.Text.Length > 0)
            {
                Utils.HideKeyboardFrom(this);
                btnVerify.Enabled = true;
            }
        }

        private void DisableResendButton()
        {
            Task.Run(async () =>
            {
                RunInvokeRequired(_=> tvResendCode.Clickable = false);
                while (ResendButtonDisabledTime > 0)
                {
                    RunInvokeRequired(_=>
                    {
                        tvResendCode.Text = GenerateTimeString(ResendButtonDisabledTime);
                    });
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    ResendButtonDisabledTime--;
                }
                
                RunInvokeRequired(_=>
                {
                    tvResendCode.Clickable = true;
                    tvResendCode.Text = GetString(Resource.String.resend);
                });
            });
        }

        private string GenerateTimeString(int timeInSeconds)
        {
            TimeSpan ts = TimeSpan.FromSeconds(timeInSeconds);
            return ts.ToString("mm") + ":" + ts.ToString("ss");
        }

        private string RecoveryEmail
        {
            get
            {
                string email = Intent.GetStringExtra(VERIFICATION_EMAIL_KEY);
                if (string.IsNullOrEmpty(email))
                {
                    throw new Exception("Email not provided by the previous activity");
                }

                return email;
            }
        }

        private bool ShowTryLater => Intent.GetBooleanExtra(SHOW_TRY_LATER_KEY, true);

    }
}