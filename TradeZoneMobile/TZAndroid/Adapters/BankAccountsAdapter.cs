﻿namespace TZAndroid
{
    internal class BankAccountsAdapter : RecyclerView.Adapter
    {
        private Activity context;
        private List<BankAccountModel> list;

        public BankAccountsAdapter(Activity context, List<BankAccountModel> list)
        {
            this.context = context;
            this.list = list;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(context).Inflate(Resource.Layout.bank_account_row_item, parent, false);
            bankAccountsAdapterViewHolder vh = new bankAccountsAdapterViewHolder(context, itemView, list);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            bankAccountsAdapterViewHolder vHolder = (bankAccountsAdapterViewHolder)holder;
            vHolder.tvbariAccNum.Text = list[position].AccountNo;
            vHolder.tvbariAccTitle.Text = list[position].Title;
            vHolder.tvbariBankName.Text = list[position].Bank;
        }

        public override int ItemCount
        {
            get { return list.Count; }
        }

    }

    public class bankAccountsAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView tvbariBankName, tvbariAccTitle, tvbariAccNum;
        private Context context;
        private List<BankAccountModel> list;

        public bankAccountsAdapterViewHolder(Context _context, View itemView, List<BankAccountModel> _list) : base(itemView)
        {
            context = _context;
            list = _list;
            tvbariBankName = (TextView)itemView.FindViewById(Resource.Id.tvbariBankName);
            tvbariAccTitle = (TextView)itemView.FindViewById(Resource.Id.tvbariAccTitle);
            tvbariAccNum = (TextView)itemView.FindViewById(Resource.Id.tvbariAccNum);
        }

    }
}