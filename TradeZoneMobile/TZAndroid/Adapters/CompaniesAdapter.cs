﻿namespace TZAndroid;

internal class CompaniesAdapter : RecyclerView.Adapter
{
    private Activity context;
    private List<CompanyModel> list;

    public CompaniesAdapter(Activity context, List<CompanyModel> list)
    {
        this.context = context;
        this.list = list;
    }

    public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.From(context).Inflate(Resource.Layout.company_row_item, parent, false);
        companiesAdapterViewHolder vh = new companiesAdapterViewHolder(context, itemView, list);
        return vh;
    }

    public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        companiesAdapterViewHolder vHolder = (companiesAdapterViewHolder)holder;
        vHolder.tvCompanyCell.Text = string.Join("\n", list[position].CompanyCell);
        vHolder.tvCompanyPhone.Text = string.Join("\n", list[position].CompanyPhone);
        vHolder.tvCompanyTitle.Text = list[position].CompanyName;
        vHolder.tvOwnerName.Text = list[position].OwnerName;
        vHolder.imgCompanyLogo.SetImageResource(
            BaseActivity.DynamicResourceCache[ResourceType.Drawable][HConfigManager.Get<TZMConfig>().BannerImage]);
        
        if (string.IsNullOrEmpty(vHolder.tvCompanyPhone.Text))
        {
            vHolder.imgCompanyPhoneLogo.Visibility = vHolder.tvCompanyPhone.Visibility = ViewStates.Gone;
        }
        else
        {
            vHolder.imgCompanyPhoneLogo.Visibility = vHolder.tvCompanyPhone.Visibility = ViewStates.Visible;
        }
    }

    public override int ItemCount
    {
        get { return list.Count; }
    }

}

public class companiesAdapterViewHolder : RecyclerView.ViewHolder
{
    public TextView tvCompanyTitle, tvCompanyPhone, tvCompanyCell, tvOwnerName;
    private Button btnDetails;
    public ImageView imgCompanyLogo, imgCompanyPhoneLogo;
    private Context context;
    private List<CompanyModel> list;

    public companiesAdapterViewHolder(Context _context, View itemView, List<CompanyModel> _list) : base(itemView)
    {
        context = _context;
        list = _list;
        tvOwnerName = (TextView)itemView.FindViewById(Resource.Id.tvOwnerName);
        tvCompanyTitle = (TextView)itemView.FindViewById(Resource.Id.tvCompanyTitle);
        tvCompanyPhone = (TextView)itemView.FindViewById(Resource.Id.tvCompanyPhone);
        tvCompanyCell = (TextView)itemView.FindViewById(Resource.Id.tvCompanyCell);
        btnDetails = (Button)itemView.FindViewById(Resource.Id.btnDetails);
        imgCompanyLogo = (ImageView)itemView.FindViewById(Resource.Id.imgCompanyLogo);
        imgCompanyPhoneLogo = (ImageView)itemView.FindViewById(Resource.Id.imgCompanyPhoneLogo);
        
        btnDetails.Click += ItemView_Click;
        itemView.Click += ItemView_Click;
    }

    private void ItemView_Click(object sender, EventArgs e)
    {
        Intent intent = new Intent(context, typeof(CompanyDetailsActivity));
        intent.PutExtra("comobj", JsonUtils.Serialize(list[AdapterPosition]));
        try {
            context.StartActivity(intent);
        }
        catch (Exception ex)
        {
            NotificationUtils.DeveloperToast(context, ex.ToString());
        }
    }
    
}