﻿namespace TZAndroid
{
    internal class InfoAdapter : RecyclerView.Adapter
    {
        private Activity context;
        private List<NameValueModel> list;

        public InfoAdapter(Activity context, List<NameValueModel> list)
        {
            this.context = context;
            this.list = list;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(context).Inflate(Resource.Layout.info_row_item, parent, false);
            infoAdapterViewHolder vh = new infoAdapterViewHolder(context, itemView, list);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            infoAdapterViewHolder vHolder = (infoAdapterViewHolder)holder;
            vHolder.tvIRIValue.Text = list[position].Value;
            vHolder.tvIRIHeading.Text = list[position].Name;
        }

        public override int ItemCount
        {
            get { return list.Count; }
        }

    }

    public class infoAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView tvIRIValue, tvIRIHeading;
        private Context context;
        private List<NameValueModel> list;

        public infoAdapterViewHolder(Context _context, View itemView, List<NameValueModel> _list) : base(itemView)
        {
            context = _context;
            list = _list;
            tvIRIValue = (TextView)itemView.FindViewById(Resource.Id.tvIRIValue);
            tvIRIHeading = (TextView)itemView.FindViewById(Resource.Id.tvIRIHeading);
        }
        
    }
}