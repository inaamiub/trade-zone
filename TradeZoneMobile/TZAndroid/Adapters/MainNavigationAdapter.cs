﻿namespace TZAndroid
{
    internal class MainNavigationAdapter : RecyclerView.Adapter
    {
        private Activity context;
        private List<mdCustomMenuItem> list;

        public MainNavigationAdapter(Activity context, List<mdCustomMenuItem> list)
        {
            this.context = context;
            this.list = list;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(context).Inflate(Resource.Layout.main_nav_row_item, parent, false);
            mainNavigationAdapterViewHolder vh = new mainNavigationAdapterViewHolder(context, itemView, list);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            mainNavigationAdapterViewHolder vHolder = (mainNavigationAdapterViewHolder)holder;
            mdCustomMenuItem cmi = list[position];
            if(cmi.DrawableResource == null)
            {
                vHolder.mainNavRowItemIcon.Visibility = ViewStates.Gone;
            }
            else
            {
                vHolder.mainNavRowItemIcon.SetImageResource(cmi.DrawableResource ?? Resource.Drawable.ic_check_box_outline_blank_black_24dp);
                vHolder.mainNavRowItemIcon.Visibility = ViewStates.Visible;
            }

            vHolder.mainNavRowItemTitle.Text = cmi.Text;

        }

        public override int ItemCount
        {
            get { return list.Count; }
        }

    }

    public class mainNavigationAdapterViewHolder : RecyclerView.ViewHolder
    {
        public AppCompatTextView mainNavRowItemTitle;
        public AppCompatImageView mainNavRowItemIcon;
        private Context context;
        private List<mdCustomMenuItem> list;

        public mainNavigationAdapterViewHolder(Context _context, View itemView, List<mdCustomMenuItem> _list) : base(itemView)
        {
            context = _context;
            list = _list;
            mainNavRowItemTitle = (AppCompatTextView)itemView.FindViewById(Resource.Id.mainNavRowItemTitle);
            mainNavRowItemIcon = (AppCompatImageView)itemView.FindViewById(Resource.Id.mainNavRowItemIcon);
            //Utils.ButtonEffect(itemView);
            
            itemView.Click += ItemView_Click;
        }

        private void ItemView_Click(object sender, EventArgs e)
        {
            try
            {
                mdCustomMenuItem cmi = list[AdapterPosition];
                if(cmi != null)
                {
                    if(cmi.ClickedEvent != null)
                    {
                        if(cmi.Params == null)
                        {
                            cmi.Params = cmi;
                        }
                        cmi.ClickedEvent(cmi.Params);
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationUtils.DeveloperToast(context, ex.ToString());
            }
        }
        
    }
}