﻿using AndroidX.Core.View;
using Google.Android.Material.Shape;

namespace TZAndroid
{
    internal class OrderHistoryAdapter : RecyclerView.Adapter
    {
        private Activity context;
        private List<SalesOrderVO> _orders;

        public OrderHistoryAdapter(Activity context, List<SalesOrderVO> orders)
        {
            this.context = context;
            _orders = orders;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(context).Inflate(Resource.Layout.order_history_row_item, parent, false);
            OrderHistoryAdapterViewHolder? vh = new OrderHistoryAdapterViewHolder(context, itemView);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            OrderHistoryAdapterViewHolder? vHolder = (OrderHistoryAdapterViewHolder)holder;
            vHolder.BindItem(_orders[position], position);
        }

        public override int ItemCount => _orders.Count;
    }

    public class OrderHistoryAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView _tvOrderId,
            _tvCreatedOn,
            _tvDeliveryType,
            _tvTotalItems,
            _tvOrderStatus,
            _tvPaymentMethod,
            _tvTotalPrice;

        private Context _context;
        private View _itemView;
        private int _position;
        private SalesOrderVO _salesOrderVo;

        public OrderHistoryAdapterViewHolder(Context context, View itemView) : base(itemView)
        {
            _context = context;
            _itemView = itemView;
            try
            {
                _tvOrderId = (TextView)itemView.FindViewById(Resource.Id.tvOrderId);
                _tvCreatedOn = (TextView)itemView.FindViewById(Resource.Id.tvCreatedOn);
                _tvDeliveryType = (TextView)itemView.FindViewById(Resource.Id.tvDeliveryType);
                _tvTotalItems = (TextView)itemView.FindViewById(Resource.Id.tvTotalItems);
                _tvOrderStatus = (TextView)itemView.FindViewById(Resource.Id.tvOrderStatus);
                _tvPaymentMethod = (TextView)itemView.FindViewById(Resource.Id.tvPaymentMethod);
                _tvTotalPrice = (TextView)itemView.FindViewById(Resource.Id.tvTotalPrice);
                
            }
            catch (Exception ex)
            {
                NotificationUtils.UserToast(context, ex.ToString());
            }
        }

        public void BindItem(SalesOrderVO salesOrderVo, int position)
        {
            _position = position;
            _salesOrderVo = salesOrderVo;

            _tvOrderId.Text = _salesOrderVo.GetOrderId();
            _tvCreatedOn.Text = _salesOrderVo.OrderDate.ToString("dd MMM yy");
            _tvDeliveryType.Text = _salesOrderVo.DeliveryType.ToString();
            _tvTotalItems.Text = _salesOrderVo.NumberOfItems.ToString() + " item(s)";
            _tvPaymentMethod.Text = _salesOrderVo.PaymentMethod.ToString();
            _tvTotalPrice.Text = "PKR " + _salesOrderVo.TotalPrice.ToString();

            string orderStatus = "In-Progress";
            int statusColor = Resource.Color.status_info;
            
            if (_salesOrderVo.IsCancel)
            {
                orderStatus = "Cancelled";
                statusColor = Resource.Color.status_danger;
            }
            else if (_salesOrderVo.IsRejected)
            {
                orderStatus = "Rejected";
                statusColor = Resource.Color.status_danger;
            }
            else if (_salesOrderVo.IsCompleted)
            {
                orderStatus = "Completed";
                statusColor = Resource.Color.status_success;
            }

            _tvOrderStatus.Text = orderStatus;
            UpdateBackground(_tvOrderStatus, statusColor);
            
            _itemView.Click += ItemViewOnClick;
        }

        private void UpdateBackground(TextView tv, int color)
        {
            float radius = _context.Resources.GetDimension(Resource.Dimension.corner_radius);

            ShapeAppearanceModel shapeAppearanceModel = new ShapeAppearanceModel().ToBuilder()
                .SetAllCorners(CornerFamily.Rounded, radius)
                .Build();
            
            MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable(shapeAppearanceModel);
            shapeDrawable.FillColor = ContextCompat.GetColorStateList(_context, color);
            ViewCompat.SetBackground(tv, shapeDrawable);
        }

        private void ItemViewOnClick(object sender, EventArgs e)
        {
            // TODO: Show order details here
        }
    }
}