﻿using AndroidX.ViewPager.Widget;

namespace TZAndroid;

internal class ProductImageSliderAdapter : PagerAdapter
{
    private Activity _context;
    private int[] _images;
    private ImageView[] _dots;
    private LinearLayout _llDotsContainer;
    
    public ProductImageSliderAdapter(Activity context)
    {
        this._context = context;
#if DEBUG
        _images = new[] {Resource.Drawable.anxiety_256p, Resource.Drawable.message_256p, Resource.Drawable.security_code_256p};
#else
        _images = new int [0];
#endif
        _llDotsContainer = _context.FindViewById<LinearLayout>(Resource.Id.llDotsContainer);
        InitDots();
    }

    public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
    {
        LayoutInflater? layoutInflater = (LayoutInflater) _context.GetSystemService(Context.LayoutInflaterService);
        
        // inflating the item.xml  
        View itemView = layoutInflater.Inflate(Resource.Layout.image_slider_item, container, false); 

        // referencing the image view from the item.xml file  
        ImageView imageView = itemView.FindViewById<ImageView>(Resource.Id.ivSliderImage); 
      
        // setting the image in the imageView 
        imageView.SetImageResource(_images[position]); 

        // Adding the View  
        container.AddView(itemView, 0); 

        return itemView;
    }

    public void OnPageSelected(int position)
    {
        if (Count < 1)
        {
            return;
        }
        
        for (int i = 0; i < _dots.Length; i++)
        {
            if (i != position)
            {
                _dots[i].SetImageResource(Resource.Drawable.nonactive_dot);
            }
        }

        _dots[position].SetImageResource(Resource.Drawable.active_dot);

    }

    private void InitDots()
    {
        _dots = new ImageView[_images.Length];
        
        // Starting from 1 because 0 will be default
        for(int i = 0; i < _dots.Length; i++){

            _dots[i] = new ImageView(_context);
            LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
            lParams.SetMargins(8, 0, 8, 0);

            _llDotsContainer.AddView(_dots[i], lParams);
        }

        for (int i = 1; i < _dots.Length; i++)
        {
            OnPageSelected(i);
        }

        if (_dots.Length > 0)
        {
            // Set default image to active
            OnPageSelected(0);
        }

    }
    
    public override bool IsViewFromObject(View view, Java.Lang.Object @object)
    {
        return view == ((ConstraintLayout) @object);
    }

    public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object @object)
    {
        container.RemoveView((View)@object);
    }

    public override int Count => _images.Length;
}