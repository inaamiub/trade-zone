﻿namespace TZAndroid
{
    internal class ShoppingCartAdapter : RecyclerView.Adapter
    {
        private readonly Activity _context;
        private readonly List<ShoppingCartItemModel> _cartDetails;

        public ShoppingCartAdapter(Activity context, List<ShoppingCartItemModel> cartDetails)
        {
            _context = context;
            _cartDetails = cartDetails;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View? itemView = LayoutInflater.From(_context)?.Inflate(Resource.Layout.shopping_cart_row_item, parent, false);
            return new ShoppingCartAdapterViewHolder(_context, itemView, _cartDetails);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ShoppingCartAdapterViewHolder? viewHolder = (ShoppingCartAdapterViewHolder)holder;
            viewHolder.BindItem(position);
        }

        public override int ItemCount => _cartDetails.Count;
    }

    public class ShoppingCartAdapterViewHolder : RecyclerView.ViewHolder
    {
        private readonly TextView _tvName, _tvCompany, _tvPrice, _tvQuantity;
        private readonly ImageView _ivImage, _imgStockStatus;
        private readonly Context _context;
        private readonly List<ShoppingCartItemModel> _list;
        private readonly View _itemView;
        private int _position;
        private ShoppingCartItemModel _shoppingCartItemModel;

        public ShoppingCartAdapterViewHolder(Context context, View itemView, List<ShoppingCartItemModel> list) : base(itemView)
        {
            _context = context;
            _list = list;
            _itemView = itemView;
            
            _tvName = itemView.FindViewById<TextView>(Resource.Id.tvName);
            _tvCompany = itemView.FindViewById<TextView>(Resource.Id.tvCompany);
            _tvPrice = itemView.FindViewById<TextView>(Resource.Id.tvPrice);
            _tvQuantity = itemView.FindViewById<TextView>(Resource.Id.tvQuantity);
                
            _ivImage = itemView.FindViewById<ImageView>(Resource.Id.ivImage);
            _imgStockStatus = itemView.FindViewById<ImageView>(Resource.Id.imgStockStatus);
        }

        public void BindItem(int position)
        {
            _position = position;
            try
            {
                if (_list.Count < position)
                {
                    throw new Exception($"Position {position} out of list length {_list.Count}");
                }

                _shoppingCartItemModel = _list[_position];

                _tvName.Text = _shoppingCartItemModel.ProductDetails.ProductName;
                _tvCompany.Text = _shoppingCartItemModel.ProductDetails.CompanyName;
                _tvPrice.Text = "Rs " + _shoppingCartItemModel.ProductDetails.RetailPrice;
                _tvQuantity.Text = _shoppingCartItemModel.Quantity.ToString();
                
                if (_shoppingCartItemModel.ProductDetails.StockStatus)
                {
                    _imgStockStatus.SetImageResource(Resource.Drawable.ic_album_green_24dp);
                }
                else
                {
                    _imgStockStatus.SetImageResource(Resource.Drawable.ic_album_red_24dp);
                }

                _itemView.Click += ItemViewOnClick;

            }
            catch (Exception e)
            {
                NotificationUtils.DeveloperToast(_context, e.Message);
                NotificationUtils.UserToast(_context, "Something went wrong in loading cart");
            }
        }
        
        private void ItemViewOnClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(_context, typeof(ProductDetailsActivity));
            intent.PutExtra(ProductDetailsActivity.PRODUCT_ID_KEY, _list[_position].ItemId);
            intent.AddFlags(ActivityFlags.ReorderToFront);
            _context.StartActivity(intent);
        }
    }
}