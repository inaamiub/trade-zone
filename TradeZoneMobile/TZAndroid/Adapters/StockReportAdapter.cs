﻿namespace TZAndroid
{
    internal class StockReportAdapter : RecyclerView.Adapter
    {
        private Activity context;
        private List<StockReportModel> list;

        public StockReportAdapter(Activity context, List<StockReportModel> list)
        {
            this.context = context;
            this.list = list;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(context).Inflate(Resource.Layout.stock_report_row_item, parent, false);
            StockReportAdapterViewHolder vh = new StockReportAdapterViewHolder(context, itemView, list);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            StockReportAdapterViewHolder vHolder = (StockReportAdapterViewHolder)holder;
            vHolder.BindItem(position);
        }

        public override int ItemCount => list.Count;
    }

    public class StockReportAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView _tvProductName, _tvCompanyName, _tvPrice;
        public ImageView _imgvStockStatus;
        private Context _context;
        private List<StockReportModel> _list;
        private View _itemView;
        private int _position;

        public StockReportAdapterViewHolder(Context context, View itemView, List<StockReportModel> list) : base(itemView)
        {
            _context = context;
            _list = list;
            _itemView = itemView;
            try
            {
                _tvProductName = (TextView)itemView.FindViewById(Resource.Id.tvProductName);
                _tvCompanyName = (TextView)itemView.FindViewById(Resource.Id.tvCompanyName);
                _tvPrice = (TextView)itemView.FindViewById(Resource.Id.tvPrice);
                _imgvStockStatus = (ImageView)itemView.FindViewById(Resource.Id.imgvStockStatus);
                
            }
            catch (Exception ex)
            {
                NotificationUtils.UserToast(context, ex.ToString());
            }
        }

        public void BindItem(int position)
        {
            _position = position;
            StockReportModel srm = _list[_position];
            _tvCompanyName.Text = srm.CompanyName;
            _tvPrice.Text = "Rs " + srm.RetailPrice;
            _tvProductName.Text = srm.ProductName;
            if (srm.StockStatus)
            {
                _imgvStockStatus.SetImageResource(Resource.Drawable.ic_album_green_24dp);
            }
            else
            {
                _imgvStockStatus.SetImageResource(Resource.Drawable.ic_album_red_24dp);
            }
            _itemView.Click += ItemViewOnClick;
        }
        
        private void ItemViewOnClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(_context, typeof(ProductDetailsActivity));
            intent.PutExtra(ProductDetailsActivity.PRODUCT_ID_KEY, _list[_position].ProductID);
            intent.AddFlags(ActivityFlags.ReorderToFront);
            _context.StartActivity(intent);
        }
    }
}