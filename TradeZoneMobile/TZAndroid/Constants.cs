﻿namespace TZAndroid
{
    public class Constants
    {
        //Note: static members will be added in a static class
        #region Endpoints
        public static class Endpoints
        {
            public const string PLAYSTORE_MARKET_APP_DETAILS = "market://details?id={0}";
            public const string PLAYSTORE_WEB_APP_DETAILS = "https://play.google.com/store/apps/details?id={0}";

        }
        #endregion

        #region Pref Keys & Default Values
        public static class Keys
        {
            public const string USERACCESSTYPE_KEY = "UserAccessType";
            public const string ISEDIT = "IsEdit";
            public const string EXIT = "EXIT";
        }
        #endregion

        #region Constants & Statics
        public const string DATE_TIME_FORMAT_ddd_dd_MMM_yyyy_hh_mm_tt = "ddd dd MMM yyyy hh:mm tt";
        public const string PREF_NAME = "TradeZone_Preferences";
#endregion

#region Messages
        public static class Messages
        {
            public static string USERNAME_REQUIRED = "User Name is required";
            public static string PASSWORD_REQUIRED = "Password is required";
            public static string NEWPASSWORD_REQUIRED = "New Password is required";
            public static string CONFIRMPASSWORD_REQUIRED = "Confirm Password is required";
            public static string NEWANDOLD_PASSWORD_MUST_BE_DIFFERENT = "New Password and Old Password must be different";
            public static string NEWANDOLD_USERNAME_MUST_BE_DIFFERENT = "New User Name and User Name must be different";
            public static string NEWUSERNAME_REQUIRED = "New User Name is required";
            public static string PASSWORD_CONFIRMPASSWORD_MISSMATCH = "Password didn't match with Confirm Password";
            public static string SOMETHING_WENT_WRONG = "Something went wrong. Transaction not completed successfully";
            public static string ALLOW_PERMISSIONS = "Please allow the required permissions for application. Otherwise the application may not be able to continue. If you've selected Don't show again for any permission then you'll need to allow permission manually from application properties";
            public static string UPDATE_USERINFO_CONFIRMATION = "Updating this information will require you to login again." + System.Environment.NewLine + "Are you sure to proceed?";
            public static string APP_UPDATE_AVAILABE = "A new version of this application is available";
            public static string APP_UPDATE_AVAILABE_FORECE = "A new version of this application is available. This update is neccessary in order run this application";

        }
#endregion

        public static class ActivityResultCodes
        {
            
        }

    }
}
