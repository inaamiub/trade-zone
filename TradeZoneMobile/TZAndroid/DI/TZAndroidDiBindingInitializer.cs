﻿namespace TZAndroid
{
    public class TZAndroidDiBindingInitializer : IDiBindingInitializer
    {
        public void InitializeRootContainer(DiContainer container)
        {
            
        }

        public void InitializeRequestContainer(DiContainer container)
        {
            
        }

        public void InitializeUserContainer(DiContainer container)
        {
            container.Bind<IUtilityService>().To<AndroidUtilityService>().AsSingle();
        }
    }
}