﻿using HatfCore;

namespace TZAndroid;

public class AndroidLogAppender(string packageName) : ILogAppender
{
    public void Log(Logger logger, LogLevel logLevel, string message, HLogTag logTags)
    {
        switch (logLevel)
        {
            case LogLevel.Trace:
            case LogLevel.Assert:
                Android.Util.Log.Verbose(packageName, message);
                break;

            case LogLevel.Debug:
                Android.Util.Log.Debug(packageName, message);
                break;
            
            case LogLevel.Info:
                Android.Util.Log.Info(packageName, message);
                break;

            case LogLevel.Warn:
                Android.Util.Log.Warn(packageName, message);
                break;
            
            case LogLevel.Error:
            case LogLevel.Fatal:
                Android.Util.Log.Error(packageName, message);
                break;

        }
    }
}