﻿using HatfCommand;

namespace TZAndroid;

public class Program
{
    public static Task InitializeApplication()
    {
        // Register executing assembly
        Hatf.RegisterAssembly("TZAndroid" );
        
        // Register di binding initializer
        HContainerFactory.RegisterDiBindingInitializer(new TZAndroidDiBindingInitializer());

        CommandExecutor commandExecutor;
        return TZMobile.TZProgram.Initialize(Platform.Android, GetAppSettingsFileName());
    }

    public static string GetAppSettingsFileName()
    {
        string appSettingsFileName = "appsettings";
#if DEBUG
        appSettingsFileName += ".Debug";
#if CSM
            appSettingsFileName += ".CSM";
#elif MT
        appSettingsFileName += ".MKT";
#endif
#else
            appSettingsFileName += ".Release";
#if CSM
            appSettingsFileName += ".CSM";
#elif MT
            appSettingsFileName += ".MKT";
#endif
#endif
        appSettingsFileName += ".json";

        return appSettingsFileName;
    }
}