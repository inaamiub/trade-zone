﻿using Android.Content.PM;
using Android.Util;

namespace TZAndroid
{
    public class AndroidUtilityService : IUtilityService
    {
        private DiContainer _container;

        public AndroidUtilityService(DiContainer container)
        {
            _container = container;
        }
        
        public object GetSharedPreference(string key, object defaultValue)
        {
            ISharedPreferences ispref = Application.Context.GetSharedPreferences(Constants.PREF_NAME, FileCreationMode.Private);
            KeyValuePair<string, object> pref = ispref.All.FirstOrDefault(m => m.Key == key);
            if(string.IsNullOrWhiteSpace(pref.Key))
            {
                PutSharedPreference(key, defaultValue);
                return defaultValue;
            }
            return pref.Value;
        }
        
        public void PutSharedPreference(string key, object value)
        {
            ISharedPreferences prefs = Application.Context.GetSharedPreferences(Constants.PREF_NAME, FileCreationMode.Private);
            ISharedPreferencesEditor editor = prefs.Edit();
            Type type = value.GetType();
            if (type == typeof(bool))
            {
                editor.PutBoolean(key, (bool)value);
            }
            else
            if (type == typeof(float) || type == typeof(double) || type == typeof(decimal))
            {
                editor.PutFloat(key, (float)value);
            }
            else
            if (type == typeof(int))
            {
                editor.PutInt(key, (int)value);
            }
            else
            if (type == typeof(long))
            {
                editor.PutLong(key, (long)value);
            }
            else
            if (type == typeof(ICollection<string>))
            {
                editor.PutStringSet(key, (ICollection<string>)value);
            }
            else
            {
                editor.PutString(key, value.ToString());
            }
            editor.Apply();
        }

        public string GetString(string key, string defaultValue = null)
        {
            return GetSharedPreference(key, defaultValue ?? string.Empty).ToString();
        }

        public int GetInteger(string key, int defaultValue = -1)
        {
            object? value = GetSharedPreference(key, defaultValue);
            return (int)value;
        }

        public int GetAppVersionCode()
        {
            int version = 0;
            try
            {
                Activity? context = GetContext();
                PackageInfo pInfo = context.PackageManager.GetPackageInfo(context.PackageName, 0);
                version = pInfo.VersionCode;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while getting app version");
            }
            return version;
        }

        public string GetAppVersionName()
        {
            string versionName = string.Empty;
            try
            {
                Activity? context = GetContext();
                PackageInfo pInfo = context.PackageManager.GetPackageInfo(context.PackageName, 0);
                versionName = pInfo.VersionName;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while getting app version");
            }
            return versionName;
        }

        public string GetAppPackageName()
        {
            string packageName = string.Empty;
            try
            {
                Activity? context = GetContext();
                PackageInfo pInfo = context.PackageManager.GetPackageInfo(context.PackageName, 0);
                packageName = pInfo.PackageName;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while getting app version");
            }
            return packageName;
        }

        public void AlertBox(string message, string title = null, string neutralButton = null, Action<object> neutralButtonClicked = null,
            string positiveButton = null, Action<object> positiveButtonClicked = null, string negativeButton = null,
            Action<object> negativeButtonClicked = null)
        {
            RunInvokeRequired((object param) =>
            {
                Utils.AlertBox(GetContext(), message, title, neutralButton, neutralButtonClicked,
                    positiveButton, positiveButtonClicked,
                    negativeButton, negativeButtonClicked);
            });
        }

        public void RunInvokeRequired(Action<object> function, object parameters = null)
        {
            Activity? context = GetContext();
            if (Looper.MyLooper() != Looper.MainLooper)
            {
                context.RunOnUiThread(() => { function(parameters); });
            }
            else
            {
                function(parameters);
            }
        }

        public void ExitApplication()
        {
            Activity? context = GetContext();
            Intent? intent = new Intent(context, typeof(SplashActivity));
            intent.SetFlags(ActivityFlags.ClearTask | ActivityFlags.ClearTop);
            intent.PutExtra(Constants.Keys.EXIT, true);
            context.StartActivity(intent);
            context.Finish();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">Default context will be used in case of null</param>
        public void MoveToLogin(object context = null)
        {
            RunInvokeRequired(_ =>
            {
                Activity? activity = context == null ? (Activity)HM.Container.Resolve<HatfContext>().Context : (Activity) context;

                // Clear all
                if (HM.Container.HasBinding<UserInformationVO>())
                {
                    HM.Container.Unbind<UserInformationVO>();
                }
                HM.Container.Resolve<TZMPrefs>().ClearAllPrefs();

                // Hide loading
                
                
                // Go to main activity which will take us to login
                Intent intent = new Intent(activity, typeof(LoginActivity));
                intent.SetFlags(ActivityFlags.ClearTask | ActivityFlags.ClearTop | ActivityFlags.NewTask);
                activity.StartActivity(intent);
                activity.Finish();
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">Default context will be used in case of null</param>
        public void MoveToMain(object context = null)
        {
            RunInvokeRequired(_ =>
            {
                Activity? activity = context == null ? (Activity)HM.Container.Resolve<HatfContext>().Context : (Activity) context;

                // Go to main activity which will take us to login
                Intent intent = new Intent(activity, typeof(MainActivity));
                intent.SetFlags(ActivityFlags.ClearTask | ActivityFlags.ClearTop | ActivityFlags.NewTask);
                activity.StartActivity(intent);
                activity.Finish();
            });
        }

        public void SetMenuBadgeText(object badgeResource, string text, bool hide = false)
        {
            try
            {
                TextView textView = (TextView) badgeResource;

                if (hide)
                {
                    textView.Visibility = ViewStates.Gone;
                }
                else
                {
                    textView.Visibility = ViewStates.Visible;
                }
                textView.Text = text;
                if (text.Length < 3)
                {
                    textView.SetTextSize(ComplexUnitType.Sp, 10);
                }
                else
                {
                    textView.SetTextSize(ComplexUnitType.Sp, 8);
                }
            }
            catch
            {
                throw;
            }
        }

        public void SetMenuBadgeVisibility(object badgeResource, string text)
        {
            throw new NotImplementedException();
        }

        private Activity GetContext() => (Activity) _container.Resolve<HatfContext>().Context;
        
        public string GetDeviceId()
        {
            try
            {
                return Android.Provider.Settings.Secure.GetString(Android.App.Application.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            }
            catch (Exception e)
            {
                HLogger.Error(e,"Unable to get device id");
            }

            return string.Empty;
        }
    }
}