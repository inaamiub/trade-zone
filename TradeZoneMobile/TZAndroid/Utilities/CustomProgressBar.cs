﻿using Android.Graphics;

namespace TZAndroid;

public class CustomProgressBar
{
    private Activity _context;
    private ProgressBar _progressBar = null;

    public CustomProgressBar(Activity context, ConstraintLayout layout)
    {
        this._context = context;

        try
        {
            //initiating layout params
            ConstraintLayout.LayoutParams lParams = new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.ChainSpread,
                ConstraintLayout.LayoutParams.WrapContent);
            lParams.BottomToBottom = layout.Id;
            lParams.LeftToLeft = layout.Id;
            lParams.RightToRight = layout.Id;
            lParams.TopToTop = layout.Id;

            _progressBar = new ProgressBar(context, null);

            Color color = new Android.Graphics.Color(ContextCompat.GetColor(context, Resource.Color.colorPrimary));
            _progressBar.IndeterminateDrawable.SetColorFilter(color, PorterDuff.Mode.SrcIn);
            _progressBar.Visibility = ViewStates.Gone;

            _progressBar.LayoutParameters = lParams;
            layout.AddView(_progressBar);
        }
        catch (Exception ex)
        {
            NotificationUtils.UserToast(context, ex.ToString());
        }
    }

    public void Show()
    {
        if (_progressBar != null)
        {
            Utils.HideKeyboardFrom(_context);
            _context.Window.SetFlags(WindowManagerFlags.NotTouchable, WindowManagerFlags.NotTouchable);
            _progressBar.Visibility = ViewStates.Visible;
        }
        else
        {
            NotificationUtils.DeveloperToast(_context, "Progressbar is null");
        }
    }

    public void Hide()
    {
        if (_progressBar != null)
        {
            _context.Window.ClearFlags(WindowManagerFlags.NotTouchable);
            _progressBar.Visibility = ViewStates.Gone;
        }
        else
        {
            NotificationUtils.DeveloperToast(_context, "Progressbar is null");
        }
    }

}
