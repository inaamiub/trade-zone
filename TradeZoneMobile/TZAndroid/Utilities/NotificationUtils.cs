﻿using Java.Lang;
using Exception = System.Exception;
using String = System.String;

namespace TZAndroid
{
    public class NotificationUtils
    {
        public static void DeveloperToast(Context context, String Txt)
        {
            try
            {
#if DEBUG
                Toast.MakeText(context, Txt, ToastLength.Long).Show();
#endif
            }
            catch (Exception e)
            {
                Toast.MakeText(context, "Toast Exp:\n" + e.Message, ToastLength.Short).Show();
            }
        }

        public static void DeveloperToast(Context context, Exception ex)
        {
            try
            {
                StringBuilder sb = new StringBuilder("Exception Occured ");
                sb.Append(ex.Message);
#if DEBUG
                sb.Append(" StackTrace ");
                sb.Append(ex.StackTrace);
#endif
                DeveloperToast(context, sb.ToString());
            }
            catch (Exception e)
            {
                Toast.MakeText(context, "Toast Exp:\n" + e.Message, ToastLength.Short).Show();
            }
        }

        public static void UserToast(Context context, String Txt)
        {
            try
            {
                Toast.MakeText(context, Txt, ToastLength.Long).Show();
            }
            catch (Exception e)
            {
                Toast.MakeText(context, "Toast Exp:\n" + e.Message, ToastLength.Short).Show();
            }
        }
    }
}