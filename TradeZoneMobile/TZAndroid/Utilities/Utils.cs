﻿using Android.Telephony;
using Android.Util;
using Android.Views.InputMethods;

namespace TZAndroid
{
    public class Utils : HatfMobile.Utils
    {
        public static void ButtonEffect(View button)
        {
            button.Touch += Button_Touch;
        }

        private static void Button_Touch(object sender, View.TouchEventArgs e)
        {
            try
            {
                View v = (View)sender;
                if(v.Background == null)
                {
                    v.SetBackgroundColor(Android.Graphics.Color.Argb(150, 255, 255, 255));
                }

                switch (e.Event.Action)
                {
                    case MotionEventActions.Down:
                        v.Background.SetColorFilter(Android.Graphics.Color.Argb(255, 244, 117, 33), Android.Graphics.PorterDuff.Mode.SrcAtop);
                        v.Invalidate();
                        break;

                    case MotionEventActions.Up:
                    case MotionEventActions.Outside:
                    case MotionEventActions.Cancel:
                        v.Background.ClearColorFilter();
                        v.Invalidate();
                        break;

                }
            }
            catch
            {

            }
        }

        public static AlertDialog AlertBox(Context context, string message, string title = null,
            string neutralButton = null, Action<object> neutralButtonClicked = null,
            string positiveButton = null, Action<object> positiveButtonClicked = null,
            string negativeButton = null, Action<object> negativeButtonClicked = null, object callbackData = null)
        {
            //Android.Support.V7.App.
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.SetCancelable(false);
            if (title == null)
            {
                title = "Alert";
            }
            builder.SetMessage(message).SetTitle(title);

            if (!string.IsNullOrWhiteSpace(positiveButton))
            {
                builder.SetPositiveButton(positiveButton, (EventHandler<DialogClickEventArgs>)null);
            }

            if (!string.IsNullOrWhiteSpace(negativeButton))
            {
                builder.SetNegativeButton(negativeButton, (EventHandler<DialogClickEventArgs>)null);
            }

            if ((string.IsNullOrWhiteSpace(positiveButton) && string.IsNullOrWhiteSpace(negativeButton)) || !string.IsNullOrWhiteSpace(neutralButton))
            {
                if(string.IsNullOrWhiteSpace(neutralButton))
                {
                    neutralButton = "OK";
                }
                builder.SetNeutralButton(neutralButton, (EventHandler<DialogClickEventArgs>)null);
            }
            AlertDialog dialog = builder.Create();
            dialog.Show();

            Button? yesBtn = dialog.GetButton((int)DialogButtonType.Positive);
            Button? noBtn = dialog.GetButton((int)DialogButtonType.Negative);
            Button? neutralBtn = dialog.GetButton((int)DialogButtonType.Neutral);

            yesBtn.Click += (sender, args) =>
            {
                positiveButtonClicked?.Invoke(callbackData);
                dialog.Dismiss();
            };
            noBtn.Click += (sender, args) =>
            {
                negativeButtonClicked?.Invoke(callbackData);
                dialog.Dismiss();
            };
            neutralBtn.Click += (sender, args) =>
            {
                neutralButtonClicked?.Invoke(callbackData);
                dialog.Dismiss();
            };
            return dialog;
        }

        public static void HideKeyboardFrom(Context context, View view)
        {
            try
            {
                InputMethodManager imm = (InputMethodManager)context.GetSystemService(Activity.InputMethodService);
                imm.HideSoftInputFromWindow(view.WindowToken, 0);
            }
            catch(Exception ex)
            {
                Log.Debug("Exception", ex.ToString());
            }
        }

        public static void HideKeyboardFrom(Activity context)
        {
            try
            {
                View view = context.CurrentFocus;
                InputMethodManager imm = (InputMethodManager)context.GetSystemService(Activity.InputMethodService);
                imm.HideSoftInputFromWindow(view.WindowToken, 0);
            }
            catch (Exception ex)
            {
                Log.Debug("Exception", ex.ToString());
            }
        }

        public static void CompleteRemnantsForUserInformation(Context context, UserType userType, ref UserInformationVO ui)
        {
            try
            {
                ui.UserType = userType;
                ui.BranchId = HConfigManager.Get<TZMConfig>().BranchId;
                ui.UUID = HatfShared.Utils.NewShortGuid();
                ui.LocalDateTime = DateTime.Now;
                try
                {
                    ui.Brand = Android.OS.Build.Brand;
                }
                catch
                {
                    // Ignore
                }
                try
                {
                    ui.SDKReleaseVersion = Android.OS.Build.VERSION.Release;
                }
                catch
                {
                    // Ignore
                }
                try
                {
                    ui.Manufacturer = Android.OS.Build.Manufacturer;
                }
                catch
                {
                    // Ignore
                }
                try
                {
                    TelephonyManager telephonyManager = (TelephonyManager)context.GetSystemService(Context.TelephonyService);
                    ui.SDKVersion = telephonyManager.DeviceSoftwareVersion;
#pragma warning disable CS0618 // Type or member is obsolete
                    ui.IMEI = telephonyManager.DeviceId;
#pragma warning disable CS0618 // Type or member is obsolete
                }
                catch
                {
                    // Ignore
                }
            }
            catch (Exception ex)
            {
                Log.Debug("Exception", ex.ToString());
            }
        }

    }
}