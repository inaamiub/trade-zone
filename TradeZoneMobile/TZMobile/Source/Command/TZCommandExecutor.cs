using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using HatfCommand;
using HatfCore;
using HatfDi;
using Nimbus.Server;
using TZMobile;

namespace TZShared
{
    public class TzCommandExecutor : CommandGateway
    {
        [Inject] private TZMPrefs _prefs;
        [Inject] private HatfHttpConfig _httpConfig;
        
        [Inject]
        private void Init()
        {
            string uri = _httpConfig.BaseUrl;
            if (uri.EndsWith("/") == false)
            {
                uri += "/";
            }

            uri += RouteConstants.EXECUTE;
            _uri = uri;
        }

        private string _uri;
        private AuthenticationHeaderValue _authHeader;

        protected override AuthenticationHeaderValue AuthenticationHeader
        {
            get
            {
                if (_authHeader == null || _authHeader.Parameter != _prefs.GetAccessToken())
                {
                    _authHeader = new AuthenticationHeaderValue("Bearer", _prefs.GetAccessToken());
                }

                return _authHeader;
            }
        }
        protected override string Uri => _uri;

        protected override async Task<bool> OnUnAuthorizedException(HExceptionData ex)
        {
            string refreshToken = _prefs.GetRefreshToken();
            if (string.IsNullOrEmpty(refreshToken) == false)
            {
                BulkCommandRequest commandRequest = new BulkCommandRequest
                {
                    Requests =
                    [
                        CommandRequest.Create(new LoginWithRefreshTokenCommandPayload()
                        {
                            RefreshToken = refreshToken,
                            ClientId = HConfigManager.Get<AuthConfig>().ClientId,
                            ClientSecret = HConfigManager.Get<AuthConfig>().ClientSecret,
                            DeviceId = HM.Container.Resolve<IUtilityService>().GetDeviceId(),
                            AppVersion = TZM.APP_VERSION.ToString(),
                            BranchId = HConfigManager.Get<TZMConfig>().BranchId,
                            Platform = Platform.Android
                        })
                    ]
                };
                (HExceptionData? exData, BulkCommandResponse response) = await SendRequest(commandRequest, false);
                if (exData == null && response != null && response.Responses.Count > 0)
                {
                    CommandResponse loginResponse = response.Responses.First();
                    if (loginResponse is { Exception: null })
                    {
                        LoginWithRefreshTokenCommandResult loginResult =
                            (LoginWithRefreshTokenCommandResult)loginResponse.Response;
                        string existingUserId = HM.Container.TryResolve<UserInformationVO>()?.Id;
                        string currentUserId = loginResult.User?.Id;
                        if (string.IsNullOrEmpty(currentUserId) == false && existingUserId == currentUserId)
                        {
                            _prefs.SetAccessToken(loginResult.AccessToken);
                            HM.Container.RebindInstance(loginResult.User);
                            return true;
                        }
                    }
                }
            }

            TZM.ON_BRANCH_NOT_FOUND_ERROR?.Invoke();
            return false;
        }
    }
}