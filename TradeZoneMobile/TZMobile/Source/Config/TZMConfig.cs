﻿namespace TZMobile
{
    public class TZMConfig : HBaseConfig
    {
        /// <summary>
        /// Branch Id
        /// </summary>
        public string BranchId { get; set; }
        
        public string LogoSmall { get; set; }

        public string BannerImage { get; set; }
        
        public string ApplicationTitle { get; set; }
        
        public bool ShowCartButton { get; set; }
        public bool SaveCartLocally { get; set; }
        public PaymentMethod DefaultPaymentMethod { get; set; }
        public OrderDeliveryType DefaultOrderDeliveryType { get; set; }
        
        public CompanyModel CompanyDetails { get; set; }
        public string PictureServerBaseUrl { get; set; }
        public string PrivacyPolicyUrl { get; set; }
    }
}