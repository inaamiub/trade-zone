﻿namespace TZMobile
{
    public class TZMDiBindingInitializer : IDiBindingInitializer
    {
        public void InitializeRootContainer(DiContainer container)
        {
            
        }

        public void InitializeRequestContainer(DiContainer container)
        {
        }

        public void InitializeUserContainer(DiContainer container)
        {
            // Bind all http requests
            foreach (Type httpRequestType in Hatf.ExecutingAssemblyTypes.Where(m => m.BaseType == typeof(TZHttpRequestBase)))
            {
                container.Bind(httpRequestType);
            }
            
            container.Bind<TZMPrefs>();
            container.Bind<ShoppingCartSystem>().AsSingle();
            container.Bind<TzCommandExecutor>().AsSingle();
        }
    }
}