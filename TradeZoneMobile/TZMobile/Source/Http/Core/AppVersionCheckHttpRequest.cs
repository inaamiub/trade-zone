﻿using System.Net.Http;

namespace TZMobile
{
    public class AppVersionCheckHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.APP_VERSION_CONTROLLER + "/" +
                   TzRouteConstants.APP_VERSION_ANDROID_NEW;
        }

        public override HttpMethod GetMethod() => HttpMethod.Post;

        public async Task<mdVersionCheckResponse> Send(int lastAppVersion)
        {
            RequestModel request = GenerateDefaultRequestModel();
            request.RequestData = new mdAppVersionCheckData()
            {
                BranchId = HConfigManager.Get<TZMConfig>().BranchId,
                LastAppVersion = lastAppVersion
            };
            
            mdCallResponse res = await Execute(request);
            return JsonUtils.Deserialize<mdVersionCheckResponse>(res.Extras.ToString());
        }
    }
}