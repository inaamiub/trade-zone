﻿namespace TZMobile
{
    public abstract class TZHttpRequestBase : HatfHttpRequestBase
    {
        [Inject] public IUtilityService _utilityService;
        protected virtual bool TryRefreshTokenIfExpired() => true;

        public override string GetBaseUrl() => HM.BaseUrl;

        /// <summary>
        /// Execute http request
        /// </summary>
        /// <returns></returns>
        protected async Task<mdCallResponse> Execute(RequestModel content = null, bool recursiveCall = false)
        {
            mdCallResponse res = await Execute<mdCallResponse>(content);

            if (res.IsSuccess)
            {
                return res;
            }
            
            if (res.ExceptionData != null)
            {
                bool relogin = false;
                if (res.Code == HatfErrorCodes.TokenExpired || res.Code == HatfErrorCodes.TokenNotFound)
                {
                    if (recursiveCall == false && TryRefreshTokenIfExpired() && await _container.Resolve<RefreshTokenHttpRequest>().Send() == true)
                    {
                        if (content != null)
                        {
                            content.Token = _container.Resolve<TZMPrefs>().GetAccessToken();
                            content.TokenType = TokenType.AccessToken;
                        }
                        return await Execute(content, recursiveCall || true);   
                    }
                    
                    // Go to re login
                    relogin = true;
                }
                
                // Check if exception is related to branch not found
                if (res.Code == TZErrorCodes.BRANCH_NOT_FOUND)
                {
                    // Handle branch not found;
                    relogin = true;
                }

                if (relogin)
                {
                    TZM.ON_BRANCH_NOT_FOUND_ERROR?.Invoke();
                }
                
                // Throw exception if found
                throw new HException(res.ExceptionData);
            }

            // Throw exception with message if not found
            throw new Exception(res.Message);
        }

        protected RequestModel GenerateDefaultRequestModel()
        {
            return new RequestModel
            {
                Token = _container.Resolve<TZMPrefs>().GetAccessToken(),
                ClientId = HConfigManager.Get<AuthConfig>().ClientId,
                ClientSecret = HConfigManager.Get<AuthConfig>().ClientSecret,
                DeviceId = _container.Resolve<IUtilityService>().GetDeviceId(),
                TokenType = TokenType.AccessToken,
                AppVersion = TZM.APP_VERSION
            };
        }
    }
}