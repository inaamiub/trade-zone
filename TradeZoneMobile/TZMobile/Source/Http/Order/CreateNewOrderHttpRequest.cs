﻿namespace TZMobile
{
    public class CreateNewOrderHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ORDER_CONTROLLER + "/" + TzRouteConstants.ORDER_NEW_ANDROID_ORDER;
        }

        public async Task<bool> Send(mdNewOrder order)
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                request.RequestData = order;
                mdCallResponse res = await Execute(request);

                mdOrderHistory orderHistory = JsonUtils.Deserialize<mdOrderHistory>(res.Extras.ToString());

                _container.RebindInstance(orderHistory);
            
                return true;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request {0}", GetRoute());
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
            }
            return false;
        }

    }
}