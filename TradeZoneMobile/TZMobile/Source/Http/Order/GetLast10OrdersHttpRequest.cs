﻿namespace TZMobile
{
    public class GetLast10OrdersHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ORDER_CONTROLLER + "/" + TzRouteConstants.ORDER_HISTORY;
        }

        public async Task Send()
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                mdCallResponse res = await Execute(request);

                mdOrderHistory orderHistory = JsonUtils.Deserialize<mdOrderHistory>(res.Extras.ToString());

                _container.RebindInstance(orderHistory);
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request {0}", GetRoute());
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
            }
        }

    }
}