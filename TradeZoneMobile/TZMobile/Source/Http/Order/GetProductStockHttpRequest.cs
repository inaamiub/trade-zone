﻿namespace TZMobile
{
    public class GetProductStockHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ORDER_CONTROLLER + "/" + TzRouteConstants.ORDER_GET_PRODUCT_STOCK;
        }

        public async Task<mdProductStock> Send()
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                mdCallResponse res = await Execute(request);

                mdProductStock productStock = JsonUtils.Deserialize<mdProductStock>(res.Extras.ToString());
                List<ProductStockModel> stocks = productStock?.ProductStock ?? new List<ProductStockModel>();

                mdProductsWithStock oldStock = _container.Resolve<mdProductsWithStock>();
                oldStock.ReportData ??= new List<StockReportModel>();
                for (int i = 0; i < oldStock.ReportData.Count; i++)
                {
                    ProductStockModel newStock = stocks.FirstOrDefault(m => m.ProductID == oldStock.ReportData[i].ProductID);
                    if (newStock == null)
                    {
                        oldStock.ReportData[i].Stock = 0;
                    }
                    else
                    {
                        oldStock.ReportData[i].Stock = newStock.Stock;
                    }
                }
                
                // Check if product exists in new stock but not in old
                

                // No need to bind as we're updating the reference
                // _container.RebindInstance(new mdProductsWithStock() {ReportData = oldStock});
            
                return productStock;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}