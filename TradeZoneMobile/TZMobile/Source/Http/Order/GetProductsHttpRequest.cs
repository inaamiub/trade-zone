﻿namespace TZMobile
{
    public class GetProductsHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ORDER_CONTROLLER + "/" + TzRouteConstants.ORDER_GET_PRODUCTS;
        }

        public async Task<mdProducts> Send()
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                mdCallResponse res = await Execute(request);

                mdProducts products = JsonUtils.Deserialize<mdProducts>(res.Extras.ToString());
                
                _container.RebindInstance(products);
            
                return products;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}