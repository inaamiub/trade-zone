﻿namespace TZMobile
{
    public class GetProductsWithStockHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ORDER_CONTROLLER + "/" + TzRouteConstants.ORDER_GET_PRODUCTS_WITH_STOCK;
        }

        public async Task<mdProductsWithStock> Send()
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                mdCallResponse res = await Execute(request);

                mdProductsWithStock stockReport = JsonUtils.Deserialize<mdProductsWithStock>(res.Extras.ToString());

                _container.RebindInstance(stockReport);
            
                return stockReport;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}