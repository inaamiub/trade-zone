﻿namespace TZMobile
{
    public class GetUserInformationHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_GET_USER_INFORMATION;
        }

        public async Task Send()
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                mdCallResponse res = await Execute(request);

                mdLoginResponse loginResponse = JsonUtils.Deserialize<mdLoginResponse>(res.Extras.ToString());
                // Bind user information with container
                _container.BindInstance(loginResponse.User);
                
                HM.Container.Resolve<IUtilityService>().MoveToMain();
            }
            catch (Exception e)
            {
                HM.Container.Resolve<IUtilityService>().MoveToLogin();
            }
        }

    }
}