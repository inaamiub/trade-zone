﻿namespace TZMobile
{
    public class LoginHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_LOGIN;
        }

        public async Task<mdLoginResponse> Send(string userName, string password)
        {
            RequestModel request = GenerateDefaultRequestModel();
            request.UserName = TZAuthHelper.CombineBranchAndUserName(HConfigManager.Get<TZMConfig>().BranchId, userName);
            request.Password = password;
            request.GrantType = GrantType.Password;

            mdCallResponse res = await Execute(request);

            mdLoginResponse loginResponse = JsonUtils.Deserialize<mdLoginResponse>(res.Extras.ToString());
            // Bind user information with container
            _container.Resolve<TZMPrefs>().SetAccessToken(loginResponse.AccessToken);
            _container.Resolve<TZMPrefs>().SetRefreshToken(loginResponse.RefreshToken);
            _container.BindInstance(loginResponse.User);
            return loginResponse;
        }

    }
}