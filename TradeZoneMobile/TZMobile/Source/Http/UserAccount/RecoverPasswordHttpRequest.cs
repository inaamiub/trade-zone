﻿namespace TZMobile
{
    public class RecoverPasswordHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" +
                   TzRouteConstants.ACCOUNT_RECOVER_PASSWORD;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns true if access token is successfully refreshed otherwise false</returns>
        /// <exception cref="Exception"></exception>
        public async Task Send(string email, string verificationCode, string password)
        {
            RequestModel request = GenerateDefaultRequestModel();
            request.UserName = TZAuthHelper.CombineBranchAndUserName(HConfigManager.Get<TZMConfig>().BranchId, email);
            request.Token = verificationCode;
            request.RequestData = new mdUserInformation
            {
                NewPassword = password
            };

            try
            {
                mdCallResponse res = await Execute(request);
                // Request executed successfully
                _container.Resolve<IUtilityService>().AlertBox(
                    "Your password has been successfully updated. Please log in to continue", "Success",
                    neutralButton: "OK",
                    neutralButtonClicked: _ => { HM.Container.Resolve<IUtilityService>().MoveToLogin(); });
            }
            catch (HException e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
            }
        }

    }
}