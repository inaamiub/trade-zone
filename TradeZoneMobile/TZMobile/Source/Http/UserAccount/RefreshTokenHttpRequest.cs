﻿namespace TZMobile
{
    public class RefreshTokenHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_REFRESH_TOKEN;
        }

        protected override bool TryRefreshTokenIfExpired() => false;

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns true if access token is successfully refreshed otherwise false</returns>
        /// <exception cref="Exception"></exception>
        public async Task<bool> Send()
        {
            if (_container.TryResolve(out TZMPrefs pref) == false)
            {
                throw new Exception("Unable to resolve Prefs");
            }

            string refreshToken = pref.GetRefreshToken();
            if (string.IsNullOrEmpty(refreshToken) == false)
            {
                RequestModel request = GenerateDefaultRequestModel();
                request.Token = refreshToken;
                request.TokenType = TokenType.RefreshToken;
                request.GrantType = GrantType.RefreshToken;

                try
                {
                    mdCallResponse res = await Execute(request);
                    mdLoginResponse loginResponse = JsonUtils.Deserialize<mdLoginResponse>(res.Extras.ToString());
                    // Bind user information with container
                    pref.SetAccessToken(loginResponse.AccessToken);
                    _container.BindInstance(loginResponse.User);
                    return true;
                }
                catch (HException e)
                {
                    HLogger.Error(e, "Exception occured while executing http request");
                    // Check if refresh token is also expired
                    if (e.ErrorCode == HatfErrorCodes.TokenExpired || e.ErrorCode == HatfErrorCodes.TokenNotFound)
                    {
                        // Go to login
                        HM.Container.Resolve<IUtilityService>().MoveToLogin();
                    }
                }

            }
            else
            {
                HM.Container.Resolve<IUtilityService>().MoveToLogin();
            }

            return false;
        }

    }
}