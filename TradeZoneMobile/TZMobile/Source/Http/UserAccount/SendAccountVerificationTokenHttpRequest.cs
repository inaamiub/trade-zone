﻿namespace TZMobile
{
    public class SendAccountVerificationTokenHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_SEND_ACCOUNT_VERIFICATION_TOKEN;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns true if access token is successfully refreshed otherwise false</returns>
        /// <exception cref="Exception"></exception>
        public async Task<bool> Send()
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                mdCallResponse res = await Execute(request);
                return res.IsSuccess;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
                return false;
            }
        }

    }
}