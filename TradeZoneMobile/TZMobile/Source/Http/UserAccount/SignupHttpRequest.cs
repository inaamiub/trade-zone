﻿namespace TZMobile
{
    public class SignupHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_SIGNUP;
        }

        public async Task<bool> Send(UserInformationVO userInformationVo)
        {
            RequestModel request = GenerateDefaultRequestModel();
            request.RequestData = userInformationVo;
            try
            {
                mdCallResponse res = await Execute(request);
            
                mdLoginResponse signupResponse = JsonUtils.Deserialize<mdLoginResponse>(res.Extras.ToString());
                            
                // Bind user information with container
                _container.Resolve<TZMPrefs>().SetAccessToken(signupResponse.AccessToken);
                _container.Resolve<TZMPrefs>().SetRefreshToken(signupResponse.RefreshToken);
                _container.BindInstance(signupResponse.User);
            
                return true;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
            }

            return false;
        }

    }
}