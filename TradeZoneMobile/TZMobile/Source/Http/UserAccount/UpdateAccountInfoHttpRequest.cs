﻿namespace TZMobile
{
    public class UpdateAccountInfoHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_UPDATE_ACCOUNT_INFO;
        }

        public async Task<bool> Send(mdUserInformation userInformation)
        {
            RequestModel request = GenerateDefaultRequestModel();
            request.RequestData = userInformation;

            try
            {
                await Execute(request);
                _container.Resolve<IUtilityService>().AlertBox("Updated Successfully", "Success", "OK",
                    (object para) => { _container.Resolve<IUtilityService>().MoveToLogin(); });
                return true;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
            }

            return false;
        }

    }
}