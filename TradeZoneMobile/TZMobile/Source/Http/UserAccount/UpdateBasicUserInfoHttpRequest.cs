﻿namespace TZMobile
{
    public class UpdateBasicUserInfoHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" +
                   TzRouteConstants.ACCOUNT_UPDATE_BASIC_USER_INFO;
        }

        public async Task<bool> Send(UserInformationVO ui)
        {
            RequestModel request = GenerateDefaultRequestModel();
            ui.BranchId = HConfigManager.Get<TZMConfig>().BranchId;
            request.RequestData = ui;
            try
            {
                mdCallResponse res = await Execute(request);
            
                mdLoginResponse loginResponse = JsonUtils.Deserialize<mdLoginResponse>(res.Extras.ToString());
            
                _container.BindInstance(loginResponse.User);

                _container.Resolve<IUtilityService>().AlertBox("Information updated successfully", "Success",
                    neutralButton: "OK", o => _utilityService.MoveToMain());
                
                return true;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
            }

            return false;
        }

    }
}