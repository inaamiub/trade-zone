﻿namespace TZMobile
{
    public class VerifyEmailHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_VERIFY_EMAIL;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns true if access token is successfully refreshed otherwise false</returns>
        /// <exception cref="Exception"></exception>
        public async Task<bool> Send(string verificationToken)
        {
            RequestModel request = GenerateDefaultRequestModel();
            request.RequestData = verificationToken;
            
            try
            {
                await Execute(request);
                // Request executed successfully
                _container.Resolve<IUtilityService>().AlertBox("Your account is successfully verified", "Success",
                    neutralButton: "OK",
                    neutralButtonClicked: _ =>
                    {
                        UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
                        userInformationVo.RecordStatus = UserRecordStatus.Active;
                        HM.Container.Resolve<IUtilityService>().MoveToMain();
                    });
                return true;
            }
            catch (HException e)
            {
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
                return false;
            }
        }

    }
}