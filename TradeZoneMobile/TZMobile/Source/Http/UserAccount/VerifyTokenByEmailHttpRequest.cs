﻿namespace TZMobile
{
    public class VerifyTokenByEmailHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER + "/" + TzRouteConstants.ACCOUNT_VERIFY_TOKEN_BY_EMAIL;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns true if access token is successfully refreshed otherwise false</returns>
        /// <exception cref="Exception"></exception>
        public async Task<bool> Send(string verificationToken, string email, TokenType tokenType)
        {
            RequestModel request = GenerateDefaultRequestModel();
            request.UserName = TZAuthHelper.CombineBranchAndUserName(HConfigManager.Get<TZMConfig>().BranchId, email);
            request.RequestData = new RequestModel
            {
                TokenType = tokenType,
                Token = verificationToken
            };

            try
            {
                mdCallResponse res = await Execute(request);
                return res.IsSuccess;
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
                return false;
            }
        }

    }
}