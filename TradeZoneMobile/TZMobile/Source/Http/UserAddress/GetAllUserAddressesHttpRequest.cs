﻿namespace TZMobile
{
    public class GetAllUserAddressesHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.USER_ADDRESS_CONTROLLER + "/" + TzRouteConstants.GET_ALL_USER_ADDRESSES;
        }

        public async Task Send()
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                mdCallResponse res = await Execute(request);
                mdUserAddresses userAddresses = JsonUtils.Deserialize<mdUserAddresses>(res.Extras.ToString());

                string lastOrderAddressId = _container.Resolve<UserInformationVO>().UserAddressVO?.Id;
                List<UserAddressVO> userAddressesVos = new List<UserAddressVO>();
                if (userAddresses != null)
                {
                    lastOrderAddressId = userAddresses.LastUsedAddressId;
                    if (userAddresses.Addresses != null && userAddresses.Addresses.Count > 0)
                    {
                        userAddressesVos = userAddresses.Addresses;
                    }
                }

                _container.BindInstanceWithId(userAddressesVos, ContainerConstants.ALL_USER_ADDRESSES);
                _container.BindInstanceWithId(lastOrderAddressId, ContainerConstants.USER_LAST_USED_ADDRESS_ID);
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request");
            }
        }

    }
}