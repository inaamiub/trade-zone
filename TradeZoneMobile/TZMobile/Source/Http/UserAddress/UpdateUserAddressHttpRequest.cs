﻿namespace TZMobile
{
    public class UpdateUserAddressHttpRequest : TZHttpRequestBase
    {
        public override string GetRoute()
        {
            return RouteConstants.API + "/" + TzRouteConstants.USER_ADDRESS_CONTROLLER + "/" + TzRouteConstants.UPDATE_ADDRESS;
        }

        public async Task<UserAddressVO> Send(UserAddressVO userAddressVo)
        {
            try
            {
                RequestModel request = GenerateDefaultRequestModel();
                request.RequestData = userAddressVo;
                mdCallResponse res = await Execute(request);

                mdUserAddresses userAddresses = JsonUtils.Deserialize<mdUserAddresses>(res.Extras.ToString());
                if (userAddresses?.Addresses?.Count > 0)
                {
                    return userAddresses.Addresses.First();
                }
                
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while executing http request {0}", GetRoute());
                _container.Resolve<IUtilityService>().AlertBox(e.Message, "Error", neutralButton: "OK");
            }
            return null;
        }

    }
}