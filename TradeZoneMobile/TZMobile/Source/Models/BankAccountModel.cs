﻿namespace TZMobile
{
    [Serializable]
    public class BankAccountModel
    {
        public BankAccountModel(string bank = "", string title = "", string accountNo = "")
        {
            Bank = bank;
            Title = title;
            AccountNo = accountNo;
        }
        public string Title { get; set; }
        public string AccountNo { get; set; }
        public string Bank { get; set; }
    }
}
