﻿namespace TZMobile
{
    [Serializable]
    public class CompanyModel
    {
        public CompanyModel()
        {
            this.BankAccounts = new List<BankAccountModel>();
        }
        public string OwnerName{ get; set; }
        public string CompanyName{ get; set; }
        public string[] CompanyPhone{ get; set; }
        public string[] CompanyCell{ get; set; }
        public string CompanyAddress{ get; set; }
        public string CompanyEmail{ get; set; }
        public List<BankAccountModel> BankAccounts { get; set; }
    }
}