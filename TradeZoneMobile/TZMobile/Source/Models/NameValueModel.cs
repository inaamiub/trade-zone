﻿namespace TZMobile
{
    [Serializable]
    public class NameValueModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}