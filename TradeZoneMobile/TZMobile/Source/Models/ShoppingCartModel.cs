﻿namespace TZMobile
{
    public class ShoppingCartModel
    {
        public ShoppingCartModel()
        {
            Items = new List<ShoppingCartItemModel>();
        }
        public List<ShoppingCartItemModel> Items;
    }
}