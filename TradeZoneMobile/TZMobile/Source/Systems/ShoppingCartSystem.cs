﻿using System.IO;

namespace TZMobile
{
    public class ShoppingCartSystem
    {
        private TZMConfig _tzmConfig;
        private UserInformationVO _userInformationVo;
        
        [Inject]
        public void OnInject(DiContainer container)
        {
            _tzmConfig = container.Resolve<TZMConfig>();
            _cart = new ShoppingCartModel();
            _userInformationVo = container.Resolve<UserInformationVO>();
            LoadCartFromDb();
        }

        private ShoppingCartModel _cart;

        private void LoadCartFromDb()
        {
            if (_tzmConfig.SaveCartLocally == false)
            {
                return;
            }

            List<ShoppingCartItemModel> cartItems = SQLiteBase.Instance.GetAll<ShoppingCartItemModel>(m => m.UserId == _userInformationVo.Id);
            _cart.Items = new List<ShoppingCartItemModel>();
            foreach (ShoppingCartItemModel cartItem in cartItems)
            {
                _cart.Items.Add(cartItem);
            }

            if (_cart.Items.Count > 0)
            {
                ShoppingCartItemModel item = _cart.Items.Last();
                _onCartUpdated?.Invoke(item.ItemId, item.Quantity, TotalQuantity);
            }
        }
        
        /*******************************************************************************
        *                                    Operations                                 
        ********************************************************************************/
        public void UpdateItem(string itemId, uint quantity)
        {
            _cart.Items ??= new List<ShoppingCartItemModel>();
            bool isUpdated = false;

            if (quantity < 1)
            {
                if (ItemExists(itemId))
                {
                    RemoveItem(itemId);
                    isUpdated = true;
                }
            }
            else
            {
                if (ItemExists(itemId) == false)
                {
                    AddItem(itemId, quantity);
                    isUpdated = true;
                }
                else
                {
                    uint existingQuantity = GetItemQuantity(itemId);
                    if (existingQuantity != quantity)
                    {
                        UpdateQuantity(itemId, quantity);
                        isUpdated = true;
                    }
                }
            }

            if (isUpdated)
            {
                _onCartUpdated?.Invoke(itemId, quantity, TotalQuantity);
            }
        }

        private void RemoveItem(string itemId)
        {
            ShoppingCartItemModel item = GetItem(itemId);
            _cart.Items.Remove(item);
            if (_tzmConfig.SaveCartLocally)
            {
                SQLiteBase.Instance.Delete(item);
            }
        }

        private void AddItem(string itemId, uint quantity)
        {
            ShoppingCartItemModel item = new ShoppingCartItemModel()
            {
                UserId = _userInformationVo.Id,
                ItemId = itemId, 
                Quantity = quantity
            };
            _cart.Items.Add(item);
            if (_tzmConfig.SaveCartLocally)
            {
                SQLiteBase.Instance.Insert(item);
            }
        }
        
        private void UpdateQuantity(string itemId, uint quantity)
        {
            ShoppingCartItemModel item = GetItem(itemId);
            item.Quantity = quantity;
            if (_tzmConfig.SaveCartLocally)
            {
                SQLiteBase.Instance.Update(item);
            }
        }

        public void ClearCart()
        {
            try
            {
                if (_tzmConfig.SaveCartLocally)
                {
                    foreach (ShoppingCartItemModel item in _cart.Items)
                    {
                        // Delete from local db
                        SQLiteBase.Instance.Delete(item);   
                    }
                }

                // Reset local items cache
                _cart.Items = new List<ShoppingCartItemModel>();

                _onCartUpdated?.Invoke("", 0, TotalQuantity);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        /*******************************************************************************
        *                                    Helper Functions                                 
        ********************************************************************************/
        public uint GetItemQuantity(string itemId)
        {
            return GetItem(itemId)?.Quantity ?? 0;
        }
        
        public ShoppingCartItemModel GetItem(string itemId)
        {
            ShoppingCartItemModel item = _cart?.Items?.FirstOrDefault(m => m.ItemId == itemId);
            if (item != null)
            {
                item.ProductDetails = GetItemDetails(itemId);
            }

            return item;
        }

        public uint CartLength => (uint)(_cart?.Items?.Count ?? 0);
        
        public long TotalQuantity => _cart?.Items?.Sum(m => m.Quantity) ?? 0;

        public List<ShoppingCartItemModel> GetAllItems()
        {
            if (_cart?.Items == null)
            {
                return new List<ShoppingCartItemModel>(); 
            }

            List<ShoppingCartItemModel> items = _cart.Items.Select(m => new ShoppingCartItemModel(m)
            {
                ProductDetails = GetItemDetails(m.ItemId)
            }).OrderBy(m => m.ProductDetails?.ProductName).ToList();

            List<ShoppingCartItemModel> emptyProductDetailsItems = items.Where(m => m.ProductDetails == null).ToList();
            if (emptyProductDetailsItems?.Count < 1)
            {
                return items;
            }
            
            // Remove items which doesn't have item details
            foreach (ShoppingCartItemModel emptyProductDetailsItem in emptyProductDetailsItems)
            {
                this.RemoveItem(emptyProductDetailsItem.ItemId);
            }

            return GetAllItems();
        }

        public bool ItemExists(string itemId) => _cart?.Items?.Count(m => m.ItemId == itemId) > 0;

        private StockReportModel GetItemDetails(string itemId)
        {
            mdProductsWithStock products = HM.Container.TryResolve<mdProductsWithStock>();
            if(products != default)
            {
                if (products?.ReportData?.Any(m => m.ProductID == itemId) == true)
                {
                    return products.ReportData.First(m => m.ProductID == itemId);
                }
            }

            return null;
        }
        
        /*******************************************************************************
        *                                    Event Listeners                                 
        ********************************************************************************/
        private readonly HatfEvent<string, uint, long> _onCartUpdated = new HatfEvent<string, uint, long>();

        public void AddOnUpdatedEventListener(HatfAction<string, uint, long> action)
        {
            _onCartUpdated.AddListener(action);
        }
        
        public void RemoveOnUpdatedEventListener(HatfAction<string, uint, long> action)
        {
            _onCartUpdated.RemoveListener(action);
        }
        
        public void RemoveOnUpdatedEventListeners()
        {
            _onCartUpdated.RemoveAllListeners();
        }
    }
}