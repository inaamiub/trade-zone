﻿namespace TZMobile
{
    public class TZM
    {
        /********************************************************************************
        *                                    Properties                                 
        ********************************************************************************/

        /// <summary>
        /// Should be available when initialize is finished
        /// </summary>
        public static string ACCESS_TOKEN
        {
            get
            {
                if (HM.Container.TryResolve(out TZMPrefs pref))
                {
                    return pref.GetAccessToken();
                }

                return null;
            }
        }

        private static int _appVersion = -1;
        public static int APP_VERSION
        {
            get
            {
                if (_appVersion == -1)
                {
                    if (HM.Container.TryResolve(out IUtilityService utilityService))
                    {
                        _appVersion = utilityService.GetAppVersionCode();
                    }
                }
                
                return _appVersion;
            }
        }

        public static bool IS_LOGGED_IN => string.IsNullOrEmpty(ACCESS_TOKEN) == false;
        public static Action ON_BRANCH_NOT_FOUND_ERROR;
        public static bool FORCE_LOGIN = false;

        /********************************************************************************
        *                                    Tests                                 
        ********************************************************************************/
        public static string UserName = "inaamiub";
        public static string Password = "123456";
        public static string Name = "Inaam";
        public static string CellNo = "03217575354";
        public static string City = "KTP";
        public static string Address = "KTP";
        public static string State = "Punjab";
        public static string Email = "inaamiub@gmail.com";
        
    }
}