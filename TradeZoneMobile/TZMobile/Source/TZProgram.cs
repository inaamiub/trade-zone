﻿using HatfCommand;

namespace TZMobile
{
    public class TZProgram
    {
        public static async Task Initialize(Platform env, string appsettingsFileName)
        {
            HatfMobile.HatfMobile.RegisterHatfMobile();
            // Register assemblies
            Hatf.RegisterAssemblies(new List<string>
            {
                "TZMobile",
                "TZShared"
            });

            // Register init tasks
            
            Hatf.RegisterJsonConverter(new CommandResultJsonConverter());
            
            // Register di binding initializer
            HContainerFactory.RegisterDiBindingInitializer(new TZMDiBindingInitializer());
      
            await HatfMobile.HatfMobile.Initialize(env, appsettingsFileName);

        }
    }
}