﻿namespace TZMobile
{
    public class TZMPrefs
    {
        [Inject] public IUtilityService _pref;

        public string GetAccessToken()
        {
            return _pref.GetString(TZMPrefKeys.ACCESS_TOKEN, string.Empty);
        }
        
        public void SetAccessToken(string accessToken)
        {
            _pref.PutSharedPreference(TZMPrefKeys.ACCESS_TOKEN, accessToken);
        }

        public int GetLastAppVersion()
        {
            return _pref.GetInteger(TZMPrefKeys.LAST_APP_VERSION, -1);
        }
        
        public bool GetPrivacyPolicy()
        {
            return _pref.GetInteger(TZMPrefKeys.PRIVACY_POLICY, 0) > 0;
        }
        
        public void SetLastAppVersion(int version)
        {
            _pref.PutSharedPreference(TZMPrefKeys.LAST_APP_VERSION, version);
        }
        
        public string GetRefreshToken()
        {
            return _pref.GetString(TZMPrefKeys.REFRESH_TOKEN, string.Empty);
        }

        public void SetRefreshToken(string refreshToken)
        {
            _pref.PutSharedPreference(TZMPrefKeys.REFRESH_TOKEN, refreshToken);
        }

        public void SetPrivacyPolicy(bool value)
        {
            _pref.PutSharedPreference(TZMPrefKeys.PRIVACY_POLICY, value == true ? 1 : 0);
        }

        public void ClearAllPrefs()
        {
            _pref.PutSharedPreference(TZMPrefKeys.ACCESS_TOKEN, string.Empty);
            _pref.PutSharedPreference(TZMPrefKeys.REFRESH_TOKEN, string.Empty);
            _pref.PutSharedPreference(TZMPrefKeys.LAST_SYNC_TIME, string.Empty);
        }
    }

    public class TZMPrefKeys
    {
        public const string ACCESS_TOKEN = "ACCESS_TOKEN";
        public const string REFRESH_TOKEN = "REFRESH_TOKEN";
        public const string LAST_SYNC_TIME = "LAST_SYNC_TIME";
        public const string LAST_APP_VERSION = "LAST_APP_VERSION";
        public const string PRIVACY_POLICY = "PRIVACY_POLICY";
    }
}