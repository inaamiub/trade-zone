﻿using HatfCommand;
using Microsoft.Extensions.Logging.Abstractions;

namespace TZServer;

public class LoginGuestUserCommandImpl : LoginGuestUserCommand
{
    [Inject] private SyncAllowedBranches _syncAllowedBranches;
    [Inject] private TZRegistry _registry;
    [Inject] private TzOAuth _auth;

    public override async Task<LoginUserCommandResult> Execute(LoginGuestUserCommandPayload request)
    {
        if (string.IsNullOrEmpty(request.BranchId))
        {
            throw new HException(TZErrorCodes.EmptyBranchId);
        }

        if (await _syncAllowedBranches.BranchAllowed(request.BranchId) == false)
        {
            throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
        }

        string userId = _registry.TryGetGuestUserId(request.BranchId);
        if (string.IsNullOrEmpty(userId))
        {
            HLogger.Error("Can't find guest user id for branch {0}", request.BranchId);
            throw new HException(TZErrorCodes.GUEST_NOT_SUPPORTED);
        }

        AuthRequest authRequest = request.ToAuthRequest();
        AuthResult authResult = await _auth.LoginWithUserId(authRequest, userId);
        return authResult.ToLoginResult(Container, request);
    }
}