﻿using HatfCommand;

namespace TZServer;

public class LoginUserCommandImpl : LoginUserCommand
{
    [Inject] private SyncAllowedBranches _syncAllowedBranches;
    [Inject] private TzJwtTokenValidator _tokenGenerator;
    [Inject] private IOAuth _auth;

    public override async Task<LoginUserCommandResult> Execute(LoginUserCommandPayload request)
    {
        if (string.IsNullOrEmpty(request.BranchId))
        {
            throw new HException(TZErrorCodes.EmptyBranchId);
        }

        if (await _syncAllowedBranches.BranchAllowed(request.BranchId) == false)
        {
            throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
        }

        AuthRequest authRequest = request.ToAuthRequest();
        AuthResult authResult = await _auth.Login(authRequest);
        return authResult.ToLoginResult(Container, request);
    }
}