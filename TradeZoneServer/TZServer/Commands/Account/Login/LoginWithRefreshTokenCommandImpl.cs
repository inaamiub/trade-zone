﻿using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace TZServer;

public class LoginWithRefreshTokenCommandImpl : LoginWithRefreshTokenCommand
{
    [Inject] private TzJwtTokenValidator _jwtTokenValidator;
    [Inject] private TokenValidationParameters _tokenValidationParameters;
    [Inject] private TzOAuth _tzOAuth;
    public override async Task<LoginWithRefreshTokenCommandResult> Execute(LoginWithRefreshTokenCommandPayload request)
    {
        if (string.IsNullOrEmpty(request.RefreshToken))
        {
            throw new HException(HatfErrorCodes.EmptyRefreshToken);
        }

        TokenValidationResult validationResult = await _jwtTokenValidator.ValidateTokenAsync(request.RefreshToken, _tokenValidationParameters);
        if (validationResult.IsValid == false)
        {
            throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);
        }

        string userId = validationResult.ClaimsIdentity.GetUserId();
        AuthRequest authRequest = new AuthRequest()
        {
            AppVersion = request.AppVersion,
            ClientId = request.ClientId,
            DeviceId = request.DeviceId,
            Platform = request.Platform,
            AdditionalData = request.BranchId,
            ClientSecret = request.ClientSecret,
            LangKey = request.LangKey
        };
        AuthResult authResult = await _tzOAuth.LoginWithUserId(authRequest, userId, false);
        return authResult.ToLoginResult(Container, request);
    }
}