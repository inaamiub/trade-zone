﻿namespace TZServer;

public class SignupUserCommandImpl : SignupUserCommand
{
    [Inject] private TzOAuth _oAuth;
    public override async Task<LoginUserCommandResult> Execute(SignupUserCommandPayload request)
    {
        // Signup Means full access
        UserAccountManagementService accountService = UserAccountManagementService.Create(Container);
        AuthResult authResult = await accountService.Signup(request);
        return authResult.ToLoginResult(Container, request);
    }
}