﻿using HatfCommand;

namespace TZServer;

public class GetCurrentUserProfileCommandImpl : GetCurrentUserProfileCommand
{
    [Inject] private HSqlFacade _sqlFacade;
    public override async Task<GetCurrentUserProfileCommandResult> Execute(VoidCommandPayload request)
    {
        UserInformationVO user = await _sqlFacade.GetById<UserInformationVO>(UserId);
        return new GetCurrentUserProfileCommandResult()
        {
            User = user
        };
    }
}