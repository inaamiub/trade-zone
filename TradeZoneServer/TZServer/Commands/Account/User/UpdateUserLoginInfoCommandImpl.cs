﻿namespace TZServer;

public class UpdateUserLoginInfoCommandImpl : UpdateUserLoginInfoCommand
{
    [Inject] private UserAccountManagementService _accountService;
    [Inject] private HSqlFacade _sqlFacade;
    
    protected override async Task VoidExecute(UpdateUserLoginInfoCommandPayload request)
    {
        UserInformationVO user = await _sqlFacade.GetById<UserInformationVO>(UserId);
        await _accountService.UpdateAccountInfo(user, new mdUserInformation
        {
            CurrentPassword = request.CurrentPassword,
            NewPassword = request.NewPassword,
            NewUserName = request.NewUserName
        });

    }
}