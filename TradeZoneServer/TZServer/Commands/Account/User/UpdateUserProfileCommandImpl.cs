﻿namespace TZServer;

public class UpdateUserProfileCommandImpl : UpdateUserProfileCommand
{
    [Inject] private UserAccountManagementService _accountService;
    public override async Task<GetCurrentUserProfileCommandResult> Execute(UpdateUserProfileCommandPayload request)
    {
        // Signup Means full access
        UserInformationVO updateUserVo = await _accountService.UpdateBasicUserInfo(request.User);

        return new GetCurrentUserProfileCommandResult
        {
            User = updateUserVo
        };
    }
}