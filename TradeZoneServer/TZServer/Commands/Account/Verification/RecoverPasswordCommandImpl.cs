﻿namespace TZServer;

public class RecoverPasswordCommandImpl : RecoverPasswordCommand
{
    [Inject] private UserAccountManagementService _accountService;
    [Inject] private TzOAuth _oAuth;
    protected override async Task VoidExecute(RecoverPasswordCommandPayload request)
    {
        UserInformationVO user = await _accountService.GetUserInformationByEmail(request.BranchId, request.Email); 

        // Expecting branch id in user name
        await _oAuth.ValidateToken(user.Id, request.Token, TokenType.PasswordRecoveryToken);

        // Update password
        mdUserInformation accountUpdateData = new mdUserInformation()
        {
            NewUserName = string.Empty,
            CurrentPassword = string.Empty,
            NewPassword = request.NewPassword
        };

        await _accountService.UpdateAccountInfo(user, accountUpdateData, false);
    }
}