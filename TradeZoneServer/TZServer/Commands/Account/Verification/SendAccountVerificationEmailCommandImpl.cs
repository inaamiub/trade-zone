﻿using HatfCommand;

namespace TZServer;

public class SendAccountVerificationEmailCommandImpl : SendAccountVerificationEmailCommand
{
    [Inject] private HSqlFacade _sqlFacade;
    [Inject] private RequestMetadata _requestMetadata;
    [Inject] private UserAccountManagementService _accountService;
    protected override async Task VoidExecute(VoidCommandPayload request)
    {
        UserInformationVO userInformationVo = await _sqlFacade.GetById<UserInformationVO>(UserId);
        await _accountService.SendAccountVerificationEmail(userInformationVo, _requestMetadata.Token.ClientId,
            _requestMetadata.Token.DeviceId);
    }
}