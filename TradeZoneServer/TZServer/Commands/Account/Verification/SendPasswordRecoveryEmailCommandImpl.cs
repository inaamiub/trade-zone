﻿namespace TZServer;

public class SendPasswordRecoveryEmailCommandImpl : SendPasswordRecoveryEmailCommand
{
    [Inject] private UserAccountManagementService _accountService;
    protected override async Task VoidExecute(SendPasswordRecoveryEmailCommandPayload request)
    {
        if (string.IsNullOrEmpty(request.BranchId))
        {
            throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
        }
        if (string.IsNullOrEmpty(request.ClientId))
        {
            throw new HException(HatfErrorCodes.ClientIdNotFound);
        }
        if (string.IsNullOrEmpty(request.DeviceId))
        {
            throw new HException(HatfErrorCodes.EmptyDeviceId);
        }
        await _accountService.SendPasswordRecoveryEmail(request.BranchId, request.Email, request.ClientId, request.DeviceId);
    }
}