﻿using HatfCommand;

namespace TZServer;

public class VerifyEmailCommandImpl : VerifyEmailCommand
{
    [Inject] private HSqlFacade _sqlFacade;
    [Inject] private TzOAuth _oAuth;

    protected override async Task VoidExecute(VerifyEmailCommandPayload request)
    {
        UserInformationVO userInformationVo = await _sqlFacade.GetById<UserInformationVO>(UserId);
        if (string.IsNullOrEmpty(request.Token))
        {
            throw new HException(HatfErrorCodes.EmptyAccessToken);
        }

        await _oAuth.ValidateToken(UserId, request.Token, TokenType.AccountVerificationToken);

        userInformationVo.RecordStatus = UserRecordStatus.Active;
        await _sqlFacade.Update(SqlQueryBuilder<UserInformationVO>
            .Update(UserInformationVOKeys.RecordStatus, userInformationVo.RecordStatus)
            .Equal(UserInformationVOKeys.Id, userInformationVo.Id));
    }
}