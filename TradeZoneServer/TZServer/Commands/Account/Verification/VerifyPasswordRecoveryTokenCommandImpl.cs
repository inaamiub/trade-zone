﻿namespace TZServer;

public class VerifyPasswordRecoveryTokenCommandImpl : VerifyPasswordRecoveryTokenCommand
{
    [Inject] private UserAccountManagementService _accountService;
    [Inject] private TzOAuth _oAuth;
    protected override async Task VoidExecute(VerifyPasswordRecoveryTokenCommandPayload request)
    {
        UserInformationVO user = await _accountService.GetUserInformationByEmail(request.BranchId, request.Email); 

        // Expecting branch id in user name
        await _oAuth.ValidateToken(user.Id, request.Token, TokenType.PasswordRecoveryToken);
    }
}