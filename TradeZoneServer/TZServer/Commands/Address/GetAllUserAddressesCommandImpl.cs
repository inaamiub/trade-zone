﻿using HatfCommand;

namespace TZServer;

public class GetAllUserAddressesCommandImpl : GetAllUserAddressesCommand
{
    [Inject] private UserInformationVO _userVo;
    [Inject] private UserAddressManagementService _addressService;
    [Inject] private OrderManagementService _orderService;
    public override async Task<GetAllUserAddressesCommandResult> Execute(VoidCommandPayload request)
    {
        List<UserAddressVO> userAddresses = await _addressService.GetAllUserAddresses(_userVo);
        List<SalesOrderVO> lastOrders = await _orderService.GetLastOrders(_userVo, 10);
        string lastOrderAddressId = lastOrders.FirstOrDefault(m => string.IsNullOrEmpty(m.AddressId) == false)?.AddressId;
        if (string.IsNullOrEmpty(lastOrderAddressId))
        {
            lastOrderAddressId = userAddresses.FirstOrDefault(m => m.Id == m.UserId)?.Id ?? userAddresses.FirstOrDefault()?.Id;
        }

        return new GetAllUserAddressesCommandResult()
        {
            Addresses = userAddresses,
            LastUsedAddressId = lastOrderAddressId
        };
    }
}