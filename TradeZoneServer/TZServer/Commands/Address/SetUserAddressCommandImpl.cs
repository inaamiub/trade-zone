﻿namespace TZServer;

public class SetUserAddressCommandImpl : SetUserAddressCommand
{
    [Inject] private UserInformationVO _userVo;
    [Inject] private UserAddressManagementService _addressService;
    public override async Task<SetUserAddressCommandPayload> Execute(SetUserAddressCommandPayload request)
    {
        SetUserAddressCommandPayload result = new SetUserAddressCommandPayload()
        {
            IsNew = request.IsNew
        };
        if (request.IsNew)
        {
            result.Address = await _addressService.AddNewAddress(request.Address, _userVo);
        }
        else
        {
            result.Address = await _addressService.UpdateUserAddress(request.Address, _userVo);
        }

        return result;
    }
}