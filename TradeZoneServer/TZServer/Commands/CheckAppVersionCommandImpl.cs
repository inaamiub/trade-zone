﻿using HatfCommand;

namespace TZServer;

public class CheckAppVersionCommandImpl : CheckAppVersionCommand
{
    [Inject] public TZRegistry _registry;
    public override Task<CheckAppVersionCommandResult> Execute(CheckAppVersionCommandPayload request)
    {
        int dbVersion = _registry.GetCurrentAndroidAppVersion(request.BranchId);
        CheckAppVersionCommandResult response = new CheckAppVersionCommandResult
        {
            UpdateAvailable = false
        };
            
        // Check if update available and forced
        if (dbVersion > request.AppVersion)
        {
            response.UpdateAvailable = true;
            response.Forced = _registry.CheckIfAndroidUpdateForced(request.BranchId);
        }

        if (request.LastAppVersion < 12)
        {
            response.ResetUserToken = true;
        }

        return Task.FromResult(response);
    }
}