﻿namespace TZServer;

public class CreateNewOrderCommandImpl : CreateNewOrderCommand
{
    [Inject] private OrderManagementService _orderService;
    [Inject] private UserInformationVO _userVo;
    [Inject] private RequestMetadata _requestMetadata;
    public override async Task<GetUserOrderHistoryCommandResult> Execute(CreateNewOrderCommandPayload request)
    {
        OrderType orderType = _requestMetadata.Token.Platform switch
        {
            Platform.Android => OrderType.A,
            Platform.Web => OrderType.W,
            _ => throw new HException(TZErrorCodes.PLATFORM_NOT_HANDLED)
        };
        await _orderService.CreateNewOrder(_userVo, request, orderType);
        List<SalesOrderVO> last10Orders = await _orderService.GetLastOrders(_userVo);
        return new GetUserOrderHistoryCommandResult()
        {
            Orders = last10Orders
        };
    }
}