﻿using HatfCommand;

namespace TZServer;

public class GetProductsCommandImpl : GetProductsCommand
{
    [Inject] private OrderManagementService _orderService;
    [Inject] private TzRequestMetadata _requestMetadata;
    public override async Task<GetProductsCommandResult> Execute(VoidCommandPayload request)
    {
        List<ProductModel> products = await _orderService.GetProducts(_requestMetadata.BranchId);

        return new GetProductsCommandResult()
        {
            Products = products
        };
    }
}