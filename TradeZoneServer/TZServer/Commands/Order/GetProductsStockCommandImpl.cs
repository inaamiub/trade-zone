﻿using HatfCommand;

namespace TZServer;

public class GetProductsStockCommandImpl : GetProductsStockCommand
{
    [Inject] private OrderManagementService _orderService;
    [Inject] private TzRequestMetadata _requestMetadata;
    [Inject] private UserInformationVO _userVo;
    public override async Task<GetProductsStockCommandResult> Execute(VoidCommandPayload request)
    {
        List<ProductStockModel> products = await _orderService.GetProductStock(_requestMetadata.BranchId, _userVo.AccountNo);

        return new GetProductsStockCommandResult()
        {
            Products = products
        };
    }
}