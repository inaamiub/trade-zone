﻿using HatfCommand;

namespace TZServer;

public class GetStockReportCommandImpl : GetStockReportCommand
{
    [Inject] private OrderManagementService _orderService;
    [Inject] private RequestMetadata _requestMetadata;
    [Inject] private UserInformationVO _userVo;
    public override async Task<GetStockReportCommandResult> Execute(VoidCommandPayload request)
    {
        List<StockReportModel> stockReport = await _orderService.GenerateStockReport(_userVo.BranchId, _userVo.AccountNo);
        return new GetStockReportCommandResult()
        {
            ReportData = stockReport
        };
    }
}