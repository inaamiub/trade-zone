﻿using HatfCommand;

namespace TZServer;

public class GetUserOrderHistoryCommandImpl : GetUserOrderHistoryCommand
{
    [Inject] private OrderManagementService _orderService;
    [Inject] private UserInformationVO _userVo;
    public override async Task<GetUserOrderHistoryCommandResult> Execute(VoidCommandPayload request)
    {
        List<SalesOrderVO> last10Orders = await _orderService.GetLastOrders(_userVo);

        return new GetUserOrderHistoryCommandResult()
        {
            Orders = last10Orders
        };
    }
}