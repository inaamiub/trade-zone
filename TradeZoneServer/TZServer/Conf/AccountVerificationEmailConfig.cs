﻿namespace TZServer
{
    public class AccountVerificationEmailConfig : HBaseConfig, ISmtpConfig
    {
        public SmtpConfig SmtpConfig { get; set; }
        public SmtpConfig GetSmtpConfig()
        {
            return SmtpConfig;
        }
    }
}