﻿namespace TZServer
{
    public class OAuthConfig : HBaseConfig
    {
        /// <summary>
        /// Access token life time in Seconds
        /// </summary>
        public long AccessTokenLifeTime { get; set; }

        /// <summary>
        /// Refresh token life time in Seconds
        /// </summary>
        public long RefreshTokenLifeTime { get; set; }
        
        /// <summary>
        /// Account verification token life time
        /// </summary>
        public long AccountVerificationTokenLifeTime { get; set; }
        
        /// <summary>
        /// Account verification token life time
        /// </summary>
        public long PasswordRecoveryTokenLifeTime { get; set; }

        /// <summary>
        /// Authorization key to be used to find the value of token
        /// </summary>
        public string AuthorizationKey { get; set; }
        
        /// <summary>
        /// Whether to authenticate auth client or not
        /// </summary>
        public bool AuthenticateClient { get; set; }
        
        /// <summary>
        /// Flag to sue client grants or not
        /// </summary>
        public bool UseClientGrants { get; set; }
    }
}