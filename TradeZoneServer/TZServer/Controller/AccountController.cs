﻿// using Microsoft.AspNetCore.Mvc;
//
// namespace TZServer;
//
// [Route(RouteConstants.API + "/" + TzRouteConstants.ACCOUNT_CONTROLLER)]
// public class AccountApiController : TzBaseApiController
// {
//     [Inject] public DiContainer _container;
//     [Inject] public OAuthResponse _oauthResponse;
//     [Inject] public RequestModel _requestData;
//
//     [HttpPost(TzRouteConstants.ACCOUNT_LOGIN)]
//     [Token(TokenType.AccessToken)]
//     public async Task<object> Login()
//     {
//         return SendSuccess("", await GetLoginResponse());
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_REFRESH_TOKEN)]
//     [Token(TokenType.RefreshToken)]
//     public async Task<object> RefreshToken()
//     {
//         return SendSuccess("", await GetLoginResponse());
//     }
//
//     private async Task<mdLoginResponse> GetLoginResponse()
//     {
//         var userInformationVo = _container.Resolve<UserInformationVO>();
//         userInformationVo.UserAddressVO = await _container.Resolve<HSqlFacade>()
//             .Get(SqlQueryBuilder<UserAddressVO>.Where(UserAddressesVOKeys.Id, userInformationVo.Id));
//         var response = new mdLoginResponse
//         {
//             AccessToken = _oauthResponse.AccessToken,
//             RefreshToken = _oauthResponse.RefreshToken,
//             User = userInformationVo
//         };
//
//         response.User.Password = "";
//         if (response.User.UserType == UserType.AsGuest)
//         {
//             response.User.UserName = "";
//         }
//
//         return response;
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_SIGNUP)]
//     public async Task<object> Signup()
//     {
//         var userInformation = _requestData.GetRequestData<UserInformationVO>();
//
//         // Signup Means full access
//         // var oAuthResponse = await UserAccountManagementService.Create(_container)
//         //     .Signup(userInformation, _requestData.ClientId, _requestData.DeviceId);
//         // RequestContainer.BindInstance(oAuthResponse);
//         // await BindUser(oAuthResponse);
//
//         return SendSuccess("", await GetLoginResponse());
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_SEND_ACCOUNT_VERIFICATION_TOKEN)]
//     [Authenticate]
//     public async Task<object> SendAccountVerificationToken()
//     {
//         var userInformationVo = _container.Resolve<UserInformationVO>();
//
//         await UserAccountManagementService.Create(_container)
//             .SendAccountVerificationEmail(userInformationVo, _requestData.ClientId, _requestData.DeviceId);
//
//         return SendSuccess();
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_VERIFY_EMAIL)]
//     [Authenticate]
//     public async Task<object> VerifyEmail()
//     {
//         var userInformationVo = _container.Resolve<UserInformationVO>();
//         var verificationToken = _requestData.GetRequestData<string>();
//
//         var tokenVerificationResponse =
//             await CheckVerificationToken(userInformationVo.BranchId, userInformationVo.Id, verificationToken,
//                 TokenType.AccountVerificationToken);
//
//         if (tokenVerificationResponse.UserId != userInformationVo.Id)
//         {
//             throw new HException(TZErrorCodes.TokenNotFound);
//         }
//
//         userInformationVo.RecordStatus = UserRecordStatus.Active;
//
//         await _container.Resolve<HSqlFacade>().Update(SqlQueryBuilder<UserInformationVO>
//             .Update(UserInformationVOKeys.RecordStatus, userInformationVo.RecordStatus)
//             .Equal(UserInformationVOKeys.Id, userInformationVo.Id));
//
//         return SendSuccess();
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_UPDATE_ACCOUNT_INFO)]
//     [Authenticate]
//     public async Task<object> UpdateAccountInfo()
//     {
//         var userInformationVo = _container.Resolve<UserInformationVO>();
//         var accountUpdateData = _requestData.GetRequestData<mdUserInformation>();
//
//         await UserAccountManagementService.Create(_container)
//             .UpdateAccountInfo(userInformationVo, accountUpdateData);
//
//         return SendSuccess();
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_UPDATE_BASIC_USER_INFO)]
//     [Authenticate]
//     public async Task<object> UpdateBasicUserInfo()
//     {
//         UserInformationVO userInformation = _requestData.GetRequestData<UserInformationVO>();
//
//         // Signup Means full access
//         await UserAccountManagementService.Create(_container).UpdateBasicUserInfo(userInformation);
//
//         return SendSuccess("", await GetLoginResponse());
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_GET_USER_INFORMATION)]
//     [Authenticate]
//     public async Task<object> GetUserInformation()
//     {
//         return SendSuccess("", await GetLoginResponse());
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_VERIFY_TOKEN_BY_EMAIL)]
//     public async Task<object> VerifyToken()
//     {
//         var request = _requestData.GetRequestData<RequestModel>();
//         // Verify the email of user information vo and the one in request data
//         if (TZAuthHelper.ExtractBranchIdAndUserName(_requestData.UserName, out string branchId, out string email) ==
//             false)
//         {
//             throw new HException(TZErrorCodes.InvalidEmail);
//         }
//
//         var uams = UserAccountManagementService.Create(_container);
//         var userInformationVO = await uams.GetUserInformationByEmail(branchId, email); 
//
//         // Expecting branch id in user name
//         await CheckVerificationToken(branchId, userInformationVO.Id, request.Token, request.TokenType);
//
//         return SendSuccess();
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_SEND_PASSWORD_RECOVERY_TOKEN)]
//     public async Task<object> SendPasswordRecoveryToken()
//     {
//         if (TZAuthHelper.ExtractBranchIdAndUserName(_requestData.UserName, out string branchId, out string email) ==
//             false)
//         {
//             throw new HException(TZErrorCodes.InvalidEmail);
//         }
//
//         await UserAccountManagementService.Create(_container)
//             .SendPasswordRecoveryEmail(branchId, email, _requestData.ClientId, _requestData.DeviceId);
//
//         return SendSuccess();
//     }
//
//     [HttpPost(TzRouteConstants.ACCOUNT_RECOVER_PASSWORD)]
//     public async Task<object> RecoverPassword()
//     {
//         // Verify the email of user information vo and the one in request data
//         if (TZAuthHelper.ExtractBranchIdAndUserName(_requestData.UserName, out string branchId, out string email) ==
//             false)
//         {
//             throw new HException(TZErrorCodes.InvalidEmail);
//         }
//
//         var uams = UserAccountManagementService.Create(_container);
//         var userInformationVO = await uams.GetUserInformationByEmail(branchId, email); 
//         
//         // Verify token first
//         var oAuthResponse = await CheckVerificationToken(branchId, userInformationVO.Id, _requestData.Token, TokenType.PasswordRecoveryToken);
//
//         // Get user information vo
//         var userInformationVo = (UserInformationVO)oAuthResponse.User;
//
//         if (userInformationVo.BranchId != branchId)
//         {
//             throw new HException(HatfErrorCodes.TokenNotFound);
//         }
//
//         if (userInformationVo.Email != email)
//         {
//             throw new HException(HatfErrorCodes.TokenNotFound);
//         }
//
//         // Update password
//         var accountUpdateData = _requestData.GetRequestData<mdUserInformation>();
//         // Make sure that you don't update user name
//         accountUpdateData.NewUserName = string.Empty;
//
//         await uams.UpdateAccountInfo(userInformationVo, accountUpdateData, false);
//
//         return SendSuccess();
//     }
//
//     private async Task<OAuthResponse>  CheckVerificationToken(string branchId, string userId, string token, TokenType tokenType)
//     {
//         RequestModel requestModel = new RequestModel
//         {
//             Token = token,
//             TokenType = tokenType,
//             UserId = userId
//         };
//
//         try
//         {
//             var oAuthResponse = await OAuth.OAuth.AuthenticateUser(_container.Resolve<IOAuth>(), requestModel);
//             var user = (UserInformationVO)oAuthResponse.User;
//             if (user.BranchId != branchId)
//             {
//                 throw new HException(HatfErrorCodes.VerificationCodeNotMatch);
//             }
//
//             return oAuthResponse;
//         }
//         catch (HException e)
//         {
//             switch (e.ErrorCode)
//             {
//                 case HatfErrorCodes.TokenExpired:
//                     throw new HException(HatfErrorCodes.VerificationCodeExpired);
//
//                 case HatfErrorCodes.TokenNotFound:
//
//                     throw new HException(HatfErrorCodes.VerificationCodeNotMatch);
//                 default:
//                     throw;
//             }
//         }
//     }
//
// }
