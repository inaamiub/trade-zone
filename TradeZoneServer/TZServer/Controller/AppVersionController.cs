﻿using Microsoft.AspNetCore.Mvc;

namespace TZServer
{
    [Route(RouteConstants.API + "/" + TzRouteConstants.APP_VERSION_CONTROLLER)]
    public static class AppVersionApiController
    {
        public static void UseAppVersionApi(this WebApplication app)
        {
            app.MapPost(
                RouteConstants.API + "/" + TzRouteConstants.APP_VERSION_CONTROLLER +
                TzRouteConstants.APP_VERSION_ANDROID_NEW, AndroidNew);
            app.MapGet(
                RouteConstants.API + "/" + TzRouteConstants.APP_VERSION_CONTROLLER +
                TzRouteConstants.APP_VERSION_ANDROID, Android);
        }

        private static IResult AndroidNew()
        {
            return Results.Ok(new mdVersionCheckResponse()
            {
                Forced = true,
                UpdateAvailable = true
            });
        }
        
        private static IResult Android(string branch, string version)
        {
            return Results.Ok(new mdVersionCheckResponse()
            {
                Forced = true,
                UpdateAvailable = true
            });
        }
    }
}