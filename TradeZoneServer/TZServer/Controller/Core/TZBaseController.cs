﻿namespace TZServer
{
    public abstract class TzBaseApiController : HatfApiController
    {
        [Inject] public DiContainer _container;
        
        public const string ACCESS_TOKEN_ID = "ACCESS_TOKEN";
        public const string USER_ID = "USER_ID";
        public const string BRANCH_ID = "BRANCH_ID";
        
        public override async Task BindUser(OAuthResponse oAuthResponse)
        {
            if (oAuthResponse?.User == null)
            {
                return;
            }
            
            UserInformationVO userInformationVO = (UserInformationVO) oAuthResponse.User;
            
            RequestContainer.BindInstance(userInformationVO);
            RequestContainer.BindInstanceWithId(oAuthResponse.AccessToken, ACCESS_TOKEN_ID);
            RequestContainer.BindInstanceWithId(userInformationVO.Id, USER_ID);
            RequestContainer.BindInstanceWithId(userInformationVO.BranchId, BRANCH_ID);
            
            // Inject dependencies in current controller again
            RequestContainer.Inject(this);
        }

        public override async Task PreExecutionValidation()
        {
            string branchId = RequestContainer.TryResolveId<string>(BRANCH_ID);
            if(string.IsNullOrEmpty(branchId))
            {
                return;
            }

            HLogger.Debug("Found branch id {0} from request. Doing pre request validation", branchId, HLogTag.Request);
            SyncAllowedBranches syncAllowedBranches = RequestContainer.Resolve<SyncAllowedBranches>();
            // Verify that branch is allowed
            bool sab = await syncAllowedBranches.BranchAllowed(branchId);
            if (sab == false)
            {
                throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
            }
        }
        
        protected override void RequestClosure(ActionExecutedContext context)
        {
            // Do anything you need to do for closure of each request like log
            RequestContainer.Resolve<HSqlFacade>().ClearChangeList();
        }

    }
}