﻿// using Microsoft.AspNetCore.Mvc;
//
// namespace TZServer
// {
//     [Route(RouteConstants.API + "/" + TzRouteConstants.ORDER_CONTROLLER)]
//     public class OrderApiController : TzBaseApiController
//     {
//         [Inject] public RequestModel _requestData;
//         
//         [HttpPost(TzRouteConstants.ORDER_GET_PRODUCTS_WITH_STOCK)]
//         [Authenticate]
//         public async Task<object> StockReport()
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             var stockReport = await OrderManagementService.Create(_container)
//                 .GenerateStockReport(userInformationVo.BranchId, userInformationVo.AccountNo);
//
//             return SendSuccess(extras: new mdProductsWithStock {ReportData = stockReport});
//         }
//
//         [HttpPost(TzRouteConstants.ORDER_GET_PRODUCTS)]
//         [Authenticate]
//         public async Task<object> GetProducts()
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             var products = await OrderManagementService.Create(_container).GetProducts(userInformationVo.BranchId);
//
//             return SendSuccess(extras: new mdProducts() {Products = products});
//         }
//
//         [HttpPost(TzRouteConstants.ORDER_GET_PRODUCT_STOCK)]
//         [Authenticate]
//         public async Task<object> GetProductStock()
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             var products = await OrderManagementService.Create(_container).GetProductStock(userInformationVo.BranchId, userInformationVo.AccountNo);
//
//             return SendSuccess(extras: new mdProductStock() {ProductStock = products});
//         }
//
//         [HttpPost(TzRouteConstants.ORDER_NEW_ANDROID_ORDER)]
//         [Authenticate]
//         public Task<object> AndroidNewOrder()
//         {
//             return CreateNewOrder(OrderType.A);
//         }
//
//         [HttpPost(TzRouteConstants.ORDER_NEW_WEB_ORDER)]
//         [Authenticate]
//         public Task<object> WebNewOrder()
//         {
//             return CreateNewOrder(OrderType.W);
//         }
//
//         private async Task<object> CreateNewOrder(OrderType orderType)
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             var newOrderData = _requestData.GetRequestData<mdNewOrder>();
//             var orderManagementService = OrderManagementService.Create(_container);
//             // await orderManagementService.CreateNewOrder(userInformationVo, newOrderData, orderType);
//
//             var last10Orders = await orderManagementService.GetLastOrders(userInformationVo);
//
//             return SendSuccess(extras: new mdOrderHistory() {Orders = last10Orders});
//         }
//         
//
//         [HttpPost(TzRouteConstants.ORDER_HISTORY)]
//         [Authenticate]
//         public async Task<object> OrderHistory()
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             
//             var orderManagementService = OrderManagementService.Create(_container);
//             var last10Orders = await orderManagementService.GetLastOrders(userInformationVo);
//             
//             return SendSuccess(extras: new mdOrderHistory() {Orders = last10Orders});
//         }
//
//     }
// }