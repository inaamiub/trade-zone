﻿namespace TZServer;

public static class TzTestController
{
    public static void UseTzTestEndpoints(this WebApplication app)
    {
        app.MapGet("/send-test-email", SendTestEmail);
    }

    private static async Task SendTestEmail(HttpContext context, string email)
    {
        await new PasswordRecoveryEmail().Send("TEST", Guid.NewGuid().ToString("N"),
            TimeSpan.FromSeconds(HConfigManager.Get<OAuthConfig>().PasswordRecoveryTokenLifeTime)
                .TotalMinutes + " minutes", email);
    }
}