﻿// using Microsoft.AspNetCore.Mvc;
//
// namespace TZServer
// {
//     [Route(RouteConstants.API + "/" + TzRouteConstants.USER_ADDRESS_CONTROLLER)]
//     public class UserAddressController : TzBaseApiController
//     {
//         [Inject] public RequestModel _requestData;
//         
//         [HttpPost(TzRouteConstants.GET_ALL_USER_ADDRESSES)]
//         [Authenticate]
//         public async Task<object> GetAllUserAddresses()
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             var userAddresses =
//                 await UserAddressManagementService.Create(_container).GetAllUserAddresses(userInformationVo);
//
//             var lastOrders = await OrderManagementService.Create(_container).GetLastOrders(userInformationVo, 10);
//             string lastOrderAddressId = userInformationVo.Id;
//             var lastOrdersWithAddress = lastOrders.Where(m => string.IsNullOrEmpty(m.AddressId) == false).ToList();
//             if (lastOrdersWithAddress.Count > 0)
//             {
//                 lastOrderAddressId = lastOrders.First().AddressId;
//             }
//
//             return SendSuccess(extras: new mdUserAddresses {Addresses = userAddresses, LastUsedAddressId = lastOrderAddressId});
//         }
//         
//         [HttpPost(TzRouteConstants.ADD_NEW_ADDRESS)]
//         [Authenticate]
//         public async Task<object> AddNewAddresses()
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             var addressVO = _requestData.GetRequestData<UserAddressVO>();
//             addressVO =
//                 await UserAddressManagementService.Create(_container).AddNewAddress(addressVO, userInformationVo);
//
//             return SendSuccess(extras: new mdUserAddresses {Addresses = new List<UserAddressVO>(){addressVO}});
//         }
//
//         [HttpPost(TzRouteConstants.UPDATE_ADDRESS)]
//         [Authenticate]
//         public async Task<object> UpdateAddress()
//         {
//             UserInformationVO userInformationVo = _container.Resolve<UserInformationVO>();
//             var addressVO = _requestData.GetRequestData<UserAddressVO>();
//             addressVO =
//                 await UserAddressManagementService.Create(_container).UpdateUserAddress(addressVO, userInformationVo);
//
//             return SendSuccess(extras: new mdUserAddresses {Addresses = new List<UserAddressVO>(){addressVO}});
//         }
//
//     }
// }