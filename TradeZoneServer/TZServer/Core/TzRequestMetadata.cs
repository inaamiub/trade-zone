namespace TZServer;

public class TzRequestMetadata : RequestMetadata
{
    public TzRequestMetadata(string requestId) : base(requestId)
    {
    }
    
    public string BranchId
    {
        get
        {
            if (Token is TzTokenData tzTokenData)
            {
                return tzTokenData.BranchId;
            }

            return string.Empty;
        }
    }
}