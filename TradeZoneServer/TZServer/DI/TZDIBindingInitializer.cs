﻿namespace TZServer
{
    public class TZDIBindingInitializer : IDiBindingInitializer
    {
        public void InitializeRootContainer(DiContainer container)
        {
        }

        public void InitializeRequestContainer(DiContainer container)
        {
            container.Bind<IOAuth>().To<TzOAuth>().AsSingle();
            container.Bind<UserAccountManagementService>().AsSingle();
            TzRequestMetadata requestMetadata = new TzRequestMetadata(container.Resolve<string>(HConst.RequestId)); 
            container.BindInstance(requestMetadata);
            container.BindInstance(typeof(RequestMetadata), requestMetadata);
        }

        public void InitializeUserContainer(DiContainer container)
        {
            string userId = container.Resolve<RequestMetadata>().UserId;
            if (string.IsNullOrEmpty(userId))
            {
                throw new Exception("User id not found");
            }
            container.Bind<UserInformationVO>().FromMethodUntypedAsync(ResolveUserVO);
            container.Bind<OrderManagementService>().AsSingle();
            container.Bind<UserAddressManagementService>().AsSingle();
        }

        private async Task<object> ResolveUserVO(DiContainer container, Type type)
        {
            string userId = container.Resolve<RequestMetadata>().UserId;
            HSqlFacade sqlFacade = container.Resolve<HSqlFacade>();
            UserInformationVO userInformationVo = await sqlFacade.GetById<UserInformationVO>(userId);
            return userInformationVo;
        }
    }
}