﻿namespace TZServer
{
    public class AccountVerificationEmail : EmailBase
    {
        private const string COMPANY_NAME = "COMPANY_NAME";
        private const string PASSCODE = "PASSCODE";
        private const string PASSCODE_VALIDITY = "PASSCODE_VALIDITY";
        
        public Task Send(string companyName, string passcode, string passcodeValidity, string recipient)
        {
            Dictionary<string, string> emailParams = new Dictionary<string, string>
            {
                {COMPANY_NAME, companyName},
                {PASSCODE, passcode},
                {PASSCODE_VALIDITY, passcodeValidity}
            };

            string content = EmailDictionary.Get(this.GetType());
            foreach (KeyValuePair<string, string> item in emailParams)
            {
                content = content.Replace("{" + item.Key + "}", item.Value);
            }

            return Send(HConfigManager.Get<AccountVerificationEmailConfig>().SmtpConfig, recipient, content);
        }
    }
}