﻿namespace TZServer
{
    public class PasswordRecoveryEmail : EmailBase
    {
        private const string COMPANY_NAME = "COMPANY_NAME";
        private const string PASSCODE = "PASSCODE";
        private const string PASSCODE_VALIDITY = "PASSCODE_VALIDITY";
        
        public Task Send(string companyName, string passcode, string passcodeValidity, string recipient)
        {
            Dictionary<string, string> emailParams = new Dictionary<string, string>
            {
                {COMPANY_NAME, companyName},
                {PASSCODE, passcode},
                {PASSCODE_VALIDITY, passcodeValidity}
            };

            string content = EmailDictionary.Get(this.GetType());
            foreach ((string key, string value) in emailParams)
            {
                content = content.Replace("{" + key + "}", value);
            }

            return Send(HConfigManager.Get<AccountVerificationEmailConfig>().SmtpConfig, recipient, content);
        }
    }
}