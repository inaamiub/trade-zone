﻿// using NLog;
// using NLog.LayoutRenderers;
// using NLog.Targets;
//
// namespace TZServer
// {
//     public class DbLogAppender : ILogAppender
//     {
//         public Target GetTarget(HatfLogConfig hatfLogConfig)
//         {
//             if (hatfLogConfig.EnableDbLog == false)
//             {
//                 return null;
//             }
//             LayoutRenderer.Register("token", info => string.IsNullOrEmpty(HLogger.LogData.Value?.Token) ? "-" : HLogger.LogData.Value?.Token);
//             LayoutRenderer.Register("version", info => (HLogger.LogData.Value?.ClientVersion ?? string.Empty) == string.Empty ? "-" : HLogger.LogData.Value.ClientVersion.ToString());
//             LayoutRenderer.Register("request-id", info => string.IsNullOrEmpty(HLogger.LogData.Value?.RequestId) ? "-" : HLogger.LogData.Value.RequestId);
//             LayoutRenderer.Register("ip", info => string.IsNullOrEmpty(HLogger.LogData.Value?.IpAddress) ? "-" : HLogger.LogData.Value.IpAddress);
//             LayoutRenderer.Register("parsed-message", info => string.IsNullOrEmpty(HLogger.LogData.Value?.ParsedMessage) ? "-" : HLogger.LogData.Value.ParsedMessage);
//             
//             string commandText = hatfLogConfig.CommandText;
//             if (string.IsNullOrEmpty(commandText))
//             {
//                 commandText = "insert into " + hatfLogConfig.DbTableName +
//                               @" (TimeStamp, Level, Message, Exception, RequestId, IpAddress, Version, Token) 
//                                   values (@timestamp, @level, @parsedMessage, @exception, @requestId, @ip, @version, @token);";
//             }
//             
//             // Database log target
//             var logDb = new NLog.Targets.DatabaseTarget()
//             {
//                 ConnectionString = hatfLogConfig.DbConnectionString,
//                 KeepConnection = true,
//                 CommandText = commandText,
//                 Parameters =
//                 {
//                     new DatabaseParameterInfo("@timestamp", "${date}"),
//                     new DatabaseParameterInfo("@level", "${level}"),
//                     new DatabaseParameterInfo("@parsedMessage", "${parsed-message}"),
//                     new DatabaseParameterInfo("@logger", "${logger}"),
//                     new DatabaseParameterInfo("@exception", "${exception:tostring}"),
//                     new DatabaseParameterInfo("@token", "${token}"),
//                     new DatabaseParameterInfo("@version", "${version:tostring}"),
//                     new DatabaseParameterInfo("@requestId", "${request-id}"),
//                     new DatabaseParameterInfo("@ip", "${ip}"),
//                 }
//             };
//
//             return logDb;
//         }
//         
//         public LogLevel GetLogLevel(HatfLogConfig hatfLogConfig)
//         {
//             return LogLevel.FromString(hatfLogConfig.DbLogLevel);
//         }
//     }
// }