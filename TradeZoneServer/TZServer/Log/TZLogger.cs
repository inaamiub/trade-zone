﻿using System.Text;

namespace TZServer
{
    public class TZLogger
    {
        public static HLogData InitLogData(string requestId)
        {
            return new TZLogData(requestId)
            {
                RequestId = requestId
            };
        }

        public static string FinalizeLogMessage(string message, HLogData logData)
        {
            if (logData is TZLogData tzLogData)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("RID ");
                stringBuilder.Append(tzLogData.RequestId);
                stringBuilder.Append(" IP ");
                stringBuilder.Append(tzLogData.IpAddress);
                stringBuilder.Append(" VR ");
                stringBuilder.Append(tzLogData.ClientVersion);
                stringBuilder.Append(" TKN ");
                string token = string.IsNullOrEmpty(tzLogData.Token) ? "_" : tzLogData.Token;
                stringBuilder.Append(token);
                stringBuilder.Append(" MSG ");
                stringBuilder.Append(message);
                
                logData.ParsedMessage = message;
                
                return stringBuilder.ToString();   
            }

            return message;
        }
    }

    public class TZLogData : HLogData
    {
        public TZLogData(string requestId) : base(requestId)
        {
        }
    }
}