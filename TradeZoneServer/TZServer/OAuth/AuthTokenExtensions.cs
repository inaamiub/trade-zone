﻿namespace TZServer;

public static class AuthTokenExtensions
{
    public static AuthRequest? ToAuthRequest(this LoginUserCommandPayload? payload)
    {
        if (payload == null)
        {
            return null;
        }

        AuthRequest authRequest = ((LoginGuestUserCommandPayload)payload).ToAuthRequest();
        if (authRequest == null)
        {
            return null;
        }
        authRequest.UserName = payload.UserName;
        authRequest.Password = payload.Password;
        return authRequest;
    }
    
    public static AuthRequest? ToAuthRequest(this LoginGuestUserCommandPayload? payload)
    {
        if (payload == null)
        {
            return null;
        }

        return new AuthRequest()
        {
            DeviceId = payload.DeviceId,
            ClientId = payload.ClientId,
            Platform = payload.Platform,
            AdditionalData = payload.BranchId,
            AppVersion = payload.AppVersion,
            ClientSecret = payload.ClientSecret,
            LangKey = payload.LangKey
        };
    }
    
    public static AuthToken ToAuthToken(this AuthTokenVO authToken)
    {
        return new AuthToken()
        {
            Expiry = authToken.Expiry.FromEpochSeconds(),
            Id = authToken.Id,
            Timestamp = authToken.Timestamp.FromEpochSeconds(),
            Token = authToken.Token,
            ClientId = authToken.ClientId,
            DeviceId = authToken.DeviceId,
            TokenStatus = authToken.RecordStatus,
            TokenType = authToken.TokenType,
            UserId = authToken.UserId
        };
    }
    
    public static LoginUserCommandResult ToLoginResult(this AuthResult authResult, DiContainer container, IJwtTokenData jwtData)
    {
        LoginUserCommandResult result = new LoginUserCommandResult()
        {
            User = (UserInformationVO)authResult.User
        };
        TzJwtTokenValidator tokenGenerator = container.Resolve<TzJwtTokenValidator>();
        if (authResult.AccessToken != null)
        {
            TzTokenData tokenData = new TzTokenData()
            {
                BranchId = jwtData.BranchId,
                DeviceId = authResult.AccessToken.DeviceId,
                Platform = jwtData.Platform,
                ClientType = authResult.AccessToken.ClientType,
                ClientVersion = jwtData.AppVersion,
                ClientId = jwtData.ClientId,
                SessionKey = authResult.AccessToken.Token,
                IssuedAt = authResult.AccessToken.Timestamp,
                ValidUntil = authResult.AccessToken.Expiry
            };
            result.AccessToken = tokenGenerator.GenerateToken(result.User.Id, tokenData);
        }

        if (authResult.RefreshToken != null)
        {
            TzTokenData tokenData = new TzTokenData()
            {
                BranchId = jwtData.BranchId,
                DeviceId = authResult.RefreshToken.DeviceId,
                Platform = jwtData.Platform,
                ClientType = authResult.RefreshToken.ClientType,
                ClientVersion = jwtData.AppVersion,
                ClientId = jwtData.ClientId,
                SessionKey = authResult.RefreshToken.Token,
                IssuedAt = authResult.RefreshToken.Timestamp,
                ValidUntil = authResult.RefreshToken.Expiry
            };
            result.RefreshToken = tokenGenerator.GenerateToken(result.User.Id, tokenData);
        }

        return result;
    }
}