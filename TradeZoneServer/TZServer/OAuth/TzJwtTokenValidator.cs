﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace TZServer;

public class TzJwtTokenValidator : HJwtTokenValidator<TzTokenData>
{
    public const string BRANCH_ID_CLAIM_KEY = "bid";
    public override Task Init()
    {
        return Task.CompletedTask;
    }

    protected override void SetPayload(JwtPayload payload, string id, TzTokenData tokenData)
    {
        base.SetPayload(payload, id, tokenData);
        if (string.IsNullOrEmpty(tokenData.BranchId) == false)
        {
            payload.Add(BRANCH_ID_CLAIM_KEY, tokenData.BranchId);
        }
    }

    protected override string GetSigningKey(TzTokenData tokenData)
    {
        return GetSigningKey(tokenData.BranchId, tokenData.ClientType);
    }

    private string GetSigningKey(string branchId, ClientType clientType)
    {
        TZRegistry registry = HContainerFactory.RootContainer.Resolve<TZRegistry>();
        string signingKey = registry.GetSigningKey(branchId, clientType);
        if (string.IsNullOrEmpty(signingKey))
        {
            HLogger.Error("Found empty signing key for branch {0} and client type {1}", branchId, clientType);
            throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);
        }

        return signingKey;
    }

    protected override string GetSigningKeyFromToken(JwtSecurityToken token)
    {
        string clientTypeInt = this.GetRequiredClaim<string>(token.Payload, CLIENT_TYPE_CLAIM_KEY);
        Enum.TryParse(clientTypeInt, out ClientType clientType);
        if (clientType == ClientType.None)
        {
            HLogger.Error("Got none client type from int {0}", clientTypeInt);
            throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);
        }

        string branchId = this.GetRequiredClaim<string>(token.Payload, BRANCH_ID_CLAIM_KEY);
        if (string.IsNullOrEmpty(branchId))
        {
            HLogger.Error("Branch id is empty from token");
            throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);
        }

        return GetSigningKey(branchId, clientType);
    }

    public override TzTokenData? ExtractTokenData(IEnumerable<Claim>? claims)
    {
        TzTokenData? tokenData = base.ExtractTokenData(claims);
        if (tokenData == null)
        {
            return null;
        }
        Claim? branchIdClaim = claims.FindFirst(BRANCH_ID_CLAIM_KEY);
        if (branchIdClaim != null)
        {
            tokenData.BranchId = branchIdClaim.Value;
        }
        return tokenData;
    }
}