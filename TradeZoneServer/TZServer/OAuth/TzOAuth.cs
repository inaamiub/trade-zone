﻿namespace TZServer;

public class TzOAuth : IOAuth
{
    [Inject] private HSqlFacade _sqlFacade;
    [Inject] private OAuthConfig _oAuthConfig;

    public async Task<AuthClient> GetAuthClient(string clientId)
    {
        AuthClientVO authClient = await _sqlFacade.GetById<AuthClientVO>(clientId);
        return new AuthClient
        {
            Id = authClient.Id,
            Secret = authClient.Secret
        };
    }

    public Task<AuthTokenVO> GetAuthTokenVo(string token, TokenType tokenType)
    {
        return _sqlFacade.Get<AuthTokenVO>(
            SqlQueryBuilder<AuthTokenVO>.Select()
                .Equal(AuthTokenVOKeys.Token, token)
                .Equal(AuthTokenVOKeys.TokenType, tokenType)
                .Equal(AuthTokenVOKeys.RecordStatus, TokenStatus.Active));

    }

    public Task<AuthTokenVO> GetAuthTokenVo(string userId, string token, TokenType tokenType)
    {
        return _sqlFacade.Get<AuthTokenVO>(
            SqlQueryBuilder<AuthTokenVO>.Select()
                .Equal(AuthTokenVOKeys.UserId, userId)
                .Begin()
                .Equal(AuthTokenVOKeys.Token, token)
                .Or()
                .Equal(AuthTokenVOKeys.Code, token)
                .End()
                .Equal(AuthTokenVOKeys.TokenType, tokenType)
                .Equal(AuthTokenVOKeys.RecordStatus, TokenStatus.Active));

    }

    public async Task<AuthToken> GetToken(string token, TokenType tokenType)
    {
        AuthTokenVO authToken = await GetAuthTokenVo(token, tokenType);

        if (authToken == null)
        {
            throw new HException(HatfErrorCodes.TokenNotFound);
        }

        return authToken.ToAuthToken();
    }

    public async Task ValidateToken(string token, TokenType tokenType)
    {
        AuthTokenVO authToken = await GetAuthTokenVo(token, tokenType);

        if (authToken == null)
        {
            throw new HException(HatfErrorCodes.TokenNotFound);
        }

        ValidateToken(authToken.ToAuthToken());
    }

    public async Task<AuthToken> GetToken(string userId, string token, TokenType tokenType)
    {
        AuthTokenVO authToken = await GetAuthTokenVo(userId, token, tokenType);

        if (authToken == null)
        {
            throw new HException(HatfErrorCodes.TokenNotFound);
        }

        return authToken.ToAuthToken();
    }

    public async Task ValidateToken(string userId, string token, TokenType tokenType)
    {
        AuthTokenVO authToken = await GetAuthTokenVo(userId, token, tokenType);

        if (authToken == null)
        {
            throw new HException(HatfErrorCodes.TokenNotFound);
        }

        ValidateToken(authToken.ToAuthToken());
    }

    public void ValidateToken(AuthToken token)
    {
        if (token == null)
        {
            throw new HException(HatfErrorCodes.TokenNotFound);
        }

        if (token.TokenStatus != TokenStatus.Active)
        {
            throw new HException(HatfErrorCodes.TokenExpired);
        }

        if (DateTimeUtils.Now >= token.Expiry)
        {
            throw new HException(HatfErrorCodes.TokenExpired, token.TokenType);
        }
    }

    public async Task<bool> GrantTypeAllowed(string clientId, GrantType grantType)
    {
        if (HConfigManager.Get<OAuthConfig>().UseClientGrants == false)
        {
            return true;
        }

        AuthGrantVO authToken = await _sqlFacade.Get<AuthGrantVO>(
            SqlQueryBuilder<AuthGrantVO>.Select()
                .Equal(AuthGrantVOKeys.ClientId, clientId)
                .Equal(AuthGrantVOKeys.GrantType, grantType));

        return authToken != null;
    }

    /// <summary>
    /// Delete auth token
    /// </summary>
    /// <param name="token"></param>
    /// <param name="tokenType"></param>
    /// <returns></returns>
    public async Task<bool> RevokeToken(string token, TokenType tokenType)
    {
        AuthTokenVO authToken = await GetAuthTokenVo(token, tokenType);
        if (authToken == null)
        {
            return true;
        }

        authToken.RecordStatus = TokenStatus.Deleted;
        await _sqlFacade.Update(SqlQueryBuilder<AuthTokenVO>
            .Update(AuthTokenVOKeys.RecordStatus, authToken.RecordStatus)
            .Equal(AuthTokenVOKeys.Token, authToken.Token));
        return true;
    }

    public async Task<bool> SaveToken(AuthToken authToken, bool deleteOld = true)
    {
        if (deleteOld)
        {
            string query = SqlQueryBuilder<AuthTokenVO>.Update(UserInformationVOKeys.RecordStatus, TokenStatus.Deleted)
                .NotEqual(UserInformationVOKeys.RecordStatus, TokenStatus.Deleted)
                .Equal(AuthTokenVOKeys.TokenType, authToken.TokenType)
                .Equal(AuthTokenVOKeys.UserId, authToken.UserId).ToString();

            // Mark old tokens as deleted
            await _sqlFacade.ExecuteNonQuery(query);
        }

        AuthTokenVO inserted = await _sqlFacade.Insert(new AuthTokenVO
        {
            Expiry = authToken.Expiry.ToEpochInSeconds(),
            Timestamp = authToken.Timestamp.ToEpochInSeconds(),
            Token = authToken.Token,
            Code = authToken.Code,
            ClientId = authToken.ClientId,
            DeviceId = authToken.DeviceId,
            RecordStatus = authToken.TokenStatus,
            TokenType = authToken.TokenType,
            UserId = authToken.UserId
        });

        return string.IsNullOrEmpty(inserted.Id) == false;
    }

    public long GetTokenLifeTime(TokenType tokenType, object user)
    {
        OAuthConfig oAuthConfig = HConfigManager.Get<OAuthConfig>();

        if (user is UserInformationVO == false)
        {
            throw new Exception($"User object should be {nameof(UserInformationVO)}");
        }

        UserInformationVO userInformationVo = (UserInformationVO)user;

        long yearSeconds = 0;
        if (userInformationVo.UserType == UserType.AsGuest)
        {
            yearSeconds = (long)TimeSpan.FromDays(365).TotalSeconds;
        }

        switch (tokenType)
        {
            case TokenType.AccessToken:
                return oAuthConfig.AccessTokenLifeTime + yearSeconds;

            case TokenType.RefreshToken:
                return oAuthConfig.RefreshTokenLifeTime + yearSeconds;

            case TokenType.AccountVerificationToken:
                return oAuthConfig.AccountVerificationTokenLifeTime + yearSeconds;

            case TokenType.PasswordRecoveryToken:
                return oAuthConfig.PasswordRecoveryTokenLifeTime + yearSeconds;

            default:
                throw new Exception($"Wrong token type {tokenType}");
        }
    }

    public Task<AuthClientVO> ValidateAuthClientAndGrant(AuthRequest request, GrantType grantType)
    {
        return ValidateAuthClientAndGrant(request.ClientId, request.ClientSecret, grantType);
    }
    
    public async Task<AuthClientVO> ValidateAuthClientAndGrant(string clientId, string clientSecret, GrantType grantType)
    {
        if (string.IsNullOrEmpty(clientId))
        {
            throw new HException(HatfErrorCodes.ClientIdNotFound);
        }

        if (_oAuthConfig.AuthenticateClient && string.IsNullOrEmpty(clientSecret))
        {
            throw new HException(HatfErrorCodes.ClientSecretNotFound);
        }

        AuthClientVO authClient = await _sqlFacade.GetById<AuthClientVO>(clientId);
        if (authClient == default(AuthClientVO))
        {
            throw new HException(HatfErrorCodes.InvalidClient);
        }

        if (_oAuthConfig.UseClientGrants)
        {
            AuthGrantVO authGrantVo = await _sqlFacade.Get<AuthGrantVO>(
                SqlQueryBuilder<AuthGrantVO>.Select()
                    .Equal(AuthGrantVOKeys.ClientId, clientId)
                    .Equal(AuthGrantVOKeys.GrantType, grantType));
            if (authGrantVo == null)
            {
                throw new HException(HatfErrorCodes.GrantTypeNotAllowed);
            }
        }

        if (_oAuthConfig.AuthenticateClient && clientSecret != authClient.Secret)
        {
            throw new HException(HatfErrorCodes.ClientAuthFailed);
        }

        return authClient;
    }
    
    public async Task<AuthResult> Login(AuthRequest request)
    {
        if (string.IsNullOrEmpty(request.UserName))
        {
            throw new HException(HatfErrorCodes.EmptyUserName);
        }

        if (string.IsNullOrEmpty(request.Password))
        {
            throw new HException(HatfErrorCodes.EmptyPassword);
        }

        if (request.AdditionalData is not string branchId)
        {
            throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
        }

        AuthClientVO authClient = await ValidateAuthClientAndGrant(request, GrantType.Password);
        
        UserInformationVO user = await ValidateUser(request.UserName, request.Password, branchId);
        AuthResult result = await GenerateAuthResult(authClient, user, request.DeviceId);
        return result;
    }

    public async Task<AuthResult> GenerateAuthResult(AuthClientVO authClient, UserInformationVO user, string deviceId, bool genRefreshToken = true)
    {
        AuthResult result = new AuthResult
        {
            User = user,
            AccessToken = await SaveToken(authClient, deviceId, user.Id, TokenType.AccessToken,
                GetTokenLifeTime(TokenType.AccessToken, user), user.UserType != UserType.AsGuest)
        };

        if (genRefreshToken)
        {
            result.RefreshToken = await SaveToken(authClient, deviceId, user.Id,
                TokenType.RefreshToken,
                GetTokenLifeTime(TokenType.RefreshToken, user), user.UserType != UserType.AsGuest);
        }

        return result;
    }
    
    public async Task<UserInformationVO> ValidateUser(string userName, string password, string branchId)
    {
        List<UserInformationVO> users = await _sqlFacade.GetAll<UserInformationVO>(
            SqlQueryBuilder<UserInformationVO>.Select()
                .Equal(UserInformationVOKeys.BranchId, branchId)
                .Begin()
                .Equal(UserInformationVOKeys.UserName, userName)
                .Or().Equal(UserInformationVOKeys.Email, userName)
                .End());

        UserInformationVO user = null;
        if (users.Count > 0)
        {
            // Try to find the user with provided user name because user name will be unique in a branch
            user =
                users.FirstOrDefault(m => m.UserName == userName && string.IsNullOrEmpty(userName) == false);
            if (user == default(UserInformationVO))
            {
                // User not found with user name, let's try email now
                // Try full access account first
                user = users.FirstOrDefault(u => u.UserType == UserType.FullAccess);
                if (user == default(UserInformationVO))
                {
                    // Guest user should login with user name which is already handled
                    // Try public access now
                    user = users.FirstOrDefault(u => u.UserType == UserType.PublicAccess) ?? null;
                }

            }
        }

        // Validate user and throw exception if needed
        if (user == null)
        {
            throw new HException(HatfErrorCodes.UserNotExists);
        }

        if (user.Password != Utils.ComputeMd5Hash(password))
        {
            throw new HException(HatfErrorCodes.UsernamePasswordMissMatch);
        }

        if (user.RecordStatus == UserRecordStatus.Deleted)
        {
            throw new HException(HatfErrorCodes.AccountDeleted);
        }

        return user;
    }

    private async Task<AuthToken> SaveToken(IAuthClient authClient, string deviceId, string userId, TokenType tokenType, long tokenLifeTime, bool deleteOld)
    {
        AuthToken authToken = GenerateAuthToken(authClient, deviceId, userId, tokenType, tokenLifeTime);
        if (await SaveToken(authToken, deleteOld) == false)
        {
            throw new HException(HatfErrorCodes.UnableToSaveToken, tokenType);
        }

        return authToken;
    }

    public static AuthToken GenerateAuthToken(IAuthClient authClient, string deviceId, string userId, TokenType tokenType,
        long tokenLifeTime)
    {
        DateTime currentEpochMs = DateTimeUtils.Now;
        return new AuthToken
        {
            Expiry = currentEpochMs.AddSeconds(tokenLifeTime),
            Token = Guid.NewGuid().ToString("N"),
            ClientId = authClient.Id,
            Timestamp = currentEpochMs,
            DeviceId = deviceId,
            TokenStatus = TokenStatus.Active,
            TokenType = tokenType,
            UserId = userId,
            ClientType = authClient.Type
        };
    }

    public Task<UserInformationVO> GetUser(string userId)
    {
        if (string.IsNullOrEmpty(userId))
        {
            throw new HException(HatfErrorCodes.EmptyUserId);
        }
        return _sqlFacade.GetById<UserInformationVO>(userId);
    }

    public async Task<AuthResult> LoginWithUserId(AuthRequest request, string userId, bool genRefreshToken = true)
    {
        if (request.AdditionalData is not string branchId)
        {
            throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
        }

        AuthClientVO authClient = await ValidateAuthClientAndGrant(request, GrantType.RefreshToken);
        
        UserInformationVO user = await GetUser(userId);
        AuthResult result = await GenerateAuthResult(authClient, user, request.DeviceId, genRefreshToken);
        return result;
    }
    
}