using Microsoft.AspNetCore.Mvc.RazorPages;

namespace TZServer
{
    public class Program
    {
        public static void RegisterTradeZoneServer()
        {
            HatfServer.HatfServer.RegisterHatfServer();
            Hatf.RegisterAssembly("TZShared");
            Hatf.RegisterAssembly("TZServer");

            // Register DI Binding Initializer
            HSContainerFactory.RegisterDiBindingInitializer(new TZDIBindingInitializer());

            HatfInitTask.RegisterInitTasks(
                new LoadEmailsInitTask(),
                new LoadSyncAllowedBranchesInitTask()
            );
        }

        public static async Task Main(string[] args)
        {
            Program.RegisterTradeZoneServer();
            HatfServerSettings settings = new HatfServerSettings()
            {
                ApiName = "TZ Server",
                WithCors = false,
                AuthHandler = new TzJwtTokenValidator(),
                OnBeforeInit = BeforeInitializeHatfServer,
                RegisterServices = RegisterServices
            };

            await HatfServer.HatfServer.Initialize(settings, args);
            HatfServer.HatfServer.App.UseAppVersionApi();
            await HatfServer.HatfServer.RunServer();
        }

        private static Task RegisterServices(IServiceCollection services)
        {
            // Add razor pages
            // services.Configure<RazorPagesOptions>(o =>
            // {
            //     o.Conventions.AddAreaPageRoute(
            //         areaName: "PrivacyPolicy",
            //         pageName: "/Mkt",
            //         route: "/Pages/PrivacyPolicy/Mkt");
            // });
            return Task.CompletedTask;
        }

        private static async Task BeforeInitializeHatfServer(DiContainer requestContainer)
        {
            HatfServer.HatfServer.App.UseTzTestEndpoints();
            
            HLogger.RegisterInitLogData(TZLogger.InitLogData);
            HLogger.RegisterPrepareLogMessage(TZLogger.FinalizeLogMessage);
            HLogger.InitLogData("Startup");
            
            // Initialize tz registry
            TZRegistry registry = new TZRegistry();
            await requestContainer.InjectAsync(registry);
            await registry.Reload(requestContainer);
            HSContainerFactory.RootContainer.BindInstance(registry);
        }
    }
}