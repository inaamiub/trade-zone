﻿using System.Collections.Concurrent;
using System.Text;

namespace TZServer
{
    public class TZRegistry
    {
        private ConcurrentDictionary<string, RegistryVO> _registry = new();
        public async Task Reload(DiContainer container)
        {
            HSqlFacade sqlFacade = container.Resolve<HSqlFacade>();
            List<RegistryVO> registry = await sqlFacade.GetAll<RegistryVO>();
            ConcurrentDictionary<string, RegistryVO> newReg = new();
            foreach (RegistryVO registryVo in registry)
            {
                newReg[registryVo.RegistryKey] = registryVo;
            }

            _registry = newReg;
        }

        private RegistryVO GetRegistry(string key)
        {
            RegistryVO registry = TryGetRegistry(key);
            if (registry == null)
            {
                throw new Exception($"Registry not found with key {key}");
            }

            return registry;
        }
        
        private RegistryVO? TryGetRegistry(string key)
        {
            if (_registry.TryGetValue(key, out RegistryVO? registry) == false)
            {
                HLogger.Debug("Unable to get registry for key {0}", key);
            }
            return registry;
        }
        
        private string GetRegistryValue(string registryKey)
        {
            return GetRegistry(registryKey)!.Value;
        }
        
        private string TryGetRegistryValue(string registryKey)
        {
            RegistryVO? registry = TryGetRegistry(registryKey);
            if (registry == null)
            {
                return string.Empty;
            }

            return registry.Value;
        }
        
        private int GetRegistryIntValue(string registryKey)
        {
            string registryValue = GetRegistryValue(registryKey);
            if (int.TryParse(registryValue, out int regIntValue) == false)
            {
                throw new Exception($"Can not convert registry key {registryKey} value {registryKey} to int");
            }

            return regIntValue;
        }

        public string GetBranchName(string branchId)
        {
            return GetRegistryValue("TZBN" + branchId);
        }
        
        public int GetCurrentAndroidAppVersion(string branchId)
        {
            return GetRegistryIntValue("TZAAVC" + branchId);
        }
        
        public bool CheckIfAndroidUpdateForced(string branchId)
        {
            int value = GetRegistryIntValue("FUAA" + branchId);
            return value == 1;
        }
        
        public string GetSigningKey(string branchId, ClientType clientType)
        {
            StringBuilder sb = new StringBuilder("SK_");
            sb.Append(branchId);
            sb.Append("_");
            sb.Append(clientType);
            return GetRegistryValue(sb.ToString());
        }
        
        public string GetGuestUserId(string branchId)
        {
            return GetRegistryValue(GetGuestUserIdRegKey(branchId));
        }
        public string TryGetGuestUserId(string branchId)
        {
            return TryGetRegistryValue(GetGuestUserIdRegKey(branchId));
        }
        
        public static string GetGuestUserIdRegKey(string branchId)
        {
            StringBuilder sb = new StringBuilder("GUEST_USERID_");
            sb.Append(branchId);
            return sb.ToString();
        }
    }
}