﻿using System.Globalization;

namespace TZServer
{
    public class OrderManagementService
    {
        [Inject] private DiContainer _container;
        [Inject] private HSqlFacade _sqlFacade;
        [Inject] private SyncAllowedBranches _syncAllowedBranches;
        [Inject] private RequestMetadata _requestMetadata;
        [Inject] private TZSharedConfig _tzSharedConfig;

        public static OrderManagementService Create(DiContainer container)
        {
            OrderManagementService instance = new OrderManagementService();
            container.Inject(instance);
            return instance;
        }

        public async Task<List<StockReportModel>> GenerateStockReport(string branchId, string accountNo)
        {
            string query = $"exec SP_GetProductsWithStock '{branchId}', '{accountNo}'";

            return await _sqlFacade.GetListByQuery<StockReportModel>(query);
        }

        public async Task<List<ProductModel>> GetProducts(string branchId)
        {
            string query = $"exec SP_GetProducts '{branchId}'";

            return await _sqlFacade.GetListByQuery<ProductModel>(query);
        }
        
        public async Task<List<ProductStockModel>> GetProductStock(string branchId, string accountNo)
        {
            string query = $"exec SP_GetProductStock '{branchId}', '{accountNo}'";

            return await _sqlFacade.GetListByQuery<ProductStockModel>(query);
        }

        public async Task CreateNewOrder(UserInformationVO userInformationVo, CreateNewOrderCommandPayload order, OrderType orderType)
        {
            order.ValidateNewOrder(userInformationVo);

            if (order.Products == null)
            {
                throw new HException(TZErrorCodes.PRODUCTS_NOT_FOUND);
            }

            if (order.Products.Any(m => string.IsNullOrEmpty(m.Key)))
            {
                throw new HException(TZErrorCodes.ONE_OF_THE_PRODUCTS_NOT_FOUND);
            }
            
            if (order.Products.Any(m => m.Value < 1))
            {
                throw new HException(TZErrorCodes.ONE_OF_THE_PRODUCTS_HAS_ZERO_QUANTITY);
            }

            if (string.IsNullOrEmpty(order.UUID))
            {
                throw new HException(TZErrorCodes.UuidNotFound);
            }
            
            order.Trim();
            // Handle nullable values
            string remarks = order.Remarks.ReplaceSingleQuote();
            if (remarks.IsNullOrEmpty())
            {
                remarks = "NULL";
            }
            else
            {
                remarks = $"'{remarks}'";
            }

            string addressId = order.AddressId;
            if (order.OrderDeliveryType == OrderDeliveryType.Deliver)
            {
                addressId = $"'{addressId}'";
            }
            else
            {
                addressId = "NULL";
            }

            string productListInsertQuery =
                string.Join(",", order.Products.Select((m,i) => $"({i + 1},'{m.Key}',{m.Value})"));
            
            int ensureStock = _tzSharedConfig.EnsureItemStockInOrders ? 1 : 0;
            
            string query = $@"DECLARE @ProductList SO_PRODUCT_LIST;
                            INSERT @ProductList VALUES {productListInsertQuery}

                            Exec [dbo].[SP_CreateNewOrder1] '{userInformationVo.Id}', '{orderType}', '{order.UUID}', {remarks}, '{order.OrderDeliveryType}', '{order.PaymentMethod}', {addressId}, '{DateTimeUtils.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff", CultureInfo.InvariantCulture)}', @ProductList, '{ensureStock}'";

            await _sqlFacade.InsertByQueryWithReturnCode(query);
        }

        public Task<List<SalesOrderVO>> GetLastOrders(UserInformationVO userInformationVo, int length = 10)
        {
            if (userInformationVo.UserType != UserType.FullAccess && userInformationVo.UserType != UserType.PublicAccess)
            {
                HLogger.Warn($"User {userInformationVo.Id} type {userInformationVo.UserType} trying to get last order which is not allowed", HLogTag.Order);
                return Task.FromResult(new List<SalesOrderVO>());
            }

            string query = "EXEC SP_GetOrderHistory '" + userInformationVo.Id + "', '" + userInformationVo.BranchId + "', '" + length +  "', '1'";
            return _sqlFacade.GetAll<SalesOrderVO>(query);
        }

    }
}