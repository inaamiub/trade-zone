﻿using FluentValidation.Results;

namespace TZServer
{
    public class UserAccountManagementService
    {
        [Inject] private DiContainer _container;
        [Inject] private HSqlFacade _sqlFacade;
        [Inject] private SyncAllowedBranches _syncAllowedBranches;
        [Inject] private RequestMetadata _requestMetadata;

        public static UserAccountManagementService Create(DiContainer container)
        {
            UserAccountManagementService instance = new UserAccountManagementService();
            container.Inject(instance);
            return instance;
        }

        /// <summary>
        /// Signup for all kind of users
        /// </summary>
        /// <param name="userInformation"></param>
        /// <param name="clientId"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        /// <exception cref="HException"></exception>
        public async Task<AuthResult> Signup(SignupUserCommandPayload signupData)
        {
            // Model Validation, will throw exception if not valid
            UserInformationVO userInformation = signupData.User;
            userInformation.Validate();
            userInformation.Trim();
            
            userInformation.Email = userInformation.Email.ToLower();
            
            // Verify that branch is allowed
            bool sab = await _syncAllowedBranches.BranchAllowed(userInformation.BranchId);
            if (sab == false)
            {
                throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
            }

            // Validate client
            TzOAuth oAuth = _container.Resolve<TzOAuth>();
            AuthClientVO authClient = await oAuth.ValidateAuthClientAndGrant(signupData.ClientId, signupData.ClientSecret, GrantType.Password);
            
            // Verify uuid if already exists
            UserInformationVO uuidCheck = await
                _sqlFacade.Get(
                    SqlQueryBuilder<UserInformationVO>.Where(UserInformationVOKeys.UUID, userInformation.UUID));
            if (uuidCheck != default(UserInformationVO))
            {
                throw new HException(HatfErrorCodes.RecordAlreadyExists);
            }

            // Check if email already exists 
            UserInformationVO emailCheck = await
                _sqlFacade.Get(
                    SqlQueryBuilder<UserInformationVO>.Where(UserInformationVOKeys.BranchId, userInformation.BranchId)
                        .Equal(UserInformationVOKeys.Email, userInformation.Email)
                        .NotEqual(UserInformationVOKeys.UserType, UserType.AsGuest));
            if (emailCheck != default(UserInformationVO))
            {
                throw new HException(TZErrorCodes.EMAIL_ALREADY_EXISTS);
            }
            
            // Check user name availability
            if (userInformation.UserType == UserType.FullAccess || userInformation.UserType == UserType.PublicAccess)
            {
                await CheckUserNameAvailability(userInformation.BranchId, userInformation.UserName);
            }
            else
            {
                userInformation.UserName = null;
                userInformation.Password = null;
            }
            
            if (userInformation.UserType == UserType.FullAccess)
            {
                // 1. Verify that account no exists
                PartyVO p = await
                    _sqlFacade.Get(SqlQueryBuilder<PartyVO>.Where(PartyVOKeys.AccountNo, userInformation.AccountNo).Equal(PartyVOKeys.BranchId, userInformation.BranchId));
                if (p == default(PartyVO))
                {
                    throw new HException(TZErrorCodes.PARTY_DOES_NOT_EXISTS);
                }
                
                // 2. Verify that party doesn't already have an account
                UserInformationVO partyAlreadyExists =
                    await _sqlFacade.Get(SqlQueryBuilder<UserInformationVO>
                        .Where(PartyVOKeys.BranchId, userInformation.BranchId)
                        .Equal(UserInformationVOKeys.AccountNo, userInformation.AccountNo));
                if (partyAlreadyExists != default(UserInformationVO))
                {
                    throw new HException(TZErrorCodes.PARTY_ALREADY_HAVE_ACCOUNT);
                }
                
                // 3. Verify Phone No matches
                string userEnteredCellNo = userInformation.CellNo.Replace("-", "").Replace(" ", "").Trim();
                string phone1 = p.Phone1.Replace("-", "").Replace(" ", "").Trim();
                string phone2 = p.Phone2.Replace("-", "").Replace(" ", "").Trim();
                string phone3 = p.Phone3.Replace("-", "").Replace(" ", "").Trim();
                if (userEnteredCellNo != phone1 &&
                    userEnteredCellNo != phone2 &&
                    userEnteredCellNo != phone3)
                {
                    throw new HException(TZErrorCodes.CELL_NO_DIDNT_MATCH_WITH_SAVED_ONE);
                }

            }

            if (userInformation.UserType == UserType.AsGuest)
            {
                userInformation.RecordStatus = UserRecordStatus.Active;
            }
            else
            {
                userInformation.RecordStatus = UserRecordStatus.PendingVerification;
            }
            
            userInformation.ServerDateTime = DateTimeUtils.Now;
            string plainPassword = userInformation.Password;
            // Save User Information
            userInformation.Password = Utils.ComputeMd5Hash(userInformation.Password);

            UserAddressVO addressVO = userInformation.UserAddressVO;
            userInformation.Id = Utils.NewShortGuid();
            userInformation = await _sqlFacade.Insert(userInformation);

            addressVO.Id = userInformation.Id;
            addressVO.UserId = userInformation.Id;
            addressVO.Name = userInformation.Name;
            addressVO.CellNo = userInformation.CellNo;
            addressVO.BranchId = userInformation.BranchId;
            addressVO.UUID = Utils.NewShortGuid();
            userInformation.UserAddressVO = await _sqlFacade.Insert(addressVO);
            
            // _sqlFacade.ChangeListInsert(userInformation);
            // await _sqlFacade.Save();
            
            // Send verification email
            if (userInformation.RecordStatus == UserRecordStatus.PendingVerification)
            {
                await SendAccountVerificationEmail(userInformation, signupData.ClientId, signupData.DeviceId);
            }
            else
            {
                // Update user name and password for guest user
                userInformation.UserName = userInformation.Id;
                userInformation.Password = HConfigManager.Get<TZServerConfig>().GuestUserPassword;
                
                await _sqlFacade.Update(SqlQueryBuilder<UserInformationVO>.Update(UserInformationVOKeys.UserName, userInformation.UserName)
                    .Set(UserInformationVOKeys.Password, userInformation.Password)
                    .Equal(UserInformationVOKeys.UUID, userInformation.UUID));
            }
            
            // Generate user session
            AuthResult authResult = await oAuth.GenerateAuthResult(authClient, userInformation, signupData.DeviceId);
            return authResult;
        }

        /// <summary>
        /// Generate email verification token
        /// </summary>
        /// <param name="userInformationVo"></param>
        /// <param name="tokenType"></param>
        /// <param name="clientId"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        private async Task<AuthToken> GenerateVerificationToken(UserInformationVO userInformationVo, TokenType tokenType, string clientId, string deviceId)
        {

            TzOAuth tzOAuth = _container.Resolve<TzOAuth>();
            
            Random random = new Random();
            int code = random.Next(100000, 999999);
            // Make sure that no other unexpired same code exists
            while (await TokenExists( userInformationVo.Id, code + "", tokenType, tzOAuth))
            {
                // Try another code
                code = random.Next(100000, 999999);
            }

            DateTime now = DateTimeUtils.Now;
            AuthToken authToken = new AuthToken
            {
                Code = code + "",
                Expiry = now.AddSeconds(tzOAuth.GetTokenLifeTime(tokenType, userInformationVo)),
                Timestamp = now,
                Token = Utils.NewShortGuid(),
                TokenStatus = TokenStatus.Active,
                TokenType = tokenType,
                UserId = userInformationVo.Id,
                ClientId = clientId,
                DeviceId = deviceId,
            };

            await tzOAuth.SaveToken(authToken);
            return authToken;
        }
 
        private static async Task<bool> TokenExists(string userId, string token, TokenType tokenType, TzOAuth tzOAuth)
        {
            AuthTokenVO authTokenVo = await tzOAuth.GetAuthTokenVo(userId, token, tokenType);
            if (authTokenVo == default(AuthTokenVO))
            {
                return false;
            }
                
            // Token exists, let's check if it is expired
            if (DateTimeUtils.EpochInMS >= authTokenVo.Expiry)
            {
                return false;
            }
                
            return true;
        }

        /// <summary>
        /// Send account verification email
        /// </summary>
        /// <param name="userInformationVo"></param>
        /// <param name="clientId"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public async Task SendAccountVerificationEmail(UserInformationVO userInformationVo, string clientId, string deviceId)
        {
            if (userInformationVo.RecordStatus == UserRecordStatus.Active)
            {
                throw new HException(HatfErrorCodes.AccountAlreadyVerified);
            }
            else if (userInformationVo.RecordStatus != UserRecordStatus.PendingVerification)
            {
                throw new HException(HatfErrorCodes.AccountVerificationNotPending);
            }
            
            AuthToken token = await GenerateVerificationToken(userInformationVo, TokenType.AccountVerificationToken, clientId, deviceId);

            await new AccountVerificationEmail().Send(
                _container.Resolve<TZRegistry>().GetBranchName(userInformationVo.BranchId), token.Code,
                TimeSpan.FromSeconds(HConfigManager.Get<OAuthConfig>().AccountVerificationTokenLifeTime)
                    .TotalMinutes + " minutes", userInformationVo.Email);
        }

        /// <summary>
        /// Check if user name is available in branch
        /// Throws exception if not available
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// <exception cref="HException"></exception>
        public async Task<bool> CheckUserNameAvailability(string branchId, string userName)
        {
            UserInformationVO existingUserName = await _sqlFacade.Get(SqlQueryBuilder<UserInformationVO>
                .Where(UserInformationVOKeys.UserName, userName)
                .Equal(PartyVOKeys.BranchId, branchId));
                
            if (existingUserName != default(UserInformationVO))
            {
                throw new HException(HatfErrorCodes.UserNameNotAvailable);
            }

            return true;
        }
 
        /// <summary>
        /// Update user basic information includes Name, CellNo, City, Address, Email
        /// </summary>
        /// <param name="newUserInformation"></param>
        /// <returns></returns>
        /// <exception cref="HException"></exception>
        public async Task<UserInformationVO> UpdateBasicUserInfo(UserInformationVO newUserInformation)
        {
            // Model Validation, will throw exception if not valid
            newUserInformation.ValidateBasicUserInfo();
            
            // // Verify that branch is allowed
            // var sab = await _syncAllowedBranches.BranchAllowed(newUserInformation.BranchId);
            // if (sab == false)
            // {
            //     throw new HException(TZErrorCodes.BRANCH_NOT_FOUND);
            // }

            newUserInformation.Trim();
            newUserInformation.Email = newUserInformation.Email.ToLower();
            
            UserInformationVO existingUi = await _sqlFacade.GetById<UserInformationVO>(_requestMetadata.UserId);
            if (existingUi.RecordStatus == UserRecordStatus.Deleted)
            {
                throw new HException(HatfErrorCodes.AccountDeleted);
            }
            
            SqlQueryBuilder<UserInformationVO> query = SqlQueryBuilder<UserInformationVO>
                .Update(UserInformationVOKeys.Name, newUserInformation.Name)
                .Set(UserInformationVOKeys.CellNo, newUserInformation.CellNo)
                .Set(UserInformationVOKeys.Email, newUserInformation.Email);

            if (newUserInformation.Email != existingUi.Email)
            {
                // Need to verify email again
                query.Set(UserInformationVOKeys.RecordStatus, UserRecordStatus.PendingVerification);
            }

            // Add where clause
            query.Equal(UserInformationVOKeys.Id, existingUi.Id);
            
            // Update user default address
            SqlQueryBuilder<UserAddressVO> addressQuery = SqlQueryBuilder<UserAddressVO>
                .Update(UserAddressesVOKeys.Name, newUserInformation.Name)
                .Set(UserAddressesVOKeys.CellNo, newUserInformation.CellNo)
                .Set(UserAddressesVOKeys.Address, newUserInformation.UserAddressVO.Address)
                .Set(UserAddressesVOKeys.City, newUserInformation.UserAddressVO.City)
                .Set(UserAddressesVOKeys.State, newUserInformation.UserAddressVO.State)
                .Set(UserAddressesVOKeys.Country, "Pakistan")
                .Equal(UserAddressesVOKeys.Id, existingUi.Id);

            // Update data
            UserInformationVO updatedUserInfo = await _sqlFacade.GetByQuery<UserInformationVO>(query + ";" + addressQuery + ";" +
                                                            SqlQueryBuilder<UserInformationVO>.Where(
                                                                UserInformationVOKeys.Id, existingUi.Id));

            return updatedUserInfo;
        }
       
        /// <summary>
        /// Update user account information includes user name and password
        /// </summary>
        /// <param name="userInformationVo"></param>
        /// <param name="accountUpdateData"></param>
        /// <returns></returns>
        /// <exception cref="HException"></exception>
        public async Task<bool> UpdateAccountInfo(UserInformationVO userInformationVo, mdUserInformation accountUpdateData, bool checkCurrentPassword = true)
        {
            accountUpdateData.Trim();
            
            bool updateUserName = string.IsNullOrEmpty(accountUpdateData.NewUserName) == false;
            bool updatePassword = string.IsNullOrEmpty(accountUpdateData.NewPassword) == false;
            
            if (updateUserName == false && updatePassword == false)
            {
                return true;
            }

            if (checkCurrentPassword && userInformationVo.Password != Utils.ComputeMd5Hash(accountUpdateData.CurrentPassword))
            {
                throw new HException(HatfErrorCodes.InvalidCurrentPassword);
            }

            Dictionary<string, string> dbUpdate = new Dictionary<string, string>();
            if (updateUserName == true)
            {
                // Update user name
                if (userInformationVo.UserName != accountUpdateData.NewUserName)
                {
                    ValidationResult validationResult = await new UserInformationVoUserNameValidator().ValidateAsync(new UserInformationVO
                    {
                        UserName = accountUpdateData.NewUserName
                    });

                    if (validationResult.IsValid == false)
                    {
                        throw new HException(HatfErrorCodes.FormValidationError, validationResult.Errors);
                    }

                    // Check user name availability
                    await CheckUserNameAvailability(userInformationVo.BranchId, accountUpdateData.NewUserName);

                    // Append set query
                    dbUpdate.Add(UserInformationVOKeys.UserName, accountUpdateData.NewUserName);
                }
            }

            if (updatePassword == true)
            {
                string newPasswordHash = Utils.ComputeMd5Hash(accountUpdateData.NewPassword);
                if (userInformationVo.Password != newPasswordHash)
                {
                    ValidationResult validationResult = await new UserInformationVoPasswordValidator().ValidateAsync(new UserInformationVO
                    {
                        Password = accountUpdateData.NewPassword
                    });

                    if (validationResult.IsValid == false)
                    {
                        throw new HException(HatfErrorCodes.FormValidationError, validationResult.Errors);
                    }

                    dbUpdate.Add(UserInformationVOKeys.Password, newPasswordHash);
                }
            }

            // Check if there is any update
            if (dbUpdate.Count < 1)
            {
                return true;
            }

            // Create update query
            SqlQueryBuilder<UserInformationVO> updateQuery = SqlQueryBuilder<UserInformationVO>.Update();
            foreach ((string key, string value)  in dbUpdate)
            {
                updateQuery.Set(key, value);
            }

            updateQuery.Equal(UserInformationVOKeys.Id, userInformationVo.Id);

            // Execute update query
            await _container.Resolve<HSqlFacade>().Update(updateQuery);

            return true;
        }

        public async Task<UserInformationVO> GetUserInformationByEmail(string branchId, string email)
        {
            ValidationResult validationResult = await new UserInformationVoEmailValidator().ValidateAsync(new UserInformationVO
            {
                Email = email
            });

            if (validationResult.IsValid == false)
            {
                throw new HException(HatfErrorCodes.FormValidationError, validationResult.Errors);
            }

            email = email.Trim().ToLower();
            UserInformationVO userInformationVO = await _sqlFacade.Get(SqlQueryBuilder<UserInformationVO>.Where(UserInformationVOKeys.BranchId, branchId)
                .Equal(UserInformationVOKeys.Email, email)
                .Begin()
                .Equal(UserInformationVOKeys.UserType, UserType.FullAccess)
                .Or()
                .Equal(UserInformationVOKeys.UserType, UserType.PublicAccess)
                .End());
            
            if (userInformationVO == default(UserInformationVO))
            {
                throw new HException(TZErrorCodes.EMAIL_NOT_EXISTS);
            }

            if (userInformationVO.RecordStatus == UserRecordStatus.Deleted)
            {
                throw new HException(HatfErrorCodes.AccountDeleted);
            }

            return userInformationVO;
        }
        
        /// <summary>
        /// Check if email is valid, exists and not deleted then send password recovery email
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="email"></param>
        /// <param name="clientId"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        /// <exception cref="HException"></exception>
        public async Task SendPasswordRecoveryEmail(string branchId, string email, string clientId, string deviceId)
        {
            UserInformationVO userInformationVo = await GetUserInformationByEmail(branchId, email);

            AuthToken token = await GenerateVerificationToken(userInformationVo, TokenType.PasswordRecoveryToken, clientId, deviceId);

            await new PasswordRecoveryEmail().Send(
                _container.Resolve<TZRegistry>().GetBranchName(userInformationVo.BranchId), token.Code,
                TimeSpan.FromSeconds(HConfigManager.Get<OAuthConfig>().PasswordRecoveryTokenLifeTime)
                    .TotalMinutes + " minutes", userInformationVo.Email);
        }
        
    }
}