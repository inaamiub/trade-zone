﻿namespace TZServer
{
    public class UserAddressManagementService
    {
        [Inject] private DiContainer _container;
        [Inject] private HSqlFacade _sqlFacade;

        public static UserAddressManagementService Create(DiContainer container)
        {
            UserAddressManagementService instance = new UserAddressManagementService();
            container.Inject(instance);
            return instance;
        }

        public Task<List<UserAddressVO>> GetAllUserAddresses(UserInformationVO userInformationVo)
        {
            return _sqlFacade.GetAll(SqlQueryBuilder<UserAddressVO>.Where(UserAddressesVOKeys.BranchId, userInformationVo.BranchId)
                    .Equal(UserAddressesVOKeys.UserId, userInformationVo.Id));
        }

        public Task<UserAddressVO> AddNewAddress(UserAddressVO userAddressVo, UserInformationVO userInformationVo)
        {
            userAddressVo.ValidateUserAddress(userInformationVo);

            userAddressVo.Id = Utils.NewShortGuid();
            userAddressVo.Country = "Pakistan";
            userAddressVo.BranchId = userInformationVo.BranchId;
            userAddressVo.UserId = userInformationVo.Id;

            return _sqlFacade.Insert(userAddressVo);
        }

        public async Task<UserAddressVO> UpdateUserAddress(UserAddressVO userAddressVo, UserInformationVO userInformationVo)
        {
            userAddressVo.ValidateUserAddress(userInformationVo);
            if (string.IsNullOrEmpty(userAddressVo.Id))
            {
                HLogger.Error("Address id not provided while updating address", HLogTag.Request);
                throw new HException(TZErrorCodes.WRONG_ADDRESS_PROVIDED);
            }

            UserAddressVO existingAddressVO =
                await _sqlFacade.Get(SqlQueryBuilder<UserAddressVO>.Where(UserAddressesVOKeys.Id, userAddressVo.Id));
            if (existingAddressVO == default)
            {
                throw new HException(TZErrorCodes.ADDRESS_NOT_FOUND);
            }

            if (existingAddressVO.UserId != userInformationVo.Id)
            {
                throw new HException(HatfErrorCodes.NotAuthorizedToUpdate);
            }
            
            // Check for changes;
            Dictionary<string, object> updates = new Dictionary<string, object>();
            if (existingAddressVO.Id != existingAddressVO.UserId)
            {
                if (existingAddressVO.Name != userAddressVo.Name)
                {
                    updates.Add(UserAddressesVOKeys.Name, userAddressVo.Name);
                }
            }
            if (existingAddressVO.CellNo != userAddressVo.CellNo)
            {
                updates.Add(UserAddressesVOKeys.CellNo, userAddressVo.CellNo);
            }
            if (existingAddressVO.Address != userAddressVo.Address)
            {
                updates.Add(UserAddressesVOKeys.Address, userAddressVo.Address);
            }
            if (existingAddressVO.City != userAddressVo.City)
            {
                updates.Add(UserAddressesVOKeys.City, userAddressVo.City);
            }
            if (existingAddressVO.State != userAddressVo.State)
            {
                updates.Add(UserAddressesVOKeys.State, userAddressVo.State);
            }
            // if (existingAddressVO.Country != userAddressesVo.Country)
            // {
            //     updates.Add(UserAddressesVOKeys.Country, userAddressesVo.Country);
            // }

            if (updates.Count < 1)
            {
                return existingAddressVO;
            }

            SqlQueryBuilder<UserAddressVO> query = SqlQueryBuilder<UserAddressVO>.Update();
            foreach ((string key, object value) in updates)
            {
                query.Set(key, value);
            }
            query.Equal(UserInformationVOKeys.Id, existingAddressVO.Id);

            existingAddressVO = await _sqlFacade.GetByQuery<UserAddressVO>(query + ";" +
                                                                             SqlQueryBuilder<UserAddressVO>.Where(
                                                                                 UserAddressesVOKeys.Id,
                                                                                 existingAddressVO.Id));

            return existingAddressVO;
        }

    }
}