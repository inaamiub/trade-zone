﻿namespace TZServer
{
    public class LoadEmailsInitTask : HatfInitTaskBase
    {
        private const string EMAIL_TYPE_POSTFIX = "Email";
        private const string EMAIL_FILE_EXTENSION = ".html";
        private const string EMAIL_DIR = "Emails";

        protected override IEnumerable<Type> Filter(IEnumerable<Type> types)
        {
            Type type = typeof(EmailBase);
            return types.Where(m => m.BaseType == type);
        }

        protected override async Task OnExecute(IEnumerable<Type> types)
        {
            foreach (Type type in types)
            {
                await LoadEmailContent(type);
            }
        }

        private static async Task LoadEmailContent(Type emailType)
        {
            string fileName = GetEmailFileName(emailType.Name);
            string filePath = Path.Combine(HS.AssemblyLocation, EMAIL_DIR, fileName);
            if (File.Exists(filePath))
            {
                string content = await File.ReadAllTextAsync(filePath);
                HLogger.Info("Loaded email content from file {0}", filePath);
                EmailDictionary.Add(emailType, content);
            }
            else
            {
                throw new Exception($"Unable to load email content from file {filePath}. File not exist");
            }
        }

        private static string GetEmailFileName(string typeName)
        {
            if (typeName.EndsWith(EMAIL_TYPE_POSTFIX))
            {
                typeName = typeName.Substring(0, typeName.Length - EMAIL_TYPE_POSTFIX.Length);
            }

            string fileName = string.Empty;
            for (int i = 0; i < typeName.Length; i++)
            {
                char oneChar = typeName[i];
                if (Char.IsUpper(oneChar))
                {
                    oneChar = Char.ToLower(oneChar);
                    if (i > 0)
                    {
                        fileName += "_";
                    }
                }

                fileName += oneChar;
            }

            return fileName + EMAIL_FILE_EXTENSION;
        }
        
    }
}
