﻿namespace TZServer
{
    public class LoadSyncAllowedBranchesInitTask : HatfInitTaskBase
    {
        protected override IEnumerable<Type> Filter(IEnumerable<Type> types)
        {
            Type type = typeof(SyncAllowedBranchVO);
            return types.Where(m => m == type);
        }

        protected override async Task OnExecute(IEnumerable<Type> types)
        {
            SyncAllowedBranches sab = new SyncAllowedBranches();
            await _startupContainer.InjectAsync(sab);
            HSContainerFactory.RootContainer.BindInstance(sab);
            await sab.Reload();
            HLogger.Debug($"Loaded sync allowed branches", HLogTag.ServerStartup);
        }

    }
}
