﻿namespace TZServer
{
    public partial class TzServerStartup
    {
        public static Task InitOAuth()
        {
            OAuthConfig oAuthConfig = HConfigManager.Get<OAuthConfig>();
            if (HConfigManager.Get<HatfServerConfig>().EnableOAuth)
            {
                HLogger.Debug("Initializing OAuth", HLogTag.ServerStartup);

                OAuth.OAuth.Init(new OAuth.OAuthConfig
                {
                    AuthenticateClient = oAuthConfig.AuthenticateClient,
                    AuthorizationKey = oAuthConfig.AuthorizationKey
                });

                HLogger.Debug("OAuth Initialized", HLogTag.ServerStartup);
            }

            return Task.CompletedTask;
        }
    }
}