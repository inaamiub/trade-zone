﻿namespace TZServer
{
    public class SyncAllowedBranches
    {
        [Inject] public HSqlFacade _sqlFacade;
        private List<SyncAllowedBranchVO> _syncAllowedBranches = null;

        public async Task Reload()
        {
            List<SyncAllowedBranchVO> syncAllowedBranches = await _sqlFacade.GetAll<SyncAllowedBranchVO>();
            _syncAllowedBranches = syncAllowedBranches;
        }
        
        private async Task<SyncAllowedBranchVO> GetSab(string branchId)
        {
            if (_syncAllowedBranches == null)
            {
                await Reload();
                if (_syncAllowedBranches == null)
                {
                    throw new Exception($"Trying to check for branch id {branchId} but sync allowed branches is null");
                }
            }
            List<SyncAllowedBranchVO> branches = _syncAllowedBranches;
            return branches.FirstOrDefault(m => m.BranchId == branchId);
        }
        
        public async Task<bool> BranchAllowed(string branchId)
        {
            SyncAllowedBranchVO branch = await GetSab(branchId);
            return branch != null && branch.RecordStatus == 1;
        }

    }
}