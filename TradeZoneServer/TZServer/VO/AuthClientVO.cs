﻿namespace TZServer
{
    [MSSqlCollection("auth", "Clients")]
    public class AuthClientVO : HBaseVO, IAuthClient
    {
        public string Secret { get; set; }
        public string Description { get; set; }
        public Platform Platform { get; set; }
        public ClientType Type { get; set; }
        
    }
}