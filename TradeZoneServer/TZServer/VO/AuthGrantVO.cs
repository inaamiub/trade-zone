﻿namespace TZServer
{
    [MSSqlCollection("auth", "Grants")]
    public class AuthGrantVO : HBaseVO
    {
        public string ClientId { get; set; }
        public GrantType GrantType { get; set; }
        
    }

    public class AuthGrantVOKeys
    {
        public const string ClientId = "ClientId";
        public const string GrantType = "GrantType";
    }
}