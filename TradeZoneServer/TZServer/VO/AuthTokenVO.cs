﻿namespace TZServer
{
    [MSSqlCollection("auth", "Token")]
    public class AuthTokenVO : HBaseVO
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public string Code { get; set; }
        public TokenType TokenType { get; set; }
        public long Expiry { get; set; }
        public string ClientId { get; set; }
        public string DeviceId { get; set; }
        public TokenStatus RecordStatus { get; set; }
        public long Timestamp { get; set; }
    }
    
    public class AuthTokenVOKeys
    {
        public const string Token = "Token";
        public const string TokenType = "TokenType";
        public const string RecordStatus = "RecordStatus";
        public const string UserId = "UserId";
        public const string Code = "Code";
    }

}