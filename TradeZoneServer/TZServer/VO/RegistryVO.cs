﻿namespace TZServer
{
    [MSSqlCollection("dboServer", "Registry")]
    public class RegistryVO : HBaseVO
    {
        public string RegistryKey { get; set; }
        public string ParentRegistryKey { get; set; }
        public string Narration { get; set; }
        public string Value { get; set; }
    }

    public static class RegistryVOKeys
    {
        public const string RegistryKey = "RegistryKey";
        public const string Value = "Value";
    }
}