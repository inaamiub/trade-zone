﻿namespace TZServer
{
    [MSSqlCollection("dbo", "SalesBody")]
    public class SalesBodyVO : HBaseVO
    {
        public int SerialNo { get; set; }
        public string BranchId { get; set; }
        public int SaleId { get; set; }
        public string ProductId { get; set; }
        public decimal Quantity { get; set; }
        public int Bonus { get; set; }
        public decimal Price { get; set; }
        public decimal DiscRatio1 { get; set; }
        public decimal DiscRatio2 { get; set; }
        public decimal STValue { get; set; }
        public decimal STRatio { get; set; }
        public decimal TTLValue { get; set; }
        public decimal TTLSalestax { get; set; }
        public bool IsDeleted { get; set; }
        public string Status { get; set; }
        public string Scale { get; set; }
        public decimal DiscValue { get; set; }
    }
}