﻿namespace TZServer
{
    [MSSqlCollection("dbo", "Sales")]
    public class SalesVO : HBaseVO
    {
        public int SaleId { get; set; }
        public string BranchId { get; set; }
        public DateTime SaleDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string SaleType { get; set; }
        public string ExternalId { get; set; }
        public string OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string SalesManId { get; set; }
        public bool IsCashInvoice { get; set; }
        public decimal PreviousDebit { get; set; }
        public bool IsAutoBatch { get; set; }
        public decimal ReceivedAmount { get; set; }
        public decimal SpecialDiscount { get; set; }
        public DateTime DueDate { get; set; }
        public decimal PrintingCharges { get; set; }
        public bool IsPrinted { get; set; }
        public string Remarks { get; set; }
        public string TimeKey { get; set; }
        public int UserNo { get; set; }
        public bool IsPosted { get; set; }
        public short TotalInst { get; set; }
        public string InstType { get; set; }
        public DateTime InstStartFrom { get; set; }
        public int DepartmentID { get; set; }
        public string MobileNo { get; set; }
        
    }
}