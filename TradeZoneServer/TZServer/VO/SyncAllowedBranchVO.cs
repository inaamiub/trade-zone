﻿namespace TZServer
{
    [MSSqlCollection("dboServer", "SyncAllowedBranches")]
    public class SyncAllowedBranchVO: HBaseVO
    {
        public string BranchId { get; set; }
        public int? RecordStatus { get; set; }
        public string CurrentState { get; set; }
        public DateTime? StateUpdatedDateTime { get; set; }
        public DateTime? Expiry { get; set; }
        public string AccountVerificationEmailSender { get; set; }
        public string AccountVerificationEmailPassword { get; set; }
    }

    public class SyncAllowedBranchVOKeys
    {
        public const string BranchId = "BranchId";
        public const string RecordStatus = "RecordStatus";
    }
}